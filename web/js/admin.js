Dropzone.autoDiscover = false;
adItemOpenId = null;

$(function () {
    $('[data-toggle="tooltip"]').tooltip();


    $.fn.selectpicker.defaults = {
        noneSelectedText: '<span class="select-placeholder">Válassz</span>',
        noneResultsText: 'Nincs találat',
        countSelectedText: function (numSelected, numTotal) {
            return (numSelected == 1) ? "{0} elem kijelölve" : "{0} elem kijelölve";
        },
        maxOptionsText: function (numAll, numGroup) {
            var arr = [];

            arr[0] = (numAll == 1) ? 'Max. {n} elem jelölhető' : 'Max. {n} elem jelölhető';
            arr[1] = (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)';

            return arr;
        },
        selectAllText: 'Mindet kijelöl',
        deselectAllText: 'Kijelölést levesz',
        multipleSeparator: ', '
    };

    $(".datepicker-input").datepicker({
        dateFormat: 'yy.mm.dd.'
    });

    $("select.form-control").selectpicker();

    initDropzone();
    initSortable();

    $(document).on('mouseenter', '.media-thumb', function () {
        $(this).addClass('hover');
    });

    $(document).on('mouseleave', '.media-thumb', function () {
        $(this).removeClass('hover');
    });

    $(document).on('click', '.media-thumb-delete', function (e) {
        if (!confirm("Biztosan törlöd a képet?")) {
            return false;
        }
        e.preventDefault();
        var $container = $(this).closest('.media-thumb');

        $.post(
            $(this).attr('href'),
            {},
            function (data) {
                if (data != "ok") {
                    console.log(data);
                } else {
                    $container.fadeOut("slow", function () {
                        $container.remove();
                    });
                }
            }
        );
    });


    $(".form-collection").each(function (i) {
        initCollections($(this));
    });

    var $propertySubType = $("#property-subType");

    $propertySubType.depdrop({
        url: $propertySubType.data('url'),
        depends: ['property-type'],
        emptyMsg: 'Nincs találat',
        placeholder: 'Válassz',
        loadingText: 'Töltődik...'
    });

    $propertySubType.on('depdrop.afterChange', function (event, id, value) {
        $("#property-subType").selectpicker('refresh');
    });

    var $propertyCity = $("#property-city");
    $propertyCity.depdrop({
        url: $propertyCity.data('url'),
        depends: ['property-county'],
        emptyMsg: 'Nincs találat',
        placeholder: 'Válassz',
        loading: true,
        loadingText: 'Töltődik...'
    });
    $propertyCity.on('depdrop.afterChange', function (event, id, value) {
        $propertyCity.selectpicker('refresh');
    });

    var $propertyDistrict = $("#property-district");
    $propertyDistrict.depdrop({
        url: $propertyDistrict.data('url'),
        depends: ['property-city'],
        emptyMsg: 'Nincs találat',
        placeholder: 'Válassz',
        loading: true,
        loadingText: 'Töltődik...'
    });
    $propertyDistrict.on('depdrop.afterChange', function (event, id, value) {
        $propertyDistrict.selectpicker('refresh');
    });

    var $propertyBorough = $("#property-borough");
    $propertyBorough.depdrop({
        url: $propertyBorough.data('url'),
        depends: ['property-city', 'property-district'],
        emptyMsg: 'Nincs találat',
        placeholder: 'Válassz',
        loading: true,
        loadingText: 'Töltődik...'
    });
    $propertyBorough.on('depdrop.afterChange', function (event, id, value) {
        $propertyBorough.selectpicker('refresh');
    });

    // $('.money-input').autoNumeric("init", {
    //     aSep: ' ',
    //     aDec: '.',
    //     mDec: 0
    // });

    // $('.size-input').autoNumeric("init", {
    //     aSep: ' ',
    //     aDec: '.',
    //     mDec: 2
    // });

    var citynames = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: $("#search-cities").data('url') + '?query=%query',
            wildcard: '%query'
        }
    });
    citynames.initialize();

    $('#search-cities').tagsinput({
        typeaheadjs: {
            name: 'citynames',
            displayKey: 'name',
            valueKey: 'name',
            source: citynames.ttAdapter()
        }
    });

    $('#modal-box').on('hidden', function () {
        $(this).data('modal', null);
    });

    $(".phone-number-input").each(function () {
        var $input = $(this);
        $input.intlTelInput({
            preferredCountries: ["hu"],
            onlyCountries: ["al", "ad", "at", "by", "be", "ba", "bg", "hr", "cz", "dk",
                "ee", "fo", "fi", "fr", "de", "gi", "gr", "va", "is", "ie", "it", "lv",
                "li", "lt", "lu", "mk", "mt", "md", "mc", "me", "nl", "no", "pl", "pt", "ro",
                "ru", "sm", "rs", "sk", "si", "es", "se", "ch", "ua", "gb", "hu"],
            utilsScript: "/js/intl-tel-input/lib/libphonenumber/build/utils.js"
        });
        $input.closest("form").submit(function () {
            $input.val($input.intlTelInput("getNumber"));
        });
    });

    $('.autocomplete').each(function () {
        var $input = $(this);

        var coll = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: $input.data('url') + '?term=%query%',
                wildcard: '%query%'
            }
        });

        $input.typeahead(null, {
            display: 'value',
            source: coll,
            limit: 10
        });

    });


    var $agreementType = $("#property-agreementType");
    $agreementType.change(function (e) {
        togglePrice($agreementType);
    });
    togglePrice($agreementType);

    $("#fullUtility").click(function (e) {
        if ($(this).is(':checked')) {
            $(".utility-select").each(function () {
                $(this).val('VAN');
                $(this).selectpicker('refresh');
            });
        } else {
            $(".utility-select").each(function () {
                $(this).val('');
                $(this).selectpicker('refresh');
            });
        }
    });


    $("#batch-select-all").click(function (e) {
        if ($(this).is(':checked')) {
            $(".batch-checkbox").each(function () {
                this.checked = true;
            });
        } else {
            $(".batch-checkbox").each(function () {
                this.checked = false;
            });
        }
    });

    $(".batch-checkbox").each(function () {
        $(this).click(function (e) {
            var checked = true;
            $(".batch-checkbox").each(function () {
                checked = checked && this.checked == true;
            });

            $("#batch-select-all").get(0).checked = checked;

            e.stopPropagation();
        });
    });

    $(".batch-checkbox").each(function (ev) {
        var $ch = $(this);
        $ch.parent().click(function (e) {
            $ch.trigger('click');
        });
    });


    $("#batch-action-form").submit(function (e) {
        if ($("#batch-action-select").val() == "print" || $("#batch-action-select1").val() == "print"
            || $("#batch-action-select").val() == "printNoLogo" || $("#batch-action-select1").val() == "printNoLogo") {
            $(this).attr('target', '_blank');
        }
    });


    $(".property-list-tr").hover(
        function (e) {
            var position = $(this).position();
            $("#photo-thumb-container").css("display", "block").css("top", (e.pageY - 170) + "px").css("left", (e.pageX + 10) + "px")
                .html("<img src='" + $(this).data("img-url") + "' class='img-property'/>");
        }, function (e) {
            $("#photo-thumb-container").html("").css("display", "none");
        }
    );

    $(document).mousemove(function (e) {
        $("#photo-thumb-container").css("display", "block").css("top", (e.pageY - 170) + "px").css("left", (e.pageX + 10) + "px");
    });


    $(document).on("click", "#property-open-icom", function (e) {
        var $target = $($(this).data('target'));

        var icomId = $target.val();
        if (icomId) {
            $(this).attr('href', $(this).attr('href') + '/' + icomId)
        }

    });

    $(document).on('click', '.ajax-link', function (e) {
        e.preventDefault();
        var $container = $($(this).data('container'));
        $.get(
            $(this).attr('href'),
            function (data) {
                $container.html(data);
                $container.find(".datepicker-input").datepicker({
                    dateFormat: 'yy.mm.dd.'
                });
            }
        );
    });

    $(document).on('click', '#activity-save', function (e) {
        e.preventDefault();
        var $container = $($(this).data('container'));
        var $url = $(this).data('action');

        if (!$('#activity-type').val()) {
            alert("Az eseményt kötelező kiválasztani.");
            return false;
        }
        $.post(
            $url,
            {
                'type': $('#activity-type').val(),
                'comment': $('#activity-comment').val(),
                'createdAt': $('#activity-createdAt').val()
            },
            function (data) {
                if (data) {
                    $container.html(data);
                    $container.find(".datepicker-input").datepicker({
                        dateFormat: 'yy.mm.dd.'
                    });
                }
            }
        );

    });

    $(document).on('change', '#property-priceType', function (e) {
        // fix forint
        if ($(this).val() == 1) {
            $('#property-price').attr('readonly', false);
            $('#property-priceEur').attr('readonly', 'readonly');
        }
        // fix euró
        else if ($(this).val() == 2) {
            $('#property-price').attr('readonly', 'readonly');
            $('#property-priceEur').attr('readonly', false);
        }
        // mindkettő fix
        else if ($(this).val() == 3) {
            $('#property-price').attr('readonly', false);
            $('#property-priceEur').attr('readonly', false);
        }
    });
    $("#property-priceType").trigger('change');


    $(document).on('keyup', '#property-price', function (e) {
        var $priceEur = $("#property-priceEur");
        var $rate = $("#property-currencyEur");
        var type = $("#property-priceType").val();

        if (type == 1) {
            var price = parseInt($(this).val().replace(/\s+/g, ''));
            var priceEur = parseInt($priceEur.val().replace(/\s+/g, ''));
            var priceEurTmp = Math.round(price / parseInt($rate.val()));
            $priceEur.val(priceEurTmp);
        }
    });

    $(document).on('keyup', '#property-priceEur', function (e) {
        var $price = $("#property-price");
        var $rate = $("#property-currencyEur");
        var type = $("#property-priceType").val();
        if (type == 2) {
            var priceEur = parseInt($(this).val().replace(/\s+/g, ''));
            var price = parseInt($price.val().replace(/\s+/g, ''));
            var priceTmp = Math.round(priceEur * parseInt($rate.val()));
            $price.val(priceTmp);
        }
    });

    $(document).on('change', '#property-type', function (e){
        $('#property-form-panel').attr('class', '');
        $('#property-form-panel').addClass("panel panel-custom " + $(this).val().toLowerCase());
    });


});

function togglePrice($agreementType) {
    if ($agreementType.val() == 1) {
        $(".form-control-sm2.sale").show();
        $(".form-control-sm2.rent").hide();
        $(".control-label.sale").show();
        $(".control-label.rent").hide();
    }
    else if ($agreementType.val() == 2) {
        $(".form-control-sm2.sale").hide();
        $(".form-control-sm2.rent").show();
        $(".control-label.sale").hide();
        $(".control-label.rent").show();
    }
}

function postAjax(formElement) {
    var $form = $(formElement);
    $.post(
        $form.attr('action'),
        $form.serialize(),
        function (data) {
            if (data) {
                $("#modal-content").html(data);
            }
        }
    );

    return false;
}

function postAjaxForm(e, form, callback) {
    e.preventDefault();

    var $form = $(form);
    var url = $form.attr('action');
    var $container = $($form.data('container'));
    $container.show().append('<div class="ajax-loading-overlay"></div><div class="ajax-loading-img"><img src="/images/loading.gif" /></div>');
    $.ajax({
        type: "POST",
        url: url,
        data: $form.serialize(),
        success: function (data) {
            $container.html(data);
            if (typeof callback != "undefined") {
                callback();
            }
        }
    });
}

function split(val) {
    return val.split(/;\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

function initDropzone() {
    if ($("div.photo-uploader").length > 0) {
        $("div.photo-uploader").each(function (i) {
            initDropzoneElement($(this))
        });
    }
}

function initDropzoneElement($elem) {
    var prevTemp = '<div class="photo-uploader-file"><span data-dz-name></span><div class="dz-error-message"><span data-dz-errormessage></span></div></div>';
    var $dropzoneElement = $elem;

    $elem.dropzone({
        url: $dropzoneElement.data('url'),
        previewTemplate: prevTemp,
        addRemoveLinks: true,
        clickable: $dropzoneElement.data('clickable'),
        dictRemoveFile: '',
        init: function () {
            var myDropzone = this;

            this.on("addedfile", function (file) {
                $($dropzoneElement.data('clickable') + " .progress").show();
            });

            this.on("totaluploadprogress", function (uploadProgress) {
                $($dropzoneElement.data('clickable') + " .progress .progress-bar").css("width", Math.round(uploadProgress) + "%").html(Math.round(uploadProgress) + "%");
            });
            // feltöltött fotók betöltése
            this.on("queuecomplete", function (file, response) {
                $.get($dropzoneElement.data('refresh-url'), function (data) {
                    $($dropzoneElement.data('container')).html(data);
                    $("select.form-control").selectpicker();
                    initDropzoneElement($($dropzoneElement.data('container')).find('.photo-uploader'));
                    initSortable();
                });
            });
        }
    });
}


function initSortable() {
    $(".media-list").each(function (i) {
        var $obj = $(this);
        $(this).sortable({
            handle: '.media-thumb-move',
            cancel: '.media-upload-new',
            update: function (event, ui) {
                var data = $(this).sortable('serialize');

                // POST to server using $.post or $.ajax
                $.ajax({
                    data: data,
                    type: 'POST',
                    url: $obj.data('order-url')
                });
            }
        });
        $(this).disableSelection();
    });
}


function initCollections($collectionHolder) {

    var $newLinkLi = $('<li class="media-upload-new"><div class="upload-logo"><span class="glyphicon glyphicon-plus"></span></div><br>' + $collectionHolder.data('new-text') + '</li>');

    $collectionHolder.children('li').each(function () {
        addTagFormDeleteLink($(this), $collectionHolder.data('delete-text'));
        if ($collectionHolder.data('edit-text')) {
            addTagFormEditLink($(this), "Módosítás");
        }
    });

    $collectionHolder.append($newLinkLi);
    $collectionHolder.data('index', $collectionHolder.children('li').length);

    $newLinkLi.click(function (e) {
        e.preventDefault();
        addTagForm($collectionHolder, $newLinkLi);
    });
}

function addTagForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');
    var index = $collectionHolder.data('index');
    var newForm = prototype.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    var $newFormLi = $('<li class="media-thumb"></li>').append(newForm);
    addTagFormDeleteLink($newFormLi, $collectionHolder.data('delete-text'));
    if ($collectionHolder.data('edit-text')) {
        addTagFormEditLink($newFormLi, "Módosítás");
    }
    $newLinkLi.before($newFormLi);

    initCollections($newFormLi.find(".form-collection"));

}

function addTagFormDeleteLink($tagFormLi, deleteText) {
    var $removeFormA = $('<a href="#" class="media-thumb-delete2" title="' + deleteText + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>');
    $tagFormLi.find('.media-tools-container').append($removeFormA);

    $removeFormA.click(function (e) {
        if (!confirm("Biztosan törlöd?")) {
            return false;
        }
        e.preventDefault();
        $tagFormLi.remove();
    });
}

function addTagFormEditLink($tagFormLi, deleteText) {
    var $removeFormA = $('<a href="#" class="media-thumb-edit" title="' + deleteText + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>');
    $tagFormLi.find('.media-tools-container').append($removeFormA);

    $removeFormA.click(function (e) {
        e.preventDefault();

        $tagFormLi.find('.media-form-container .embed-container').hide();
        $tagFormLi.find('.media-form-container .html-container').show();
    });
}

