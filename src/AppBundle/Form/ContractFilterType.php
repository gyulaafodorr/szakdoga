<?php

namespace AppBundle\Form;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Contract;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\FloorEnum;
use AppBundle\Enum\PropertyTypeEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $options['em'];

        $administrators = $entityManager->getRepository('AppBundle\Dto\Administrator')
            ->createQueryBuilder('a')
            ->where('a.status = :status')
            ->andWhere('a.role IN (:role)')
            ->setParameter('status', Administrator::STATUS_ACTIVE)
            ->setParameter('role', array(Administrator::ROLE_ADMIN, Administrator::ROLE_SUPER_ADMIN))
            ->orderBy('a.name', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        $adminArr = array("" => "Összes");
        foreach ($administrators as $a) {
            $adminArr[$a['id']] = $a['name'];
        }
        $adminArr = array_flip($adminArr);

        $builder
            ->add('id', TextType::class, array('label' => 'Azonosító', 'required' => false,))
            ->add('contractNumber', TextType::class, array('label' => 'Szerződés száma', 'required' => false))
            ->add('status', ChoiceType::class, array(
                'label' => 'Státusz',
                'choices' => array_flip(Contract::getStatusMap()),
                'required' => false,
            ))
            ->add('agreementType', ChoiceType::class, array(
                'label' => 'Értékesítési mód',
                'required' => false,
                'choices' => array_flip(AgreementTypeEnum::getLabelConstMap()),
            ))
            ->add('priceFrom', TextType::class, array(
                'label' => 'Eladási ár (tól)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('priceTo', TextType::class, array(
                'label' => 'Eladási ár (ig)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('priceEurFrom', TextType::class, array(
                'label' => 'Euro ár (tól)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('priceEurTo', TextType::class, array(
                'label' => 'Euro ár (ig)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('clientFirstname', TextType::class, array(
                'label' => '',
                'required' => false,
                'attr' => array('placeholder' => 'Keresztnév'),
            ))
            ->add('clientLastname', TextType::class, array(
                'label' => 'Tulajdonos neve',
                'required' => false,
                'attr' => array('placeholder' => 'Vezetéknév'),
            ))
            ->add('isExclusive', CheckboxType::class, array('label' => 'Kizárólagos', 'required' => false))
            ->add('administrator', ChoiceType::class, array(
                'label' => 'Munkatárs',
                'required' => false,
                'choices' => $adminArr,
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired(array('em'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'contract_filter';
    }
}
