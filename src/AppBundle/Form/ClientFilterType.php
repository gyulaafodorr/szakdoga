<?php

namespace AppBundle\Form;

use AppBundle\Dto\Administrator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $options['em'];

        $administrators = $entityManager->getRepository('AppBundle\Dto\Administrator')
            ->createQueryBuilder('a')
            ->where('a.status = :status')
            ->andWhere('a.role IN (:role)')
            ->setParameter('status', Administrator::STATUS_ACTIVE)
            ->setParameter('role', array(Administrator::ROLE_ADMIN, Administrator::ROLE_SUPER_ADMIN))
            ->orderBy('a.name', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        $adminArr = array("" => "Összes");
        foreach ($administrators as $a) {
            $adminArr[$a['id']] = $a['name'];
        }

        $adminArr = array_flip($adminArr);
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Ügyfél neve',
                'required' => false,
            ))
//            ->add('phone', TextType::class, array('label' => 'Telefonszám', 'required' => false,))
            ->add('city', TextType::class, array('label' => 'Település', 'required' => false,))
            ->add('zipcode', TextType::class, array('label' => 'Irányítószám', 'required' => false,))
            ->add('address', TextType::class, array('label' => 'Utca, házszám', 'required' => false,))
            ->add('comment', TextType::class, array('label' => 'Megjegyzés', 'required' => false,))
            ->add('searchArchives', CheckboxType::class, array('label' => 'Keresés archívok között', 'required' => false,))
            ->add('email', TextType::class, array('label' => 'Email', 'required' => false))
//            ->add('contact', TextType::class, array('label' => 'Telefonon elérhető egyéb személy', 'required' => false))
//            ->add('isActive', CheckboxType::class, array('label' => 'Aktív', 'required' => false))
            ->add('createdAtFrom', DateType::class, array(
                'label' => 'Létrehozva (tól)',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'yyyy.MM.dd.',
                'required' => false,
                'invalid_message' => 'A dátum hibás, helyesen pl. 2015.05.19.',
            ))
            ->add('createdAtTo', DateType::class, array(
                'label' => 'Létrehozva (ig)',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'yyyy.MM.dd.',
                'required' => false,
                'invalid_message' => 'A dátum hibás, helyesen pl. 2015.05.19.',
            ))
            ->add('administrator', ChoiceType::class, array(
                'label' => 'Munkatárs',
                'required' => false,
                'choices' => $adminArr,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired(array('em'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'client_filter';
    }
}
