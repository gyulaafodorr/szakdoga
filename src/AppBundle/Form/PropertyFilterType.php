<?php

namespace AppBundle\Form;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Property;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\ConditionEnum;
use AppBundle\Enum\FloorEnum;
use AppBundle\Enum\PropertyTypeEnum;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PropertyFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $options['em'];

        $administrators = $entityManager->getRepository('AppBundle\Dto\Administrator')
            ->createQueryBuilder('a')
            ->where('a.status = :status')
            ->andWhere('a.role IN (:role)')
            ->setParameter('status', Administrator::STATUS_ACTIVE)
            ->setParameter('role', array(Administrator::ROLE_ADMIN, Administrator::ROLE_SUPER_ADMIN))
            ->orderBy('a.name', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        $adminArr = array();
        foreach ($administrators as $a) {
            $adminArr[$a['id']] = $a['name'];
        }

        $adminArr = array_flip($adminArr);

        $districts = array_flip(array_merge(array("0" => ""), array(
            1 => "1. kerület",
            2 => "2. kerület",
            3 => "3. kerület",
            4 => "4. kerület",
            5 => "5. kerület",
            6 => "6. kerület",
            7 => "7. kerület",
            8 => "8. kerület",
            9 => "9. kerület",
            10 => "10. kerület",
            11 => "11. kerület",
            12 => "12. kerület",
            13 => "13. kerület",
            14 => "14. kerület",
            15 => "15. kerület",
            16 => "16. kerület",
            17 => "17. kerület",
            18 => "18. kerület",
            19 => "19. kerület",
            20 => "20. kerület",
            21 => "21. kerület",
            22 => "22. kerület",
            23 => "23. kerület",
        )));
        $conditions = (ConditionEnum::getLabelConstMap());

        $builder
            ->add('id', TextType::class, array('label' => 'Azonosító', 'required' => false,))
            ->add('type', ChoiceType::class, array(
                'label' => 'Típus',
                'choices' => array_flip(PropertyTypeEnum::getLabelConstMap()),
                'required' => false,
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'Státusz',
                'choices' => array_flip(Property::getStatusMap()),
                'required' => false,
            ))
            ->add('sizeFrom', TextType::class, array(
                'label' => 'Épület területe (tól)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('sizeTo', TextType::class, array(
                'label' => 'Épület területe (ig)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('lotSizeFrom', TextType::class, array(
                'label' => 'Telek területe (tól)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('lotSizeTo', TextType::class, array(
                'label' => 'Telek területe (ig)',
                'attr' => array('class' => 'form-control'),
                'required' => false,
            ))
            ->add('clientFirstname', TextType::class, array(
                'label' => '',
                'required' => false,
                'attr' => array('placeholder' => 'Keresztnév'),
            ))
            ->add('clientLastname', TextType::class, array(
                'label' => 'Tulajdonos neve',
                'required' => false,
                'attr' => array('placeholder' => 'Vezetéknév'),
            ))
            ->add('street', TextType::class, array('label' => 'Utca', 'required' => false,))
            ->add('district', ChoiceType::class, array(
                'label' => 'Kerület',
                'required' => false,
                'choices' => $districts,
                'multiple' => true,
            ))
            ->add('city', TextType::class, array('label' => 'Település', 'required' => false,))
            ->add('roomsFrom', TextType::class, array('label' => 'Szobák (tól)', 'required' => false,))
            ->add('roomsTo', TextType::class, array('label' => 'Szobák (ig)', 'required' => false,))
            ->add('administrator', ChoiceType::class, array(
                'label' => 'Referens',
                'required' => false,
                'choices' => $adminArr,
            ))
            ->add('floorFrom', ChoiceType::class, array(
                'label' => 'Emelet (tól)',
                'required' => false,
                'choices' => array_flip(FloorEnum::getLabelConstMap()),
            ))
            ->add('floorTo', ChoiceType::class, array(
                'label' => 'Emelet (ig)',
                'required' => false,
                'choices' => array_flip(FloorEnum::getLabelConstMap()),
            ))
            ->add('condition', ChoiceType::class, array(
                'label' => 'Állapot',
                'required' => false,
                'choices' => $conditions,
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($entityManager, $builder, $options) {
            $form = $event->getForm();
            $data = $event->getData();

            $form->add('subType', ChoiceType::class, array(
                'label' => 'Altípus',
                'required' => false,
                'choices' => isset($data['type']) ? array_flip((PropertyTypeEnum::getLabelConstSubtypeMap($data['type']))) : array()
            ));

        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($entityManager, $builder, $options) {
            $form = $event->getForm();
            $data = $event->getData();

            $form->add('subType', ChoiceType::class, array(
                'label' => 'Altípus',
                'required' => false,
                'choices' => isset($data['type']) ? array_flip(PropertyTypeEnum::getLabelConstSubtypeMap($data['type'])) : array()
            ));
        });
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired(array('em'));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'property_filter';
    }
}
