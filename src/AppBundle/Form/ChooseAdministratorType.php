<?php

namespace AppBundle\Form;

use AppBundle\Dto\Administrator;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChooseAdministratorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('administrator', EntityType::class, array(
                'label' => 'Referens',
                'required' => false,
                'class' => 'AppBundle\Dto\Administrator',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('a')
                        ->where('a.status = :status')
                        ->andWhere('a.role IN (:role)')
                        ->setParameter('status', Administrator::STATUS_ACTIVE)
                        ->setParameter('role', array(Administrator::ROLE_ADMIN, Administrator::ROLE_SUPER_ADMIN))
                        ->orderBy('a.name', 'ASC');
                },
                'choice_label' => 'name',
                'constraints' => array(
                    new NotBlank(['message' => "Kötelező mező"])
                )
            ));
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'choose_administrator';
    }
}
