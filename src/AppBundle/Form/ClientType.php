<?php

namespace AppBundle\Form;

use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\PhoneTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array('label' => 'Keresztnév', 'required' => false))
            ->add('lastname', TextType::class, array('label' => 'Vezetéknév', 'required' => false))
            ->add('zipcode', TextType::class, array('label' => 'Irányítószám', 'required' => false))
            ->add('city', TextType::class, array('label' => 'Település', 'required' => false))
            ->add('address', TextType::class, array('label' => 'Utca, házszám', 'required' => false))
            ->add('idNumber', TextType::class, array('label' => 'Szig. szám', 'required' => false))
            ->add('email', TextType::class, array('label' => 'Email', 'required' => false))
            ->add('emailType', ChoiceType::class, array(
                'label' => 'Email típusa',
                'required' => false,
                'choices' => array_flip(EmailTypeEnum::getLabelConstMap()),
            ))
            ->add('contact1Phone', TextType::class, array('label' => '1. Telefonszám', 'required' => false))
            ->add('contact1Type', ChoiceType::class, array(
                'label' => 'Típus',
                'required' => false,
                'choices' => array_flip(PhoneTypeEnum::getLabelConstMap()),
            ))
            ->add('contact1Description', TextType::class, array('label' => 'Megjegyzés', 'required' => false))
            ->add('contact2Phone', TextType::class, array('label' => '2. Telefonszám', 'required' => false))
            ->add('contact2Type', ChoiceType::class, array(
                'label' => 'Típus',
                'required' => false,
                'choices' => array_flip(PhoneTypeEnum::getLabelConstMap()),
            ))
            ->add('contact2Description', TextType::class, array('label' => 'Megjegyzés', 'required' => false))
            ->add('contact3Phone', TextType::class, array('label' => '3. Telefonszám', 'required' => false))
            ->add('contact3Type', ChoiceType::class, array(
                'label' => 'Típus',
                'required' => false,
                'choices' => array_flip(PhoneTypeEnum::getLabelConstMap()),
            ))
            ->add('contact3Description', TextType::class, array('label' => 'Megjegyzés', 'required' => false))

            ->add('comment', TextareaType::class, array('label' => 'Megjegyzés', 'required' => false))
            ->add('isActive', CheckboxType::class, array('label' => 'Aktív', 'required' => false))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Dto\Client',
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_client';
    }
}
