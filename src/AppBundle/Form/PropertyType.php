<?php

namespace AppBundle\Form;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Property;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\AtticEnum;
use AppBundle\Enum\BathroomToiletEnum;
use AppBundle\Enum\CeilingHeightEnum;
use AppBundle\Enum\ComfortEnum;
use AppBundle\Enum\ConditionEnum;
use AppBundle\Enum\CountyEnum;
use AppBundle\Enum\EnergyCertificateEnum;
use AppBundle\Enum\FloorEnum;
use AppBundle\Enum\HasFurnituresEnum;
use AppBundle\Enum\HeatingEnum;
use AppBundle\Enum\OfficeCategoryEnum;
use AppBundle\Enum\OrientationEnum;
use AppBundle\Enum\ParkingEnum;
use AppBundle\Enum\ParkingPriceTypeEnum;
use AppBundle\Enum\PriceTypeEnum;
use AppBundle\Enum\PropertyTypeEnum;
use AppBundle\Enum\UtilityEnum;
use AppBundle\Enum\ViewEnum;
use AppBundle\Enum\RadioButtonEnum;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $counties = array_flip((CountyEnum::getLabelConstMap()));
        $floors = array_flip((FloorEnum::getLabelConstMap()));
        $heatings = array_flip((HeatingEnum::getLabelConstMap()));
        $comforts = array_flip((ComfortEnum::getLabelConstMap()));
        $parkings = array_flip((ParkingEnum::getLabelConstMap()));
        $parkingPriceTypes = array_flip((ParkingPriceTypeEnum::getLabelConstMap()));
        $views = array_flip((ViewEnum::getLabelConstMap()));
        $conditions = array_flip((ConditionEnum::getLabelConstMap()));
        $utility = array_flip((UtilityEnum::getLabelConstMap()));
        $attic = array_flip((AtticEnum::getLabelConstMap()));
        $energyCert = array_flip((EnergyCertificateEnum::getLabelConstMap()));
        $bethroomToilet = array_flip((BathroomToiletEnum::getLabelConstMap()));
        $ceilingHeight = array_flip((CeilingHeightEnum::getLabelConstMap()));
        $priceType = array_flip((PriceTypeEnum::getLabelConstMap()));
        $radioButtonChoices = array_flip((RadioButtonEnum::getLabelConstMap()));
        $hasFurnituresChoices = array_flip((HasFurnituresEnum::getLabelConstMap()));

        $builder
            ->add('id', HiddenType::class)
            /********************* STEP 1 **********************/
            ->add('type', ChoiceType::class, array(
                'label' => 'Típus',
                'choices' => array_flip(PropertyTypeEnum::getLabelConstMap()),
                'required' => false,
            ))
            ->add('orientation', ChoiceType::class, array(
                'label' => 'Tájolás',
                'choices' => array_flip(OrientationEnum::getLabelConstMap()),
                'required' => false,
            ))
            ->add('county', ChoiceType::class, array(
                'label' => 'Megye',
                'required' => false,
                'choices' => $counties,
            ))
            ->add('street', TextType::class, array('label' => 'Utca', 'required' => false))
            ->add('streetNp', CheckboxType::class, array('label' => 'Nem publikus', 'required' => false))
            ->add('houseNumberNp', CheckboxType::class, array('label' => 'Nem publikus', 'required' => false))
            ->add('houseNumber', TextType::class, array('label' => 'Házszám', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'Ingatlan leírása', 'required' => false))

            /********************* STEP 2 **********************/

            ->add($builder
                ->create('size', TextType::class, array(
                    'label' => 'Alapterület',
                    'required' => false,
                    'invalid_message' => 'Itt csak számot adhatsz meg.',))
            )
            ->add($builder
                ->create('lotSize', TextType::class, array(
                    'label' => 'Telekterület',
                    'required' => false,
                    'invalid_message' => 'Itt csak számot adhatsz meg.',))
            )
            ->add('rooms', TextType::class, array('label' => 'Szobák', 'required' => false))
            ->add('halfRooms', TextType::class, array('label' => 'Félszobák', 'required' => false))
            ->add('levels', TextType::class, array('label' => 'Belső szintek száma', 'required' => false))
            ->add('floor', ChoiceType::class, array(
                'label' => 'Emelet',
                'required' => false,
                'choices' => $floors,
            ))
            ->add('heating', ChoiceType::class, array(
                'label' => 'Fűtés',
                'required' => false,
                'choices' => $heatings,
            ))
            ->add('comfort', ChoiceType::class, array(
                'label' => 'Komfort',
                'required' => false,
                'choices' => $comforts,
            ))
            ->add('hasElectricity', ChoiceType::class, array(
                'label' => 'Villany',
                'required' => false,
                'choices' => $utility,
            ))
            ->add('hasGas', ChoiceType::class, array(
                'label' => 'Gáz',
                'required' => false,
                'choices' => $utility,
            ))
            ->add('hasWater', ChoiceType::class, array(
                'label' => 'Víz',
                'required' => false,
                'choices' => $utility,
            ))
            ->add('hasDrain', ChoiceType::class, array(
                'label' => 'Csatorna',
                'required' => false,
                'choices' => $utility,
            ))
            ->add('hasElevator', ChoiceType::class, array(
                'label' => 'Lift',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('hasCellar', ChoiceType::class, array(
                'label' => 'Pince',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('parking', ChoiceType::class, array(
                'label' => 'Parkolás',
                'required' => false,
                'choices' => $parkings,
            ))
            ->add('parkingFee', TextType::class, array('label' => 'Parkolási díj', 'required' => false))
            ->add('view', ChoiceType::class, array(
                'label' => 'Kilátás',
                'required' => false,
                'choices' => $views,
            ))
            ->add('condition', ChoiceType::class, array(
                'label' => 'Állapot',
                'required' => false,
                'choices' => $conditions,
            ))
            ->add(
                $builder
                    ->create('balconySize', TextType::class, array(
                        'label' => 'Erkély mérete',
                        'required' => false,
                        'invalid_message' => 'Itt csak számot adhatsz meg.',))
            )
            ->add(
                $builder
                    ->create('gardenSize', TextType::class, array(
                        'label' => 'Kert mérete',
                        'required' => false,
                        'invalid_message' => 'Itt csak számot adhatsz meg.',))
            )
            ->add('attic', ChoiceType::class, array(
                'label' => 'Tetőtér',
                'required' => false,
                'choices' => $attic,
            ))
            ->add('hasFurniture', ChoiceType::class, array(
                'label' => 'Bútorozott',
                'choices' => $hasFurnituresChoices,
                'required' => true
            ))
            ->add('energyCertificate', ChoiceType::class, array(
                'label' => 'Energiabesorolás',
                'required' => false,
                'choices' => $energyCert,
            ))
            ->add('ceilingHeight', ChoiceType::class, array(
                'label' => 'Belmagasság',
                'required' => false,
                'choices' => $ceilingHeight,
            ))
            ->add('smoker', ChoiceType::class, array(
                'label' => 'Dohányzó',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('hasDisabledAccess', ChoiceType::class, array(
                'label' => 'Akadálymentesített',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('mechanized', ChoiceType::class, array(
                'label' => 'Gépesített',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('petAllowed', ChoiceType::class, array(
                'label' => 'Kisállat',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('panelProgram', ChoiceType::class, array(
                'label' => 'Panelprogram',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add($builder
                ->create('utilityBills', TextType::class, array(
                    'label' => 'Rezsi összege',
                    'attr' => array('class' => 'form-control money-input'),
                    'required' => false,
                    'invalid_message' => 'Itt csak számot adhatsz meg.',))
            )
            ->add('hasGardenAccess', ChoiceType::class, array(
                'label' => 'Kertkapcsolatos',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('leaseRights', CheckboxType::class, array('label' => 'Bérleti jog', 'required' => false))
            ->add('hasAc', ChoiceType::class, array(
                'label' => 'Légkondicionált',
                'choices' => $radioButtonChoices,
                'required' => true
            ))
            ->add('bathToilet', ChoiceType::class, array(
                'label' => 'Fürdő és wc',
                'required' => false,
                'choices' => $bethroomToilet,
            ))
            ->add('officeCategory', ChoiceType::class, array(
                'label' => 'Iroda kategóriája',
                'choices' => array_flip(OfficeCategoryEnum::getLabelConstMap()),
                'required' => false,
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($builder, $options) {
            $form = $event->getForm();
            $data = $event->getData();

            $form->add('subType', ChoiceType::class, array(
                'label' => 'Altípus',
                'required' => false,
                'choices' => array_flip(PropertyTypeEnum::getLabelConstSubtypeMap($data->getType()))
            ));

            if ($data->getCounty()) {
                $cityOptions = array(
                    'query_builder' => function (EntityRepository $er) use ($data) {
                        return $er->createQueryBuilder('c')
                            ->where('c.county = :countyId')
                            ->setParameter('countyId', $data->getCounty())
                            ->orderBy('c.name', 'ASC');
                    },
                );
            }
            else {
                $cityOptions = array(
                    'choices' => array(),
                );
            }

            $form->add('city', EntityType::class, array_merge($cityOptions, array(
                'label' => 'Település',
                'required' => false,
                'class' => 'AppBundle\Dto\City',
                'choice_label' => 'name',
                'invalid_message' => 'Kötelező mező',
            )));

            if ($data->getCity()) {
                $cityOptions = array(
                    'query_builder' => function (EntityRepository $er) use ($data) {
                        $q = $er->createQueryBuilder('b')
                            ->innerJoin('b.city', 'c')
                            ->where('c.id = :city')
                            ->setParameter('city', $data->getCity()->getId())
                            ->orderBy('b.borough', 'ASC');

                        if ($data->getDistrict()) {
                            $q->andWhere('b.district = :district')
                                ->setParameter('district', $data->getDistrict());
                        }
                        return $q;
                    },
                );
            }
            else {
                $cityOptions = array(
                    'choices' => array(),
                );
            }

            $form->add('borough', EntityType::class, array_merge($cityOptions, array(
                'label' => 'Városrész',
                'required' => false,
                'class' => 'AppBundle\Dto\Borough',
                'choice_name' => 'borough',
                'choice_label' => 'borough',
                'invalid_message' => 'Kötelező mező',
            )));

            $districts = array();
            if ($data->getCity() && $data->getCity()->getId() == 392) {
                $districts = array_combine(range(1, 23), range(1, 23));
            }

            $form->add('district', ChoiceType::class, array(
                'label' => 'Kerület',
                'required' => false,
                'choices' => $districts,
            ));
        });


        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($builder, $options) {
            $form = $event->getForm();
            $data = $event->getData();

            $form->add('subType', ChoiceType::class, array(
                'label' => 'Altípus',
                'required' => false,
                'choices' => array_flip(PropertyTypeEnum::getLabelConstSubtypeMap($data['type']))
            ));


            if (isset($data['county']) && $data['county']) {
                $cityOptions = array(
                    'query_builder' => function (EntityRepository $er) use ($data) {
                        return $er->createQueryBuilder('c')
                            ->where('c.county = :countyId')
                            ->setParameter('countyId', $data['county'])
                            ->orderBy('c.name', 'ASC');
                    },
                );
            }
            else {
                $cityOptions = array(
                    'choices' => array(),
                );
            }

            $form->add('city', EntityType::class, array_merge($cityOptions, array(
                'label' => 'Település',
                'required' => false,
                'class' => 'AppBundle\Dto\City',
                'choice_label' => 'name',
                'invalid_message' => 'Kötelező mező',
            )));


            if (isset($data['city']) && $data['city']) {
                $cityOptions = array(
                    'query_builder' => function (EntityRepository $er) use ($data) {
                        $q = $er->createQueryBuilder('b')
                            ->innerJoin('b.city', 'c')
                            ->where('c.id = :city')
                            ->setParameter('city', $data['city'])
                            ->orderBy('b.borough', 'ASC');

                        if (isset($data['district']) && $data['district']) {
                            $q->andWhere('b.district = :district')
                                ->setParameter('district', $data['district']);
                        }
                        return $q;
                    },
                );
            }
            else {
                $cityOptions = array(
                    'choices' => array(),
                );
            }

            $form->add('borough', EntityType::class, array_merge($cityOptions, array(
                'label' => 'Városrész',
                'required' => false,
                'class' => 'AppBundle\Dto\Borough',
                'choice_label' => 'borough',
                'invalid_message' => 'Kötelező mező',
            )));

            $districts = array();
            if (isset($data['city']) && $data['city'] == 392) {
                $districts = array_combine(range(1, 23), range(1, 23));
            }

            $form->add('district', ChoiceType::class, array(
                'label' => 'Kerület',
                'required' => false,
                'choices' => $districts,
            ));
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Dto\Property',
            'clientId' => null,
        ));

        $resolver->setRequired('clientId');
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_property';
    }
}
