<?php

namespace AppBundle\Form;

use AppBundle\Dto\Contract;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\AgreementTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contractNumber', TextType::class, array('label' => 'Megbízás sorszáma', 'required' => false))
            ->add('commission', TextType::class, array('label' => 'Jutalék (%)', 'required' => false))
            ->add('comment', TextareaType::class, array('label' => 'Megjegyzés', 'required' => false))
            ->add('agreementType', ChoiceType::class, array(
                'label' => 'Megbízás típusa',
                'required' => false,
                'choices' => array_flip(AgreementTypeEnum::getLabelConstMapWithEmpty()),
            ))
            ->add('isExclusive', CheckboxType::class, array('label' => 'Kizárólagos?', 'required' => false))
            ->add('deadline', DateType::class, array(
                'label' => 'Megbízás határideje',
                'input' => 'datetime',
                'widget' => 'single_text',
                'format' => 'yyyy.MM.dd.',
                'required' => false,
                'invalid_message' => 'A dátum hibás, helyesen pl. 2015.05.19.',
                'data_class' => null,
            ))
            ->add('price', TextType::class, array('label' => 'Ár', 'required' => false))
            ->add('priceEur', TextType::class, array('label' => 'Ár (euró)', 'required' => false));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Dto\Contract',
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_client';
    }
}
