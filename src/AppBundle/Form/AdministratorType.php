<?php

namespace AppBundle\Form;

use AppBundle\Dto\Administrator;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdministratorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Felhasználónév',
            ))
            ->add('name', TextType::class, array(
                'label' => 'Név',
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email cím',
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
            ))
            ->add('phone2', TextType::class, array(
                'label' => 'Telefon (2)',
            ))
            ->add('phone3', TextType::class, array(
                'label' => 'Telefon (3)',
            ))
            ->add('role', ChoiceType::class, array(
                'label' => 'Jogosultság',
                'choices' => array_flip(Administrator::getRoleMap()),
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'Státusz',
                'choices' => array_flip(Administrator::getStatusMap()),
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'A jelszavaknak egyezniük kell.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => false,
                'first_options' => array('label' => 'Jelszó'),
                'second_options' => array('label' => 'Jelszó megerősítése'),
            ));
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_classss' => 'AppBundle\Dto\Administrator',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_administrator';
    }
}
