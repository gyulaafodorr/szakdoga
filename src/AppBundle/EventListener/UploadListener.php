<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 04. 20.
 * Time: 13:23
 */

namespace AppBundle\EventListener;

use AppBundle\Service\PropertyService;
use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;

class UploadListener
{
    /**
     * @var EntityManager
     */
    protected $entityManager;
    /**
     * @var PropertyService
     */
    protected $service;
    /**
     * @var DataManager
     */
    protected $liipDataManager;
    /**
     * @var FilterManager
     */
    protected $liipFilterManager;

    public function __construct($entityManager, $service, $liipDataManager, $liipFilterManager)
    {
        $this->entityManager = $entityManager;
        $this->service = $service;
        $this->liipDataManager = $liipDataManager;
        $this->liipFilterManager = $liipFilterManager;
    }

    public function onUpload(PostPersistEvent $event)
    {
        $request = $event->getRequest();
        $type = $event->getType();
        $response = $event->getResponse();

        if ($type == "photo_upload") {
            $propertyId = $request->get('propertyId');
            $file = $event->getFile();

            $newPath = $file->getPath() . DIRECTORY_SEPARATOR . $propertyId;
            $newFilePath = $file->getPath() . DIRECTORY_SEPARATOR . $propertyId . DIRECTORY_SEPARATOR . $file->getFilename();
            $webPath = $propertyId . DIRECTORY_SEPARATOR . $file->getFilename();

            $file->move($newPath);

//            $image = $this->liipDataManager->find('photo_big', $webPath);
//            $imageResponse = $this->liipFilterManager->applyFilter($image, 'photo_big'); // run the filter
//            $thumb = $imageResponse->getContent(); // get the image from the response
//
//            $f = fopen($newFilePath, 'w'); // create thumbnail file
//            fwrite($f, $thumb); // write the thumbnail
//            fclose($f); // close the file


            $photo = $this->service->addMedia($propertyId, $webPath);


            $response['id'] = $photo->getId();
        }
    }
}