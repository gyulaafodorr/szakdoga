<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 12:18
 */

namespace AppBundle\EventListener;

use AppBundle\Dto\AbstractDto;
use Doctrine\ORM\Event\PreFlushEventArgs;

class DtoDetachListener
{
    /**
     * @param PreFlushEventArgs $args
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getIdentityMap() as $entityArray) {
            foreach ($entityArray as $entity) {
                if ($entity instanceof AbstractDto) {
                    $em->detach($entity);
                }
            }
        }
        $uow->computeChangeSets();
    }
}