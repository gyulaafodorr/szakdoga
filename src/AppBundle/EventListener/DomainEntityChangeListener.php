<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 29.
 * Time: 13:16
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\AbstractEntity;
use AppBundle\Entity\Changelog;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class DomainEntityChangeListener
{
    /**
     * @var TokenStorage
     */
    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @param PreFlushEventArgs $args
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $uow->computeChangeSets();

        foreach ($uow->getIdentityMap() as $entityArray) {
            /** @var AbstractEntity $entity */
            foreach ($entityArray as $entity) {
                if ($this->token->getToken() && $this->token->getToken()->getUser()) {
                    $administratorDto = $this->token->getToken()->getUser();
                    $administrator = $em->getRepository('AppBundle\Entity\Administrator')
                        ->find($administratorDto->getId());

                    $changeset = $uow->getEntityChangeSet($entity);
                    if (count($changeset) > 0 || !$entity->getId()) {
                        foreach ($changeset as $field => $values) {
                            if (!is_object($values[1]) && !is_object($values[0]) && $field != 'password') {
                                $changelog = new Changelog($administrator,
                                    $entity->getId(),
                                    $entity->getClass(),
                                    $field,
                                    $values[0],
                                    $values[1]
                                );
                                $em->persist($changelog);
                            }
                        }
                    }
                }
            }
        }
    }
} 