<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 27.
 * Time: 11:55
 */

namespace AppBundle\DataFixtures\Install;

use AppBundle\Entity\Administrator;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAdministratorData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $admin1 = new Administrator('Rendszer adminisztrátor', 'admin', 'admin@irodaszakdoga.hu', '+36304966322', null, null, 'valami', Administrator::ROLE_SYSTEM_ADMIN, Administrator::STATUS_ACTIVE);
        $manager->persist($admin1);

        $manager->flush();
    }
}