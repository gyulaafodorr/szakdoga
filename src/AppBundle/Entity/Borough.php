<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Borough
 *
 * @ORM\Table(name="borough", indexes={@ORM\Index(name="fk_bor_city_idx", columns={"city_id"})})
 * @ORM\Entity
 */
class Borough extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="district", type="integer", nullable=true)
     */
    protected $district;

    /**
     * @var string
     *
     * @ORM\Column(name="borough", type="string", length=255, nullable=false)
     */
    protected $borough;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    protected $city;

    /**
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @return string
     */
    public function getBorough()
    {
        return $this->borough;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }




}

