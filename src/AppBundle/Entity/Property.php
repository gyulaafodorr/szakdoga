<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Property
 *
 * @ORM\Table(name="property", indexes={
 *     @ORM\Index(name="fk_pro_client_idx", columns={"client_id"}),
 *     @ORM\Index(name="fk_pro_administrator_idx", columns={"administrator_id"}),
 *     @ORM\Index(name="fk_pro_city_idx", columns={"city_id"}),
 *     @ORM\Index(name="fk_pro_borough_idx", columns={"borough_id"})
 * })
 * @ORM\Entity
 */
class Property extends AbstractEntity
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_ARCHIVED = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    protected $client;

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    protected $city;

    /**
     * @var \AppBundle\Entity\Borough
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Borough")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="borough_id", referencedColumnName="id")
     * })
     */
    protected $borough;

    /**
     * @var \AppBundle\Entity\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administrator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     */
    protected $administrator;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PropertyMedia", mappedBy="property", cascade={"all"})
     * @ORM\OrderBy({"listingOrder" = "ASC"})
     * @var ArrayCollection
     */
    protected $medias;


    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_type", type="string", length=30, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $subType;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    protected $size;

    /**
     * @var integer
     *
     * @ORM\Column(name="lot_size", type="string", nullable=true)
     */
    protected $lotSize;

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=30, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $county;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    protected $street;

    /**
     * @var boolean
     *
     * @ORM\Column(name="street_np", type="boolean", nullable=true)
     */
    protected $streetNp = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="district", type="integer", nullable=true)
     */
    protected $district;

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=30, nullable=true)
     */
    protected $houseNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="house_number_np", type="boolean", nullable=true)
     */
    protected $houseNumberNp = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="rooms", type="integer", nullable=true)
     */
    protected $rooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="half_rooms", type="integer", nullable=true)
     */
    protected $halfRooms;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="string", length=50, nullable=true)
     */
    protected $floor;

    /**
     * @var integer
     *
     * @ORM\Column(name="levels", type="integer", nullable=true)
     */
    protected $levels;

    /**
     * @var string
     *
     * @ORM\Column(name="heating", type="string", length=50, nullable=true)
     */
    protected $heating;

    /**
     * @var string
     *
     * @ORM\Column(name="comfort", type="string", length=50, nullable=true)
     */
    protected $comfort;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    protected $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_elevator", type="integer", nullable=true)
     */
    protected $hasElevator;

    /**
     * @var string
     *
     * @ORM\Column(name="parking", type="string", length=50, nullable=true)
     */
    protected $parking;

    /**
     * @var string
     *
     * @ORM\Column(name="has_electricity", type="string", length=20, nullable=true)
     */
    protected $hasElectricity;

    /**
     * @var string
     *
     * @ORM\Column(name="has_gas", type="string", length=20, nullable=true)
     */
    protected $hasGas;

    /**
     * @var string
     *
     * @ORM\Column(name="has_drain", type="string", length=20, nullable=true)
     */
    protected $hasDrain;

    /**
     * @var string
     *
     * @ORM\Column(name="has_water", type="string", length=20, nullable=true)
     */
    protected $hasWater;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_cellar", type="integer", nullable=true)
     */
    protected $hasCellar;

    /**
     * @var string
     *
     * @ORM\Column(name="view", type="string", length=50, nullable=true)
     */
    protected $view;

    /**
     * @var string
     *
     * @ORM\Column(name="parking_fee", type="string", length=100, nullable=true)
     */
    protected $parkingFee;

    /**
     * @var string
     *
     * @ORM\Column(name="parking_fee_uom", type="string", length=25, nullable=true)
     */
    protected $parkingFeeUom;

    /**
     * @var string
     *
     * @ORM\Column(name="`condition`", type="string", length=50, nullable=true)
     */
    protected $condition;

    /**
     * @var float
     *
     * @ORM\Column(name="balcony_size", type="float", precision=10, scale=0, nullable=true)
     */
    protected $balconySize;

    /**
     * @var float
     *
     * @ORM\Column(name="garden_size", type="float", precision=10, scale=0, nullable=true)
     */
    protected $gardenSize;

    /**
     * @var string
     *
     * @ORM\Column(name="attic", type="string", length=50, nullable=true)
     */
    protected $attic;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_furniture", type="integer", nullable=true)
     */
    protected $hasFurniture;

    /**
     * @var boolean
     *
     * @ORM\Column(name="agents_can_help", type="boolean", nullable=true)
     */
    protected $agentsCanHelp;

    /**
     * @var string
     *
     * @ORM\Column(name="energy_certificate", type="string", length=50, nullable=true)
     */
    protected $energyCertificate;

    /**
     * @var string
     *
     * @ORM\Column(name="ceiling_height", type="string", nullable=true)
     */
    protected $ceilingHeight;

    /**
     * @var integer
     *
     * @ORM\Column(name="smoker", type="integer", nullable=true)
     */
    protected $smoker;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_disabled_access", type="integer", nullable=true)
     */
    protected $hasDisabledAccess;

    /**
     * @var integer
     *
     * @ORM\Column(name="mechanized", type="integer", nullable=true)
     */
    protected $mechanized;

    /**
     * @var integer
     *
     * @ORM\Column(name="pet_allowed", type="integer", nullable=true)
     */
    protected $petAllowed;

    /**
     * @var integer
     *
     * @ORM\Column(name="panel_program", type="integer", nullable=true)
     */
    protected $panelProgram;

    /**
     * @var string
     *
     * @ORM\Column(name="utility_bills", type="string", length=255, nullable=true)
     */
    protected $utilityBills;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_garden_access", type="integer", nullable=true)
     */
    protected $hasGardenAccess;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lease_rights", type="boolean", nullable=true)
     */
    protected $leaseRights;

    /**
     * @var string
     *
     * @ORM\Column(name="orientation", type="string", length=50, nullable=true)
     */
    protected $orientation;

    /**
     * @var string
     *
     * @ORM\Column(name="bath_toilet", type="string", length=50, nullable=true)
     */
    protected $bathToilet;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_ac", type="integer", nullable=true)
     */
    protected $hasAc;

    /**
     * @var string
     *
     * @ORM\Column(name="office_category", type="string", length=20, nullable=true)
     */
    protected $officeCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';


    public function __construct(
        $administrator,
        $client,
        $status,
        $type,
        $subType,
        $size,
        $lotSize,
        $county,
        $city,
        $borough,
        $street,
        $streetNp,
        $district,
        $houseNumber,
        $houseNumberNp,
        $rooms,
        $halfRooms,
        $floor,
        $levels,
        $heating,
        $comfort,
        $description,
        $hasElevator,
        $parking,
        $hasElectricity,
        $hasGas,
        $hasWater,
        $hasCellar,
        $hasDrain,
        $view,
        $parkingFee,
        $parkingFeeUom,
        $condition,
        $balconySize,
        $gardenSize,
        $attic,
        $hasFurniture,
        $agentsCanHelp,
        $energyCertificate,
        $ceilingHeight,
        $smoker,
        $hasDisabledAccess,
        $mechanized,
        $petAllowed,
        $panelProgram,
        $utilityBills,
        $hasGardenAccess,
        $leaseRights,
        $orientation,
        $bathToilet,
        $hasAc,
        $officeCategory
    )
    {
        $this->administrator = $administrator;
        $this->client = $client;
        $this->status = $status;
        $this->type = $type;
        $this->subType = $subType;
        $this->size = $size;
        $this->lotSize = $lotSize;
        $this->county = $county;
        $this->city = $city;
        $this->borough = $borough;
        $this->street = $street;
        $this->streetNp = $streetNp;
        $this->district = $district;
        $this->houseNumber = $houseNumber;
        $this->houseNumberNp = $houseNumberNp;
        $this->rooms = $rooms;
        $this->halfRooms = $halfRooms;
        $this->floor = $floor;
        $this->levels = $levels;
        $this->heating = $heating;
        $this->comfort = $comfort;
        $this->description = $description;
        $this->hasElevator = $hasElevator;
        $this->parking = $parking;
        $this->hasElectricity = $hasElectricity;
        $this->hasGas = $hasGas;
        $this->hasWater = $hasWater;
        $this->hasCellar = $hasCellar;
        $this->hasDrain = $hasDrain;
        $this->view = $view;
        $this->parkingFee = $parkingFee;
        $this->parkingFeeUom = $parkingFeeUom;
        $this->condition = $condition;
        $this->balconySize = $balconySize;
        $this->gardenSize = $gardenSize;
        $this->attic = $attic;
        $this->hasFurniture = $hasFurniture;
        $this->agentsCanHelp = $agentsCanHelp;
        $this->energyCertificate = $energyCertificate;
        $this->ceilingHeight = $ceilingHeight;
        $this->smoker = $smoker;
        $this->hasDisabledAccess = $hasDisabledAccess;
        $this->mechanized = $mechanized;
        $this->petAllowed = $petAllowed;
        $this->panelProgram = $panelProgram;
        $this->utilityBills = $utilityBills;
        $this->hasGardenAccess = $hasGardenAccess;
        $this->leaseRights = $leaseRights;
        $this->orientation = $orientation;
        $this->bathToilet = $bathToilet;
        $this->hasAc = $hasAc;
        $this->officeCategory = $officeCategory;

        $this->medias = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function update(
        $administrator,
        $client,
        $status,
        $type,
        $subType,
        $size,
        $lotSize,
        $county,
        $city,
        $borough,
        $street,
        $streetNp,
        $district,
        $houseNumber,
        $houseNumberNp,
        $rooms,
        $halfRooms,
        $floor,
        $levels,
        $heating,
        $comfort,
        $description,
        $hasElevator,
        $parking,
        $hasElectricity,
        $hasGas,
        $hasWater,
        $hasCellar,
        $hasDrain,
        $view,
        $parkingFee,
        $parkingFeeUom,
        $condition,
        $balconySize,
        $gardenSize,
        $attic,
        $hasFurniture,
        $agentsCanHelp,
        $energyCertificate,
        $ceilingHeight,
        $smoker,
        $hasDisabledAccess,
        $mechanized,
        $petAllowed,
        $panelProgram,
        $utilityBills,
        $hasGardenAccess,
        $leaseRights,
        $orientation,
        $bathToilet,
        $hasAc,
        $officeCategory
    )
    {
        $this->administrator = $administrator;
        $this->client = $client;
        $this->status = $status;
        $this->type = $type;
        $this->subType = $subType;
        $this->size = $size;
        $this->lotSize = $lotSize;
        $this->county = $county;
        $this->city = $city;
        $this->borough = $borough;
        $this->street = $street;
        $this->streetNp = $streetNp;
        $this->district = $district;
        $this->houseNumber = $houseNumber;
        $this->houseNumberNp = $houseNumberNp;
        $this->rooms = $rooms;
        $this->halfRooms = $halfRooms;
        $this->floor = $floor;
        $this->levels = $levels;
        $this->heating = $heating;
        $this->comfort = $comfort;
        $this->description = $description;
        $this->hasElevator = $hasElevator;
        $this->parking = $parking;
        $this->hasElectricity = $hasElectricity;
        $this->hasGas = $hasGas;
        $this->hasWater = $hasWater;
        $this->hasCellar = $hasCellar;
        $this->hasDrain = $hasDrain;
        $this->view = $view;
        $this->parkingFee = $parkingFee;
        $this->parkingFeeUom = $parkingFeeUom;
        $this->condition = $condition;
        $this->balconySize = $balconySize;
        $this->gardenSize = $gardenSize;
        $this->attic = $attic;
        $this->hasFurniture = $hasFurniture;
        $this->agentsCanHelp = $agentsCanHelp;
        $this->energyCertificate = $energyCertificate;
        $this->ceilingHeight = $ceilingHeight;
        $this->smoker = $smoker;
        $this->hasDisabledAccess = $hasDisabledAccess;
        $this->mechanized = $mechanized;
        $this->petAllowed = $petAllowed;
        $this->panelProgram = $panelProgram;
        $this->utilityBills = $utilityBills;
        $this->hasGardenAccess = $hasGardenAccess;
        $this->leaseRights = $leaseRights;
        $this->orientation = $orientation;
        $this->bathToilet = $bathToilet;
        $this->hasAc = $hasAc;
        $this->officeCategory = $officeCategory;

        $this->updatedAt = new \DateTime();
    }

    /**
     * Archiválás
     */
    public function archive()
    {
        $this->status = self::STATUS_ARCHIVED;
    }

    /**
     * Fotók lekérdezése
     *
     * @return ArrayCollection
     */
    public function getMedias()
    {
        return $this->medias;
    }

    /**
     * Fotó hozzáadása
     *
     * @param $path
     * @param int $listingOrder
     * @return PropertyMedia
     */
    public function addMedia($path, $listingOrder = 999999999)
    {
        $photo = new PropertyMedia($this, $path, $listingOrder);
        $this->medias->add($photo);

        return $photo;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return Borough
     */
    public function getBorough()
    {
        return $this->borough;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSubType()
    {
        return $this->subType;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getLotSize()
    {
        return $this->lotSize;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return bool
     */
    public function isStreetNp()
    {
        return $this->streetNp;
    }

    /**
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @return bool
     */
    public function isHouseNumberNp()
    {
        return $this->houseNumberNp;
    }

    /**
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @return int
     */
    public function getHalfRooms()
    {
        return $this->halfRooms;
    }

    /**
     * @return string
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @return bool
     */
    public function isLevels()
    {
        return $this->levels;
    }

    /**
     * @return bool
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * @return string
     */
    public function getHeating()
    {
        return $this->heating;
    }

    /**
     * @return string
     */
    public function getComfort()
    {
        return $this->comfort;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isHasElevator()
    {
        return $this->hasElevator;
    }

    /**
     * @return string
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * @return string
     */
    public function getHasElectricity()
    {
        return $this->hasElectricity;
    }

    /**
     * @return string
     */
    public function getHasGas()
    {
        return $this->hasGas;
    }

    /**
     * @return string
     */
    public function getHasDrain()
    {
        return $this->hasDrain;
    }

    /**
     * @return string
     */
    public function getHasWater()
    {
        return $this->hasWater;
    }

    /**
     * @return bool
     */
    public function isHasCellar()
    {
        return $this->hasCellar;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return string
     */
    public function getParkingFee()
    {
        return $this->parkingFee;
    }

    /**
     * @return string
     */
    public function getParkingFeeUom()
    {
        return $this->parkingFeeUom;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @return float
     */
    public function getBalconySize()
    {
        return $this->balconySize;
    }

    /**
     * @return float
     */
    public function getGardenSize()
    {
        return $this->gardenSize;
    }

    /**
     * @return string
     */
    public function getAttic()
    {
        return $this->attic;
    }

    /**
     * @return bool
     */
    public function isHasFurniture()
    {
        return $this->hasFurniture;
    }

    /**
     * @return bool
     */
    public function isAgentsCanHelp()
    {
        return $this->agentsCanHelp;
    }

    /**
     * @return string
     */
    public function getEnergyCertificate()
    {
        return $this->energyCertificate;
    }

    /**
     * @return bool
     */
    public function isCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * @return bool
     */
    public function getCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * @return bool
     */
    public function isSmoker()
    {
        return $this->smoker;
    }

    /**
     * @return bool
     */
    public function isHasDisabledAccess()
    {
        return $this->hasDisabledAccess;
    }

    /**
     * @return bool
     */
    public function isMechanized()
    {
        return $this->mechanized;
    }

    /**
     * @return bool
     */
    public function isPetAllowed()
    {
        return $this->petAllowed;
    }

    /**
     * @return bool
     */
    public function isPanelProgram()
    {
        return $this->panelProgram;
    }

    /**
     * @return string
     */
    public function getUtilityBills()
    {
        return $this->utilityBills;
    }

    /**
     * @return bool
     */
    public function isHasGardenAccess()
    {
        return $this->hasGardenAccess;
    }

    /**
     * @return bool
     */
    public function isLeaseRights()
    {
        return $this->leaseRights;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @return string
     */
    public function getBathToilet()
    {
        return $this->bathToilet;
    }

    /**
     * @return bool
     */
    public function isHasAc()
    {
        return $this->hasAc;
    }

    /**
     * @return string
     */
    public function getOfficeCategory()
    {
        return $this->officeCategory;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


}

