<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ORM\Table(name="client", indexes={@ORM\Index(name="fk_client_administrator_idx", columns={"administrator_id"})})
 * @ORM\Entity(
 *      repositoryClass="AppBundle\Repository\ClientRepository"
 * )
 */
class Client extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=10, nullable=true)
     */
    protected $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="id_number", type="string", length=20, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $idNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="email_type", type="string", length=10, nullable=true)
     */
    protected $emailType;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_phone", type="string", length=255, nullable=true)
     */
    protected $contact1Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_description", type="string", length=255, nullable=true)
     */
    protected $contact1Description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_type", type="string", length=10, nullable=true)
     */
    protected $contact1Type;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_phone", type="string", length=255, nullable=true)
     */
    protected $contact2Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_description", type="string", length=255, nullable=true)
     */
    protected $contact2Description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_type", type="string", length=10, nullable=true)
     */
    protected $contact2Type;

    /**
     * @var string
     *
     * @ORM\Column(name="contact3_phone", type="string", length=255, nullable=true)
     */
    protected $contact3Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact3_description", type="string", length=255, nullable=true)
     */
    protected $contact3Description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact3_type", type="string", length=10, nullable=true)
     */
    protected $contact3Type;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    protected $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administrator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     */
    protected $administrator;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contract", mappedBy="client")
     */
    protected $contracts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Property", mappedBy="client")
     */
    protected $properties;


    /**
     * Létrehoz egy ügyfelet
     *
     * @param string $firstname
     * @param string $lastname
     * @param string $zipcode
     * @param string $city
     * @param string $address
     * @param string $idNumber
     * @param string $email
     * @param string $emailType
     * @param string $contact1Phone
     * @param string $contact1Description
     * @param string $contact1Type
     * @param string $contact2Phone
     * @param string $contact2Description
     * @param string $contact2Type
     * @param string $contact3Phone
     * @param string $contact3Description
     * @param string $contact3Type
     * @param string $comment
     * @param bool $isActive
     * @param Administrator $administrator
     */
    public function __construct($firstname, $lastname, $zipcode, $city, $address, $idNumber, $email, $emailType, $contact1Phone, $contact1Description, $contact1Type, $contact2Phone, $contact2Description, $contact2Type, $contact3Phone, $contact3Description, $contact3Type, $comment, $isActive, Administrator $administrator)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->zipcode = $zipcode;
        $this->city = $city;
        $this->address = $address;
        $this->idNumber = $idNumber;
        $this->email = $email;
        $this->emailType = $emailType;
        $this->contact1Phone = $contact1Phone;
        $this->contact1Description = $contact1Description;
        $this->contact1Type = $contact1Type;
        $this->contact2Phone = $contact2Phone;
        $this->contact2Description = $contact2Description;
        $this->contact2Type = $contact2Type;
        $this->contact3Phone = $contact3Phone;
        $this->contact3Description = $contact3Description;
        $this->contact3Type = $contact3Type;
        $this->comment = $comment;
        $this->isActive = true;
        $this->administrator = $administrator;

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Módosítja az adatokat
     *
     * @param $firstname
     * @param $lastname
     * @param $zipcode
     * @param $city
     * @param $address
     * @param $idNumber
     * @param $email
     * @param $emailType
     * @param $contact1Phone
     * @param $contact1Description
     * @param $contact1Type
     * @param $contact2Phone
     * @param $contact2Description
     * @param $contact2Type
     * @param $contact3Phone
     * @param $contact3Description
     * @param $contact3Type
     * @param $comment
     * @param $isActive
     */
    public function update($firstname, $lastname, $zipcode, $city, $address, $idNumber, $email, $emailType, $contact1Phone, $contact1Description, $contact1Type, $contact2Phone, $contact2Description, $contact2Type, $contact3Phone, $contact3Description, $contact3Type, $comment, $isActive)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->zipcode = $zipcode;
        $this->city = $city;
        $this->address = $address;
        $this->idNumber = $idNumber;
        $this->email = $email;
        $this->emailType = $emailType;
        $this->contact1Phone = $contact1Phone;
        $this->contact1Description = $contact1Description;
        $this->contact1Type = $contact1Type;
        $this->contact2Phone = $contact2Phone;
        $this->contact2Description = $contact2Description;
        $this->contact2Type = $contact2Type;
        $this->contact3Phone = $contact3Phone;
        $this->contact3Description = $contact3Description;
        $this->contact3Type = $contact3Type;
        $this->comment = $comment;
        $this->isActive = $isActive;

        $this->updatedAt = new \DateTime();
    }

    /**
     * Archiválás
     */
    public function archive()
    {
        $this->firstname = substr(md5($this->firstname), 0, 10);
        $this->lastname = substr(md5($this->lastname), 0, 10);
        $this->idNumber = substr(md5($this->idNumber), 0, 10);
        $this->email = substr(md5($this->email), 0, 10);
        $this->contact1Phone = substr(md5($this->contact1Phone), 0, 10);
        $this->contact1Description = substr(md5($this->contact1Description), 0, 10);
        $this->contact2Phone = substr(md5($this->contact2Phone), 0, 10);
        $this->contact2Description = substr(md5($this->contact2Description), 0, 10);
        $this->contact3Phone = substr(md5($this->contact3Phone), 0, 10);
        $this->contact3Description = substr(md5($this->contact3Description), 0, 10);

        $this->isActive = false;
        $this->updatedAt = new \DateTime();
    }

    /**
     * Munkatárshoz rendelés
     *
     * @param Administrator $admin
     */
    public function assign(Administrator $admin)
    {
        $this->administrator = $admin;
    }

    /**
     * Adminisztrátorhoz rendelés
     * @param Administrator $administrator
     */
    public function assignToAdministrator(Administrator $administrator)
    {
        $this->administrator = $administrator;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getEmailType()
    {
        return $this->emailType;
    }

    /**
     * @return string
     */
    public function getContact1Phone()
    {
        return $this->contact1Phone;
    }

    /**
     * @return string
     */
    public function getContact1Description()
    {
        return $this->contact1Description;
    }

    /**
     * @return string
     */
    public function getContact1Type()
    {
        return $this->contact1Type;
    }

    /**
     * @return string
     */
    public function getContact2Phone()
    {
        return $this->contact2Phone;
    }

    /**
     * @return string
     */
    public function getContact2Description()
    {
        return $this->contact2Description;
    }

    /**
     * @return string
     */
    public function getContact2Type()
    {
        return $this->contact2Type;
    }

    /**
     * @return string
     */
    public function getContact3Phone()
    {
        return $this->contact3Phone;
    }

    /**
     * @return string
     */
    public function getContact3Description()
    {
        return $this->contact3Description;
    }

    /**
     * @return string
     */
    public function getContact3Type()
    {
        return $this->contact3Type;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @return mixed
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }

}



