<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="city", indexes={@ORM\Index(name="city_name_idx", columns={"name"}), @ORM\Index(name="city_county_idx", columns={"county"})})
 * @ORM\Entity
 */
class City extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="zipcode", type="integer", nullable=false)
     */
    protected $zipcode = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=50, nullable=false)
     */
    protected $county;

    /**
     * @var integer
     *
     * @ORM\Column(name="region", type="integer", nullable=true)
     */
    protected $region;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_region", type="integer", nullable=true)
     */
    protected $subRegion;

    /**
     * @var integer
     *
     * @ORM\Column(name="region1", type="integer", nullable=true)
     */
    protected $region1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="list", type="boolean", nullable=true)
     */
    protected $list = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=6, nullable=true)
     */
    protected $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", precision=10, scale=6, nullable=true)
     */
    protected $lng;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @return int
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return int
     */
    public function getSubRegion()
    {
        return $this->subRegion;
    }

    /**
     * @return int
     */
    public function getRegion1()
    {
        return $this->region1;
    }

    /**
     * @return bool
     */
    public function isList()
    {
        return $this->list;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



}

