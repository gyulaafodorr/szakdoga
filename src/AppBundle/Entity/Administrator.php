<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Administrator
 *
 * @ORM\Table(name="administrator")
 * @ORM\Entity
 */
class Administrator extends AbstractEntity
{
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    const STATUS_ARCHIVED = 'ARCHIVED';

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const ROLE_SYSTEM_ADMIN = 'ROLE_SYSTEM_ADMIN';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     * @Assert\Email(
     *     message = "A megadott email cím '{{ value }}' hibás.",
     *     checkMX = false
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=200, nullable=true)
     */
    protected $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="phone3", type="string", length=200, nullable=true)
     */
    protected $phone3;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="text", length=255, nullable=true)
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=30, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $role;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=30, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * Létrehoz egy adminisztrátort.
     *
     * @param string $username
     * @param string $email
     * @param string $phone
     * @param string $phone2
     * @param string $phone3
     * @param string $name
     * @param string $password
     * @param string $role
     * @param string $status
     */
    public function __construct($name, $username, $email, $phone, $phone2 = null, $phone3 = null, $password, $role, $status)
    {
        $this->username = $username;
        $this->email = $email;
        $this->phone = $phone;
        $this->phone2 = $phone2;
        $this->phone3 = $phone3;
        $this->name = $name;
        $this->password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 13));
        $this->role = $role;
        $this->status = $status;

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Módosítja az adatokat.
     *
     * @param $name
     * @param $username
     * @param $email
     * @param $phone
     * @param $phone2
     * @param $phone3
     * @param $password
     * @param $role
     * @param $status
     */
    public function update($name, $username, $email, $phone, $phone2 = null, $phone3 = null, $password = null, $role, $status)
    {
        $this->username = $username;
        $this->email = $email;
        $this->phone = $phone;
        $this->phone2 = $phone2;
        $this->phone3 = $phone3;
        $this->name = $name;
        if ($password) {
            $this->password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 13));
        }
        $this->role = $role;
        $this->status = $status;

        $this->updatedAt = new \DateTime();
    }

    /**
     * Inaktiválás
     */
    public function inactivate()
    {
        $this->status = self::STATUS_INACTIVE;
    }

    /**
     * Aktiválás
     */
    public function activate()
    {
        $this->status = self::STATUS_ACTIVE;
    }


    /**
     * Archiválás
     */
    public function archive()
    {
        $this->status = self::STATUS_ARCHIVED;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * @return string
     */
    public function getPhone3()
    {
        return $this->phone3;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}

