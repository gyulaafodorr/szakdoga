<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contract
 *
 * @ORM\Table(name="contract", indexes={@ORM\Index(name="fk_pro_client_idx", columns={"client_id"}), @ORM\Index(name="fk_pro_administrator_idx", columns={"administrator_id"})})
 * @ORM\Entity(
 *      repositoryClass="AppBundle\Repository\ContractRepository"
 * )
 */
class Contract extends AbstractEntity
{
    const STATUS_ACTIVE = 1;
    const STATUS_ENDED = 2;
    const STATUS_ARCHIVED = 3;

    const RESULT_SUCCESS = 1;
    const RESULT_FAILURE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    protected $client;


    /**
     * @var \AppBundle\Entity\Property
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Property")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     * })
     */
    protected $property;

    /**
     * @var \AppBundle\Entity\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administrator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     */
    protected $administrator;

    /**
     * @var string
     *
     * @ORM\Column(name="contract_number", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $contractNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="commission", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $commission;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    protected $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="result", type="integer", nullable=true)
     */
    protected $result;

    /**
     * @var integer
     *
     * @ORM\Column(name="agreement_type", type="integer", nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $agreementType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_exclusive", type="boolean", nullable=true)
     */
    protected $isExclusive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", nullable=true)
     */
    protected $deadline;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="bigint", nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_eur", type="bigint", nullable=true)
     */
    protected $priceEur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * Létrehoz egy megbízást
     *
     * @param string $contractNumber
     * @param string $commission
     * @param string $comment
     * @param int $agreementType
     * @param bool $isExclusive
     * @param \DateTime $deadline
     * @param int $price
     * @param int $priceEur
     * @param Client $client
     * @param Administrator $administrator
     */
    public function __construct($contractNumber, $commission, $comment, $agreementType, $isExclusive, $deadline, $price, $priceEur, Client $client, Administrator $administrator)
    {
        $this->contractNumber = $contractNumber;
        $this->commission = $commission;
        $this->comment = $comment;
        $this->status = self::STATUS_ACTIVE;
        $this->agreementType = $agreementType;
        $this->isExclusive = $isExclusive;
        $this->deadline = $deadline;
        $this->price = $price;
        $this->priceEur = $priceEur;
        $this->client = $client;
        $this->administrator = $administrator;

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }


    /**
     * Módosít egy megbízást
     *
     * @param $contractNumber
     * @param $commission
     * @param $comment
     * @param $agreementType
     * @param $isExclusive
     * @param \DateTime $deadline
     * @param $price
     * @param $priceEur
     * @param Client $client
     * @param Administrator $administrator
     */
    public function update($contractNumber, $commission, $comment, $agreementType, $isExclusive, $deadline, $price, $priceEur, Client $client, Administrator $administrator)
    {
        $this->contractNumber = $contractNumber;
        $this->commission = $commission;
        $this->comment = $comment;
        $this->agreementType = $agreementType;
        $this->isExclusive = $isExclusive;
        $this->deadline = $deadline;
        $this->price = $price;
        $this->priceEur = $priceEur;
        $this->client = $client;
        $this->administrator = $administrator;

        $this->updatedAt = new \DateTime();
    }

    /**
     * Adminisztrátorhoz rendelés
     *
     * @param Administrator $administrator
     */
    public function assignToAdministrator(Administrator $administrator)
    {
        $this->administrator = $administrator;
    }

    /**
     * Ingatlan hozzárendelése
     *
     * @param Property $property
     */
    public function assignProperty(Property $property)
    {
        $this->property = $property;
    }

    /**
     * Archiválás
     */
    public function archive()
    {
        $this->status = self::STATUS_ARCHIVED;
        $this->comment = '';

        $this->updatedAt = new \DateTime();
    }

    /**
     * Aktiválás
     */
    public function activate()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->result = null;

        $this->updatedAt = new \DateTime();
    }

    /**
     * Zárás.
     *
     * @param $result
     */
    public function close($result)
    {
        $this->result = $result;
        $this->status = self::STATUS_ENDED;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @return string
     */
    public function getContractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isResult()
    {
        return $this->result;
    }


    /**
     * @return bool
     */
    public function getResult()
    {
        return $this->result;
    }


    /**
     * @return int
     */
    public function getAgreementType()
    {
        return $this->agreementType;
    }

    /**
     * @return bool
     */
    public function isExclusive()
    {
        return $this->isExclusive;
    }

    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getPriceEur()
    {
        return $this->priceEur;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


}

