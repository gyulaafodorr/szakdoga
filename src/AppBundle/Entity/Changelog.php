<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Changelog
 *
 * @ORM\Table(name="changelog", indexes={
 *     @ORM\Index(name="pc_field_idx", columns={"field"}),
 *     @ORM\Index(name="pc_admin_idx", columns={"administrator_id"})
 * })
 * @ORM\Entity
 */
class Changelog extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Administrator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $administrator;


    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $field;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="text", length=65535, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $entityId;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_name", type="text", length=255, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $entityName;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="text", length=65535, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $newValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed_at", type="datetime", nullable=false)
     */
    protected $changedAt;


    public function __construct($administrator, $entityId, $entityName, $field, $oldValue, $newValue)
    {
        $this->administrator = $administrator;
        $this->entityId = $entityId;
        $this->entityName = $entityName;
        $this->field = $field;
        $this->oldValue = $oldValue;
        $this->newValue = $newValue;
        $this->changedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @return string
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }


}

