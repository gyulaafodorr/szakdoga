<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 12:20
 */

namespace AppBundle\Entity;


abstract class AbstractEntity
{
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public final function getClass()
    {
        $class = explode("\\", get_class($this));
        return array_pop($class);
    }

    /**
     * @return string
     */
    public final function getNamespace()
    {
        $class = explode("\\", get_class($this));
        array_pop($class);
        return implode("\\", $class);
    }
}