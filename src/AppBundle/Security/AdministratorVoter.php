<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 08.
 * Time: 14:12
 */

namespace AppBundle\Security;

use AppBundle\Dto\Administrator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AdministratorVoter extends Voter
{
    const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT))) {
            return false;
        }

        if (!$subject instanceof Administrator) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof Administrator) {
            return false;
        }

        /** @var Administrator $administrator */
        $administrator = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($administrator, $user);
        }

        throw new \LogicException('Ezt a kódot nem szabad elérni!');
    }

    private function canEdit(Administrator $administrator, Administrator $user)
    {
        return $user->getRole() == Administrator::ROLE_SUPER_ADMIN or $user->getRole() == Administrator::ROLE_SYSTEM_ADMIN;
    }
}