<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 09.
 * Time: 23:10
 */

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @var \Twig_Environment
     */
    private $templating;

    public function __construct($templating)
    {
        $this->templating = $templating;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {

        return new Response($this->templating->render('admin/403.html.twig'), 403);
    }
}