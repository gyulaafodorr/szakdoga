<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 08.
 * Time: 14:12
 */

namespace AppBundle\Security;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Contract;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContractVoter extends Voter
{
    const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT))) {
            return false;
        }

        if (!$subject instanceof Contract) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof Administrator) {
            return false;
        }

        /** @var Contract $contract */
        $contract = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($contract, $user);
        }

        throw new \LogicException('Ezt a kódot nem szabad elérni!');
    }

    private function canEdit(Contract $contract, Administrator $user)
    {
        return $user === $contract->getAdministrator() or $user->getRole() == Administrator::ROLE_SUPER_ADMIN;
    }
}