<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 08.
 * Time: 14:12
 */

namespace AppBundle\Security;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Client;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ClientVoter extends Voter
{
    const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT))) {
            return false;
        }

        if (!$subject instanceof Client) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof Administrator) {
            return false;
        }

        /** @var Client $client */
        $client = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($client, $user);
        }

        throw new \LogicException('Ezt a kódot nem szabad elérni!');
    }

    private function canEdit(Client $client, Administrator $user)
    {
        return $user === $client->getAdministrator() or $user->getRole() == Administrator::ROLE_SUPER_ADMIN;
    }
}