<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 03.
 * Time: 12:28
 */

namespace AppBundle\Repository;

use AppBundle\Dto\Client;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\EntityRepository;

class ClientDtoRepository extends EntityRepository
{
    public function getAll($filter = array(), $order, $currentPage = 1, $limit = PaginatorHelper::LIMIT)
    {
        $q = $this->createQueryBuilder('c')
            ->select('c');

        if (isset($filter['hasProperty']) && $filter['hasProperty']) {
            $q->innerJoin('c.properties', 'p')
                ->addSelect('p');
        }
        else {
            $q->leftJoin('c.properties', 'p')
                ->addSelect('p');
        }

        if (isset($filter['searchArchives']) && $filter['searchArchives']) {
            $q->andWhere('c.isActive IN (:isActive)')
                ->setParameter('isActive', [true, false]);
        }
        else {
            $q->andWhere('c.isActive IN (:isActive)')
                ->setParameter('isActive', true);
        }

        if (!empty($filter)) {
            if (isset($filter['administrator']) && $filter['administrator']) {
                $q->leftJoin('p.administrator', 'pa')
                    ->andWhere('pa.id = :administrator')
                    ->setParameter('administrator', $filter['administrator']);
            }
            if (isset($filter['name']) && $filter['name']) {
                $q->andWhere('CONCAT(c.lastname, \' \', c.firstname) LIKE :name')
                    ->setParameter('name', '%' . $filter['name'] . '%');
            }
            if (isset($filter['email']) && $filter['email']) {
                $q->andWhere('c.email LIKE :email')
                    ->setParameter('email', '%' . $filter['email'] . '%');
            }
            if (isset($filter['city']) && $filter['city']) {
                $q->andWhere('c.city LIKE :city')
                    ->setParameter('city', '%' . $filter['city'] . '%');
            }
            if (isset($filter['idNumber']) && $filter['idNumber']) {
                $q->andWhere('c.idNumber LIKE :idNumber')
                    ->setParameter('idNumber', '%' . $filter['idNumber'] . '%');
            }
            if (isset($filter['isActive']) && $filter['isActive']) {
                $q->andWhere('c.isActive = 1');
            }
            if (isset($filter['isBuyer']) && $filter['isBuyer']) {
                $q->andWhere('c.isBuyer = 1');
            }
            if (isset($filter['buyerIsActive']) && $filter['buyerIsActive']) {
                $q->andWhere('c.buyerIsActive = 1');
            }
            if (isset($filter['phone']) && $filter['phone']) {
                $q->andWhere('c.contact1Phone LIKE :phone OR c.contact2Phone LIKE :phone OR c.contact3Phone LIKE :phone')
                    ->setParameter('phone', '%' . $filter['phone'] . '%');
            }
            if (isset($filter['contact']) && $filter['contact']) {
                $q->andWhere('c.contact2Description LIKE :contact OR c.contact3Description LIKE :contact')
                    ->setParameter('contact', '%' . $filter['contact'] . '%');
            }
            if (isset($filter['zipcode']) && $filter['zipcode']) {
                $q->andWhere('c.zipcode = :zipcode')
                    ->setParameter('zipcode', $filter['zipcode']);
            }
            if (isset($filter['address']) && $filter['address']) {
                $q->andWhere('c.address LIKE :address')
                    ->setParameter('address', '%' . $filter['address'] . '%');
            }
            if (isset($filter['comment']) && $filter['comment']) {
                $q->andWhere('c.comment LIKE :comment')
                    ->setParameter('comment', '%' . $filter['comment'] . '%');
            }
            if (isset($filter['createdAtFrom']) && $filter['createdAtFrom']) {
                $q->andWhere('c.createdAt >= :createdAtFrom')
                    ->setParameter('createdAtFrom', $filter['createdAtFrom']);
            }
            if (isset($filter['createdAtTo']) && $filter['createdAtTo']) {
                $q->andWhere('c.createdAt <= :createdAtTo')
                    ->setParameter('createdAtTo', $filter['createdAtTo']);
            }
        }

        if (!empty($order) && isset($order['order']) && isset($order['direction'])) {
            if ($order['order'] == 'c.name') {
                $q->addOrderBy('c.lastname', $order['direction']);
                $q->addOrderBy('c.firstname', $order['direction']);
            }
            else {
                $q->orderBy($order['order'], $order['direction']);
            }
        }
        else {
            $q->addOrderBy('c.lastname');
            $q->addOrderBy('c.firstname');
        }

        return PaginatorHelper::paginate($q->getQuery(), $currentPage, $limit);
    }

}