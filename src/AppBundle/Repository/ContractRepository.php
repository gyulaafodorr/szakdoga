<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 03.
 * Time: 12:28
 */

namespace AppBundle\Repository;

use AppBundle\Dto\Client;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\EntityRepository;

class ContractRepository extends EntityRepository
{
    /**
     * @param $id
     * @return array
     */
    public function getAllByAdministrator($id)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.administrator', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

}