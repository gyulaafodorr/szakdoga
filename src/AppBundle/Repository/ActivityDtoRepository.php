<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 15:53
 */

namespace AppBundle\Repository;

use AppBundle\Dto\Contract;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ActivityDtoRepository extends EntityRepository
{
    /**
     * @param $clientId
     * @return array
     */
    public function getAllByClient($clientId, $contractId)
    {
        $q = $this->createQueryBuilder('a')
            ->select('a')
            ->innerJoin('a.client', 'c')
            ->where('c.id = :clientId')
            ->setParameter('clientId', $clientId);

        if ($contractId) {
            $q->innerJoin('a.contract', 'co')
                ->andWhere('co.id = :contractId')
                ->setParameter('contractId', $contractId);
        }
        $q->addOrderBy('a.createdAt', 'DESC');

        return $q->getQuery()->getResult();
    }


}