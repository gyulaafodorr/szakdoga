<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 04.
 * Time: 12:29
 */

namespace AppBundle\Repository;

use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\EntityRepository;

class ContractDtoRepository extends EntityRepository
{
    public function getAll($filter = array(), $order = array(), $currentPage = 1, $limit = PaginatorHelper::LIMIT)
    {
        $q = $this->createQueryBuilder('p')
            ->select('p');

        if (!empty($filter)) {
            if (isset($filter['id']) && $filter['id']) {
                $q->andWhere('p.id = :id')
                    ->setParameter('id', $filter['id']);
            }
            elseif (isset($filter['contractNumber']) && $filter['contractNumber']) {
                $q->andWhere('p.contractNumber = :contractNumber')
                    ->setParameter('contractNumber', $filter['contractNumber']);
            }
            else {
                if (isset($filter['administrator']) && $filter['administrator']) {
                    $q->andWhere('a.id = :administrator')
                        ->setParameter('administrator', $filter['administrator']);
                }

                if (isset($filter['status']) && $filter['status']) {
                    $q->andWhere('p.status = :status')
                        ->setParameter('status', $filter['status']);
                }

                if (isset($filter['clientFirstname']) && $filter['clientFirstname']) {
                    $q->andWhere('cl.firstname LIKE :clientFirstname')
                        ->setParameter('clientFirstname', '%' . $filter['clientFirstname'] . '%');
                }

                if (isset($filter['clientLastname']) && $filter['clientLastname']) {
                    $q->andWhere('cl.lastname LIKE :clientLastname')
                        ->setParameter('clientLastname', '%' . $filter['clientLastname'] . '%');
                }
                if (isset($filter['agreementType']) && $filter['agreementType']) {
                    $q->andWhere('p.agreementType = :agreementType')
                        ->setParameter('agreementType', $filter['agreementType']);

                    if ($filter['agreementType'] == AgreementTypeEnum::SALE) {
                        if (isset($filter['priceFrom']) && $filter['priceFrom'] != "") {
                            $q->andWhere('p.price >= :priceFrom')
                                ->setParameter('priceFrom', $filter['priceFrom']);
                        }
                        if (isset($filter['priceTo']) && $filter['priceTo']) {
                            $q->andWhere('p.price <= :priceTo')
                                ->setParameter('priceTo', $filter['priceTo']);
                        }
                    }
                    elseif ($filter['agreementType'] == AgreementTypeEnum::RENT) {
                        if (isset($filter['priceFrom']) && $filter['priceFrom'] != "") {
                            $q->andWhere('p.price >= :priceFrom')
                                ->setParameter('priceFrom', $filter['priceFrom']);
                        }
                        if (isset($filter['priceTo']) && $filter['priceTo']) {
                            $q->andWhere('p.price <= :priceTo')
                                ->setParameter('priceTo', $filter['priceTo']);
                        }
                    }

                    if (isset($filter['priceEurFrom']) && $filter['priceEurFrom'] != "") {
                        $q->andWhere('p.priceEur >= :priceEurFrom')
                            ->setParameter('priceEurFrom', $filter['priceEurFrom']);
                    }
                    if (isset($filter['priceEurTo']) && $filter['priceEurTo']) {
                        $q->andWhere('p.priceEur <= :priceEurTo')
                            ->setParameter('priceEurTo', $filter['priceEurTo']);
                    }
                }
                if (isset($filter['isExclusive']) && $filter['isExclusive']) {
                    $q->andWhere('p.isExclusive = 1');
                }

            }
        }

        return PaginatorHelper::paginate($q->getQuery(), $currentPage, $limit);
    }
}