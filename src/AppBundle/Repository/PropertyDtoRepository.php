<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 03.
 * Time: 12:28
 */

namespace AppBundle\Repository;

use AppBundle\Dto\Client;
use AppBundle\Dto\Property;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\EntityRepository;

class PropertyDtoRepository extends EntityRepository
{
    public function getAll($filter = array(), $order, $currentPage = 1, $limit = PaginatorHelper::LIMIT)
    {
        $q = $this->createQueryBuilder('p')
            ->select('p,c,a,cl')
//            ->addSelect('p.rooms + p.halfRooms')
            ->innerJoin('p.city', 'c')
            ->innerJoin('p.administrator', 'a')
            ->innerJoin('p.client', 'cl');

        if (!empty($filter)) {
            if (isset($filter['id']) && $filter['id']) {
                $q->andWhere('p.id = :id')
                    ->setParameter('id', $filter['id']);
            }
            else {

                if (isset($filter['administrator']) && $filter['administrator']) {
                    $q->andWhere('a.id = :administrator')
                        ->setParameter('administrator', $filter['administrator']);
                }
                if (isset($filter['status']) && $filter['status']) {
                    $q->andWhere('p.status = :status')
                        ->setParameter('status', $filter['status']);
                }
                if (isset($filter['condition']) && $filter['condition']) {
                    $q->andWhere('p.condition = :condition')
                        ->setParameter('condition', $filter['condition']);
                }
                if (isset($filter['type']) && $filter['type']) {
                    $q->andWhere('p.type = :type')
                        ->setParameter('type', $filter['type']);
                }
                if (isset($filter['subType']) && $filter['subType']) {
                    $q->andWhere('p.subType = :subType')
                        ->setParameter('subType', $filter['subType']);
                }
                if (isset($filter['district']) && $filter['district']) {
                    $q->andWhere('p.district IN (:district)')
                        ->setParameter('district', $filter['district']);
                }
                if (isset($filter['city']) && $filter['city']) {
                    $q->andWhere('c.name LIKE :city')
                        ->setParameter('city', '%' . $filter['city'] . '%');
                }
                if (isset($filter['street']) && $filter['street']) {
                    $q->andWhere('p.street LIKE :street')
                        ->setParameter('street', '%' . $filter['street'] . '%');
                }
                if (isset($filter['clientFirstname']) && $filter['clientFirstname']) {
                    $q->andWhere('cl.firstname LIKE :clientFirstname')
                        ->setParameter('clientFirstname', '%' . $filter['clientFirstname'] . '%');
                }

                if (isset($filter['clientLastname']) && $filter['clientLastname']) {
                    $q->andWhere('cl.lastname LIKE :clientLastname')
                        ->setParameter('clientLastname', '%' . $filter['clientLastname'] . '%');
                }

                if (isset($filter['sizeFrom']) && $filter['sizeFrom']) {
                    $q->andWhere('p.size >= :sizeFrom')
                        ->setParameter('sizeFrom', $filter['sizeFrom']);
                }
                if (isset($filter['sizeTo']) && $filter['sizeTo']) {
                    $q->andWhere('p.size <= :sizeTo')
                        ->setParameter('sizeTo', $filter['sizeTo']);
                }
                if (isset($filter['lotSizeFrom']) && $filter['lotSizeFrom']) {
                    $q->andWhere('p.lotSize >= :lotSizeFrom')
                        ->setParameter('lotSizeFrom', $filter['lotSizeFrom']);
                }
                if (isset($filter['lotSizeTo']) && $filter['lotSizeTo']) {
                    $q->andWhere('p.lotSize <= :lotSizeTo')
                        ->setParameter('lotSizeTo', $filter['lotSizeTo']);
                }
                if (isset($filter['roomsFrom']) && $filter['roomsFrom']) {
                    $q->andWhere('(CASE WHEN(p.rooms IS NULL) THEN 0 ELSE p.rooms END) + (CASE WHEN(p.halfRooms IS NULL) THEN 0 ELSE p.halfRooms END) >= :roomsFrom')
                        ->setParameter('roomsFrom', $filter['roomsFrom']);
                }
                if (isset($filter['roomsTo']) && $filter['roomsTo']) {
                    $q->andWhere('(CASE WHEN(p.rooms IS NULL) THEN 0 ELSE p.rooms END) + (CASE WHEN(p.halfRooms IS NULL) THEN 0 ELSE p.halfRooms END) <= :roomsTo')
                        ->setParameter('roomsTo', $filter['roomsTo']);
                }

                if (isset($filter['floorFrom'])) {
                    $q->andWhere('p.floor IS NOT NULL AND p.floor >= :floorFrom')
                        ->setParameter('floorFrom', $filter['floorFrom']);
                }
                if (isset($filter['floorTo'])) {
                    $q->andWhere('p.floor IS NOT NULL AND p.floor <= :floorTo')
                        ->setParameter('floorTo', $filter['floorTo']);
                }
            }
        }
        else {
            $q->andWhere('p.status = :status')
                ->setParameter('status', Property::STATUS_ACTIVE);
        }

        if (!empty($order) && isset($order['order']) && isset($order['direction'])) {
            $q->addOrderBy($order['order'], $order['direction']);
            if ($order['order'] == 'c.name') {
                $q->addOrderBy('p.district', $order['direction']);
            }
        }
        else {
            $q->addOrderBy('p.createdAt', 'DESC');
        }

        return PaginatorHelper::paginate($q->getQuery(), $currentPage, $limit);
    }
}