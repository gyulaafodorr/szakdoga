<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 15:53
 */

namespace AppBundle\Repository;

use AppBundle\Dto\Contract;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AdministratorDtoRepository extends EntityRepository implements UserProviderInterface
{
    public function getAll($filters = array(), $currentPage = 1, $limit = PaginatorHelper::LIMIT)
    {
        $q = $this->createQueryBuilder('a')
            ->select('a,p,c')
            ->leftJoin('a.contracts', 'p')
            ->leftJoin('a.clients', 'c')
            ->addOrderBy('a.status')
            ->addOrderBy('a.name');

        // if we have filter, apply
        if (!empty($filter)) {

        }

        return PaginatorHelper::paginate($q->getQuery(), $currentPage, $limit);
    }

    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.email = :username')
            ->setParameter('username', $username)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Nincs ilyen adminisztrátor "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }

}