<?php

namespace AppBundle\Dto;

use AppBundle\Dto\AbstractDto;
use Doctrine\ORM\Mapping as ORM;

/**
 * Borough
 *
 * @ORM\Table(name="borough", indexes={@ORM\Index(name="fk_bor_city_idx", columns={"city_id"})})
 * @ORM\Entity
 */
class Borough extends AbstractDto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="district", type="integer", nullable=true)
     */
    protected $district;

    /**
     * @var string
     *
     * @ORM\Column(name="borough", type="string", length=255, nullable=false)
     */
    protected $borough;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Dto\City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    protected $city;

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param int $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getBorough()
    {
        return $this->borough;
    }

    /**
     * @param string $borough
     */
    public function setBorough($borough)
    {
        $this->borough = $borough;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return City
     */
    public function getCity2()
    {
        return $this->city2;
    }

    /**
     * @param City $city2
     */
    public function setCity2($city2)
    {
        $this->city2 = $city2;
    }


}

