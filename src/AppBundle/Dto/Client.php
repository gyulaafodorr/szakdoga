<?php

namespace AppBundle\Dto;

use AppBundle\Dto\AbstractDto;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\PhoneTypeEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Client
 *
 * @ORM\Table(name="client", indexes={@ORM\Index(name="fk_client_administrator_idx", columns={"administrator_id"})})
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\ClientDtoRepository"
 * )
 */
class Client extends AbstractDto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Dto\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Administrator", inversedBy="clients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     */
    protected $administrator;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Dto\Contract", mappedBy="client")
     */
    protected $contracts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Dto\Property", mappedBy="client")
     */
    protected $properties;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=10, nullable=true)
     */
    protected $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="id_number", type="string", length=20, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $idNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="email_type", type="string", length=10, nullable=true)
     */
    protected $emailType;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_phone", type="string", length=255, nullable=true)
     */
    protected $contact1Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_description", type="string", length=255, nullable=true)
     */
    protected $contact1Description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_type", type="string", length=10, nullable=true)
     */
    protected $contact1Type;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_phone", type="string", length=255, nullable=true)
     */
    protected $contact2Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_description", type="string", length=255, nullable=true)
     */
    protected $contact2Description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_type", type="string", length=10, nullable=true)
     */
    protected $contact2Type;

    /**
     * @var string
     *
     * @ORM\Column(name="contact3_phone", type="string", length=255, nullable=true)
     */
    protected $contact3Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact3_description", type="string", length=255, nullable=true)
     */
    protected $contact3Description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact3_type", type="string", length=10, nullable=true)
     */
    protected $contact3Type;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    protected $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    protected $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @return string
     */
    public function getStatusFormatted()
    {
        return ($this->isActive) ? "Aktív" : "Archivált";
    }

    /**
     * @return string
     */
    public function getAddressFormatted()
    {
        $address = $this->zipcode . " " . $this->city . ", " . $this->address;
        return $address;
    }

    /**
     * @return string
     */
    public function getEmailFormatted()
    {
        $map = EmailTypeEnum::getLabelConstMap();
        if (!is_null($this->emailType)) {
            $email = $this->email . " - " . $map[$this->emailType];
        }
        else {
            $email = $this->email;
        }
        return $email;
    }

    /**
     * @return string
     */
    public function getContact1Formatted()
    {
        $map = PhoneTypeEnum::getLabelConstMap();
        if (!is_null($this->contact1Type)) {
            $contact = $this->contact1Description . ": " . $this->contact1Phone . " - " . $map[$this->contact1Type];
        }
        else {
            $contact = $this->contact1Description . ": " . $this->contact1Phone;
        }
        return $contact;
    }

    /**
     * @return string
     */
    public function getContact2Formatted()
    {
        $map = PhoneTypeEnum::getLabelConstMap();
        if (!is_null($this->contact2Type)) {
            $contact = $this->contact2Description . ": " . $this->contact2Phone . " - " . $map[$this->contact2Type];
        }
        else {
            $contact = $this->contact2Description . ": " . $this->contact2Phone;
        }
        return $contact;
    }

    /**
     * @return string
     */
    public function getContact3Formatted()
    {
        $map = PhoneTypeEnum::getLabelConstMap();
        if (!is_null($this->contact3Type)) {
            $contact = $this->contact3Description . ": " . $this->contact3Phone . " - " . $map[$this->contact3Type];
        }
        else {
            $contact = $this->contact3Description . ": " . $this->contact3Phone;
        }
        return $contact;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * @param string $idNumber
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmailType()
    {
        return $this->emailType;
    }

    /**
     * @param string $emailType
     */
    public function setEmailType($emailType)
    {
        $this->emailType = $emailType;
    }

    /**
     * @return string
     */
    public function getContact1Phone()
    {
        return $this->contact1Phone;
    }

    /**
     * @param string $contact1Phone
     */
    public function setContact1Phone($contact1Phone)
    {
        $this->contact1Phone = $contact1Phone;
    }

    /**
     * @return string
     */
    public function getContact1Description()
    {
        return $this->contact1Description;
    }

    /**
     * @param string $contact1Description
     */
    public function setContact1Description($contact1Description)
    {
        $this->contact1Description = $contact1Description;
    }

    /**
     * @return string
     */
    public function getContact1Type()
    {
        return $this->contact1Type;
    }

    /**
     * @param string $contact1Type
     */
    public function setContact1Type($contact1Type)
    {
        $this->contact1Type = $contact1Type;
    }

    /**
     * @return string
     */
    public function getContact2Phone()
    {
        return $this->contact2Phone;
    }

    /**
     * @param string $contact2Phone
     */
    public function setContact2Phone($contact2Phone)
    {
        $this->contact2Phone = $contact2Phone;
    }

    /**
     * @return string
     */
    public function getContact2Description()
    {
        return $this->contact2Description;
    }

    /**
     * @param string $contact2Description
     */
    public function setContact2Description($contact2Description)
    {
        $this->contact2Description = $contact2Description;
    }

    /**
     * @return string
     */
    public function getContact2Type()
    {
        return $this->contact2Type;
    }

    /**
     * @param string $contact2Type
     */
    public function setContact2Type($contact2Type)
    {
        $this->contact2Type = $contact2Type;
    }

    /**
     * @return string
     */
    public function getContact3Phone()
    {
        return $this->contact3Phone;
    }

    /**
     * @param string $contact3Phone
     */
    public function setContact3Phone($contact3Phone)
    {
        $this->contact3Phone = $contact3Phone;
    }

    /**
     * @return string
     */
    public function getContact3Description()
    {
        return $this->contact3Description;
    }

    /**
     * @param string $contact3Description
     */
    public function setContact3Description($contact3Description)
    {
        $this->contact3Description = $contact3Description;
    }

    /**
     * @return string
     */
    public function getContact3Type()
    {
        return $this->contact3Type;
    }

    /**
     * @param string $contact3Type
     */
    public function setContact3Type($contact3Type)
    {
        $this->contact3Type = $contact3Type;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isIsActive()
    {
        return $this->isActive;
    }


    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @param Administrator $administrator
     */
    public function setAdministrator($administrator)
    {
        $this->administrator = $administrator;
    }

    public function __toString()
    {
        return $this->lastname . ' ' . $this->firstname;
    }

    /**
     * @return mixed
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * @param mixed $contracts
     */
    public function setContracts($contracts)
    {
        $this->contracts = $contracts;
    }

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }

    /**
     * @return mixed
     */
    public function getEmailTypeFormatted()
    {
        return $this->getEmailType() ? EmailTypeEnum::getLabelByConst($this->getEmailType()) : $this->getEmailType();
    }

    /**
     * @return mixed
     */
    public function getContact1TypeFormatted()
    {
        return $this->contact1Type ? PhoneTypeEnum::getLabelByConst($this->contact1Type) : $this->contact1Type;
    }

    /**
     * @return mixed
     */
    public function getContact2TypeFormatted()
    {
        return $this->contact2Type ? PhoneTypeEnum::getLabelByConst($this->contact2Type) : $this->contact2Type;
    }

    /**
     * @return mixed
     */
    public function getContact3TypeFormatted()
    {
        return $this->contact3Type ? PhoneTypeEnum::getLabelByConst($this->contact3Type) : $this->contact3Type;
    }
}
