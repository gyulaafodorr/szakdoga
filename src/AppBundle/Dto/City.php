<?php

namespace AppBundle\Dto;

use AppBundle\Dto\AbstractDto;
use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="city", indexes={@ORM\Index(name="city_name_idx", columns={"name"}), @ORM\Index(name="city_county_idx", columns={"county"})})
 * @ORM\Entity
 */
class City extends AbstractDto
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="zipcode", type="integer", nullable=false)
     */
    protected $zipcode = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=50, nullable=false)
     */
    protected $county;

    /**
     * @var integer
     *
     * @ORM\Column(name="region", type="integer", nullable=true)
     */
    protected $region;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub_region", type="integer", nullable=true)
     */
    protected $subRegion;

    /**
     * @var integer
     *
     * @ORM\Column(name="region1", type="integer", nullable=true)
     */
    protected $region1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="list", type="boolean", nullable=true)
     */
    protected $list = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=6, nullable=true)
     */
    protected $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", precision=10, scale=6, nullable=true)
     */
    protected $lng;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param int $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return int
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param int $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return int
     */
    public function getSubRegion()
    {
        return $this->subRegion;
    }

    /**
     * @param int $subRegion
     */
    public function setSubRegion($subRegion)
    {
        $this->subRegion = $subRegion;
    }

    /**
     * @return int
     */
    public function getRegion1()
    {
        return $this->region1;
    }

    /**
     * @param int $region1
     */
    public function setRegion1($region1)
    {
        $this->region1 = $region1;
    }

    /**
     * @return bool
     */
    public function isList()
    {
        return $this->list;
    }

    /**
     * @param bool $list
     */
    public function setList($list)
    {
        $this->list = $list;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}

