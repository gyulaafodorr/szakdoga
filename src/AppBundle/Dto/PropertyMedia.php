<?php

namespace AppBundle\Dto;

use AppBundle\Dto\AbstractDto;
use Doctrine\ORM\Mapping as ORM;

/**
 * PropertyMedia
 *
 * @ORM\Table(name="property_media", indexes={
 *     @ORM\Index(name="fk_pm_property_idx", columns={"property_id"}),
 *     @ORM\Index(name="pm_type_2_idx", columns={"property_id", "is_public", "listing_order"}),
 *     @ORM\Index(name="pm_type_3_idx", columns={"property_id", "is_public"}),
 *     @ORM\Index(name="pm_type_4_idx", columns={"property_id", "listing_order"})
 * })
 * @ORM\Entity
 */
class PropertyMedia extends AbstractDto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Dto\Property
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Property")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     * })
     */
    protected $property;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255, nullable=true)
     */
    protected $src;

    /**
     * @var integer
     *
     * @ORM\Column(name="listing_order", type="bigint", nullable=true)
     */
    protected $listingOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_public", type="boolean", nullable=false)
     */
    protected $isPublic = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param string $src
     */
    public function setSrc($src)
    {
        $this->src = $src;
    }

    /**
     * @return int
     */
    public function getListingOrder()
    {
        return $this->listingOrder;
    }

    /**
     * @param int $listingOrder
     */
    public function setListingOrder($listingOrder)
    {
        $this->listingOrder = $listingOrder;
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param bool $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }


}

