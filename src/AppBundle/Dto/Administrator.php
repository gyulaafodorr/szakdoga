<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 12:16
 */

namespace AppBundle\Dto;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Administrator
 *
 * @ORM\Table(name="administrator")
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\AdministratorDtoRepository"
 * )
 */
class Administrator extends AbstractDto implements UserInterface, \Serializable
{
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    const STATUS_ARCHIVED = 'ARCHIVED';

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const ROLE_SYSTEM_ADMIN = 'ROLE_SYSTEM_ADMIN';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Dto\Contract", mappedBy="administrator")
     */
    protected $contracts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Dto\Client", mappedBy="administrator")
     */
    protected $clients;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=200, nullable=true)
     */
    protected $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="phone3", type="string", length=200, nullable=true)
     */
    protected $phone3;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=255, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="text", length=255, nullable=true)
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=30, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $role;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=30, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // ha új, kérjünk jelszót
        if (!$this->getId() && !$this->getPassword()) {
            $context->buildViolation('Jelszó magadása kötelező!')
                ->atPath('password')
                ->addViolation();
        }
    }

    /**
     * @return array
     */
    static public function getStatusMap()
    {
        return array(
            self::STATUS_ACTIVE => "Aktív",
            self::STATUS_INACTIVE => "Inaktív",
            self::STATUS_ARCHIVED => "Archív",
        );
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getRoles()
    {
        if (!$this->isActive()) {
            return array();
        }
        return array($this->getRole());
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @return string
     */
    public function getStatusFormatted()
    {
        $map = self::getStatusMap();
        return isset($map[$this->getStatus()]) ? $map[$this->getStatus()] : $this->getStatus();
    }

    /**
     * @return array
     */
    static public function getRoleMap()
    {
        return array(
            self::ROLE_ADMIN => "Referens",
            self::ROLE_SUPER_ADMIN => "Irodavezető",
            self::ROLE_SYSTEM_ADMIN => "Rendszer adminisztrátor",
        );
    }

    /**
     * @return string
     */
    public function getRoleFormatted()
    {
        $map = self::getRoleMap();
        return isset($map[$this->getRole()]) ? $map[$this->getRole()] : $this->getRole();
    }

    public function isActive()
    {
        return $this->getStatus() == self::STATUS_ACTIVE;
    }

    public function isInactive()
    {
        return $this->getStatus() == self::STATUS_INACTIVE;
    }

    public function isDeleted()
    {
        return $this->getStatus() == self::STATUS_ARCHIVED;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Administrator
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Administrator
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Administrator
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Administrator
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set phone3
     *
     * @param string $phone3
     *
     * @return Administrator
     */
    public function setPhone3($phone3)
    {
        $this->phone3 = $phone3;

        return $this;
    }

    /**
     * Get phone3
     *
     * @return string
     */
    public function getPhone3()
    {
        return $this->phone3;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Administrator
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Administrator
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Administrator
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Administrator
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Administrator
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Administrator
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->clients;
    }


    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }


    public function __toString()
    {
        return (string) $this->name;
    }
}