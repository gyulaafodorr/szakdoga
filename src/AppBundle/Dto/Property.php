<?php

namespace AppBundle\Dto;

use AppBundle\Dto\AbstractDto;
use AppBundle\Enum\PropertyTypeEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Property
 *
 * @ORM\Table(name="property", indexes={
 *     @ORM\Index(name="fk_pro_client_idx", columns={"client_id"}),
 *     @ORM\Index(name="fk_pro_administrator_idx", columns={"administrator_id"}),
 *     @ORM\Index(name="fk_pro_city_idx", columns={"city_id"}),
 *     @ORM\Index(name="fk_pro_borough_idx", columns={"borough_id"})
 * })
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\PropertyDtoRepository"
 * )
 */
class Property extends AbstractDto
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_ARCHIVED = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Dto\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Client", inversedBy="properties")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    protected $client;

    /**
     * @var \AppBundle\Dto\City
     * @Assert\NotBlank(message = "Kötelező mező")
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    protected $city;

    /**
     * @var \AppBundle\Dto\Borough
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Borough")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="borough_id", referencedColumnName="id")
     * })
     */
    protected $borough;

    /**
     * @var \AppBundle\Dto\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Administrator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     */
    protected $administrator;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Dto\Contract", mappedBy="property")
     */
    protected $contracts;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Dto\PropertyMedia", mappedBy="property")
     */
    protected $medias;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_type", type="string", length=30, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $subType;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    protected $size;

    /**
     * @var integer
     *
     * @ORM\Column(name="lot_size", type="integer", nullable=true)
     */
    protected $lotSize;

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=30, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $county;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    protected $street;

    /**
     * @var boolean
     *
     * @ORM\Column(name="street_np", type="boolean", nullable=true)
     */
    protected $streetNp = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="district", type="integer", nullable=true)
     */
    protected $district;

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=30, nullable=true)
     */
    protected $houseNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="house_number_np", type="boolean", nullable=true)
     */
    protected $houseNumberNp = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="rooms", type="integer", nullable=true)
     */
    protected $rooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="half_rooms", type="integer", nullable=true)
     */
    protected $halfRooms;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="string", length=50, nullable=true)
     */
    protected $floor;

    /**
     * @var integer
     *
     * @ORM\Column(name="levels", type="integer", nullable=true)
     */
    protected $levels;

    /**
     * @var string
     *
     * @ORM\Column(name="heating", type="string", length=50, nullable=true)
     */
    protected $heating;

    /**
     * @var string
     *
     * @ORM\Column(name="comfort", type="string", length=50, nullable=true)
     */
    protected $comfort;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    protected $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_elevator", type="integer", nullable=true)
     */
    protected $hasElevator;

    /**
     * @var string
     *
     * @ORM\Column(name="parking", type="string", length=50, nullable=true)
     */
    protected $parking;

    /**
     * @var string
     *
     * @ORM\Column(name="has_electricity", type="string", length=20, nullable=true)
     */
    protected $hasElectricity;

    /**
     * @var string
     *
     * @ORM\Column(name="has_gas", type="string", length=20, nullable=true)
     */
    protected $hasGas;

    /**
     * @var string
     *
     * @ORM\Column(name="has_drain", type="string", length=20, nullable=true)
     */
    protected $hasDrain;

    /**
     * @var string
     *
     * @ORM\Column(name="has_water", type="string", length=20, nullable=true)
     */
    protected $hasWater;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_cellar", type="integer", nullable=true)
     */
    protected $hasCellar;

    /**
     * @var string
     *
     * @ORM\Column(name="view", type="string", length=50, nullable=true)
     */
    protected $view;

    /**
     * @var string
     *
     * @ORM\Column(name="parking_fee", type="string", length=100, nullable=true)
     */
    protected $parkingFee;

    /**
     * @var string
     *
     * @ORM\Column(name="parking_fee_uom", type="string", length=25, nullable=true)
     */
    protected $parkingFeeUom;

    /**
     * @var string
     *
     * @ORM\Column(name="`condition`", type="string", length=50, nullable=true)
     */
    protected $condition;

    /**
     * @var float
     *
     * @ORM\Column(name="balcony_size", type="float", precision=10, scale=0, nullable=true)
     */
    protected $balconySize;

    /**
     * @var float
     *
     * @ORM\Column(name="garden_size", type="float", precision=10, scale=0, nullable=true)
     */
    protected $gardenSize;

    /**
     * @var string
     *
     * @ORM\Column(name="attic", type="string", length=50, nullable=true)
     */
    protected $attic;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_furniture", type="integer", nullable=true)
     */
    protected $hasFurniture;

    /**
     * @var boolean
     *
     * @ORM\Column(name="agents_can_help", type="boolean", nullable=true)
     */
    protected $agentsCanHelp;

    /**
     * @var string
     *
     * @ORM\Column(name="energy_certificate", type="string", length=50, nullable=true)
     */
    protected $energyCertificate;

    /**
     * @var string
     *
     * @ORM\Column(name="ceiling_height", type="string", nullable=true)
     */
    protected $ceilingHeight;

    /**
     * @var integer
     *
     * @ORM\Column(name="smoker", type="integer", nullable=true)
     */
    protected $smoker;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_disabled_access", type="integer", nullable=true)
     */
    protected $hasDisabledAccess;

    /**
     * @var integer
     *
     * @ORM\Column(name="mechanized", type="integer", nullable=true)
     */
    protected $mechanized;

    /**
     * @var integer
     *
     * @ORM\Column(name="pet_allowed", type="integer", nullable=true)
     */
    protected $petAllowed;

    /**
     * @var integer
     *
     * @ORM\Column(name="panel_program", type="integer", nullable=true)
     */
    protected $panelProgram;

    /**
     * @var string
     *
     * @ORM\Column(name="utility_bills", type="string", length=255, nullable=true)
     */
    protected $utilityBills;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_garden_access", type="integer", nullable=true)
     */
    protected $hasGardenAccess;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lease_rights", type="boolean", nullable=true)
     */
    protected $leaseRights;

    /**
     * @var string
     *
     * @ORM\Column(name="orientation", type="string", length=50, nullable=true)
     */
    protected $orientation;

    /**
     * @var string
     *
     * @ORM\Column(name="bath_toilet", type="string", length=50, nullable=true)
     */
    protected $bathToilet;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_ac", type="integer", nullable=true)
     */
    protected $hasAc;

    /**
     * @var string
     *
     * @ORM\Column(name="office_category", type="string", length=20, nullable=true)
     */
    protected $officeCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';


    public function __construct()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function getTypeFormatted()
    {
        return PropertyTypeEnum::getLabelByConst($this->getType());
    }

    /**
     * @return string
     */
    public function getSubTypeFormatted()
    {
        return PropertyTypeEnum::getSubTypeLabelByConst($this->getType(), $this->getSubType());
    }

    /**
     * @return array
     */
    static public function getStatusMap()
    {
        return array(
            self::STATUS_ACTIVE => "Aktív",
            self::STATUS_INACTIVE => "Inaktív",
            self::STATUS_ARCHIVED => "Archív",
        );
    }


    public function isActive()
    {
        return $this->getStatus() == self::STATUS_ACTIVE;
    }

    public function isArchive()
    {
        return $this->getStatus() == self::STATUS_ARCHIVED;
    }


    /**
     * @return string
     */
    public function getStatusFormatted()
    {
        $map = self::getStatusMap();
        return isset($map[$this->getStatus()]) ? $map[$this->getStatus()] : $this->getStatus();
    }


    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSubType()
    {
        return $this->subType;
    }

    /**
     * @param string $subType
     */
    public function setSubType($subType)
    {
        $this->subType = $subType;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getLotSize()
    {
        return $this->lotSize;
    }

    /**
     * @param int $lotSize
     */
    public function setLotSize($lotSize)
    {
        $this->lotSize = $lotSize;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return bool
     */
    public function isStreetNp()
    {
        return $this->streetNp;
    }

    /**
     * @param bool $streetNp
     */
    public function setStreetNp($streetNp)
    {
        $this->streetNp = $streetNp;
    }

    /**
     * @return bool
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param bool $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return bool
     */
    public function isHouseNumberNp()
    {
        return $this->houseNumberNp;
    }

    /**
     * @param bool $houseNumberNp
     */
    public function setHouseNumberNp($houseNumberNp)
    {
        $this->houseNumberNp = $houseNumberNp;
    }

    /**
     * @return bool
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param bool $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return bool
     */
    public function getHalfRooms()
    {
        return $this->halfRooms;
    }

    /**
     * @param bool $halfRooms
     */
    public function setHalfRooms($halfRooms)
    {
        $this->halfRooms = $halfRooms;
    }

    /**
     * @return string
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * @param string $floor
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;
    }

    /**
     * @return bool
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * @param bool $levels
     */
    public function setLevels($levels)
    {
        $this->levels = $levels;
    }

    /**
     * @return string
     */
    public function getHeating()
    {
        return $this->heating;
    }

    /**
     * @param string $heating
     */
    public function setHeating($heating)
    {
        $this->heating = $heating;
    }

    /**
     * @return string
     */
    public function getComfort()
    {
        return $this->comfort;
    }

    /**
     * @param string $comfort
     */
    public function setComfort($comfort)
    {
        $this->comfort = $comfort;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isHasElevator()
    {
        return $this->hasElevator;
    }

    /**
     * @param bool $hasElevator
     */
    public function setHasElevator($hasElevator)
    {
        $this->hasElevator = $hasElevator;
    }

    /**
     * @return string
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * @param string $parking
     */
    public function setParking($parking)
    {
        $this->parking = $parking;
    }

    /**
     * @return string
     */
    public function getHasElectricity()
    {
        return $this->hasElectricity;
    }

    /**
     * @param string $hasElectricity
     */
    public function setHasElectricity($hasElectricity)
    {
        $this->hasElectricity = $hasElectricity;
    }

    /**
     * @return string
     */
    public function getHasGas()
    {
        return $this->hasGas;
    }

    /**
     * @param string $hasGas
     */
    public function setHasGas($hasGas)
    {
        $this->hasGas = $hasGas;
    }

    /**
     * @return string
     */
    public function getHasDrain()
    {
        return $this->hasDrain;
    }

    /**
     * @param string $hasDrain
     */
    public function setHasDrain($hasDrain)
    {
        $this->hasDrain = $hasDrain;
    }

    /**
     * @return string
     */
    public function getHasWater()
    {
        return $this->hasWater;
    }

    /**
     * @param string $hasWater
     */
    public function setHasWater($hasWater)
    {
        $this->hasWater = $hasWater;
    }

    /**
     * @return bool
     */
    public function isHasCellar()
    {
        return $this->hasCellar;
    }

    /**
     * @param bool $hasCellar
     */
    public function setHasCellar($hasCellar)
    {
        $this->hasCellar = $hasCellar;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return string
     */
    public function getParkingFee()
    {
        return $this->parkingFee;
    }

    /**
     * @param string $parkingFee
     */
    public function setParkingFee($parkingFee)
    {
        $this->parkingFee = $parkingFee;
    }

    /**
     * @return string
     */
    public function getParkingFeeUom()
    {
        return $this->parkingFeeUom;
    }

    /**
     * @param string $parkingFeeUom
     */
    public function setParkingFeeUom($parkingFeeUom)
    {
        $this->parkingFeeUom = $parkingFeeUom;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return float
     */
    public function getBalconySize()
    {
        return $this->balconySize;
    }

    /**
     * @param float $balconySize
     */
    public function setBalconySize($balconySize)
    {
        $this->balconySize = $balconySize;
    }

    /**
     * @return float
     */
    public function getGardenSize()
    {
        return $this->gardenSize;
    }

    /**
     * @param float $gardenSize
     */
    public function setGardenSize($gardenSize)
    {
        $this->gardenSize = $gardenSize;
    }

    /**
     * @return string
     */
    public function getAttic()
    {
        return $this->attic;
    }

    /**
     * @param string $attic
     */
    public function setAttic($attic)
    {
        $this->attic = $attic;
    }

    /**
     * @return bool
     */
    public function isHasFurniture()
    {
        return $this->hasFurniture;
    }

    /**
     * @param bool $hasFurniture
     */
    public function setHasFurniture($hasFurniture)
    {
        $this->hasFurniture = $hasFurniture;
    }

    /**
     * @return bool
     */
    public function isAgentsCanHelp()
    {
        return $this->agentsCanHelp;
    }

    /**
     * @param bool $agentsCanHelp
     */
    public function setAgentsCanHelp($agentsCanHelp)
    {
        $this->agentsCanHelp = $agentsCanHelp;
    }

    /**
     * @return string
     */
    public function getEnergyCertificate()
    {
        return $this->energyCertificate;
    }

    /**
     * @param string $energyCertificate
     */
    public function setEnergyCertificate($energyCertificate)
    {
        $this->energyCertificate = $energyCertificate;
    }

    /**
     * @return bool
     */
    public function isCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * @param bool $ceilingHeight
     */
    public function setCeilingHeight($ceilingHeight)
    {
        $this->ceilingHeight = $ceilingHeight;
    }

    /**
     * @return bool
     */
    public function isSmoker()
    {
        return $this->smoker;
    }

    /**
     * @param bool $smoker
     */
    public function setSmoker($smoker)
    {
        $this->smoker = $smoker;
    }

    /**
     * @return bool
     */
    public function isHasDisabledAccess()
    {
        return $this->hasDisabledAccess;
    }

    /**
     * @param bool $hasDisabledAccess
     */
    public function setHasDisabledAccess($hasDisabledAccess)
    {
        $this->hasDisabledAccess = $hasDisabledAccess;
    }

    /**
     * @return bool
     */
    public function isMechanized()
    {
        return $this->mechanized;
    }

    /**
     * @param bool $mechanized
     */
    public function setMechanized($mechanized)
    {
        $this->mechanized = $mechanized;
    }

    /**
     * @return bool
     */
    public function isPetAllowed()
    {
        return $this->petAllowed;
    }

    /**
     * @param bool $petAllowed
     */
    public function setPetAllowed($petAllowed)
    {
        $this->petAllowed = $petAllowed;
    }

    /**
     * @return bool
     */
    public function isPanelProgram()
    {
        return $this->panelProgram;
    }

    /**
     * @param bool $panelProgram
     */
    public function setPanelProgram($panelProgram)
    {
        $this->panelProgram = $panelProgram;
    }

    /**
     * @return string
     */
    public function getUtilityBills()
    {
        return $this->utilityBills;
    }

    /**
     * @param string $utilityBills
     */
    public function setUtilityBills($utilityBills)
    {
        $this->utilityBills = $utilityBills;
    }

    /**
     * @return bool
     */
    public function isHasGardenAccess()
    {
        return $this->hasGardenAccess;
    }

    /**
     * @param bool $hasGardenAccess
     */
    public function setHasGardenAccess($hasGardenAccess)
    {
        $this->hasGardenAccess = $hasGardenAccess;
    }

    /**
     * @return bool
     */
    public function isLeaseRights()
    {
        return $this->leaseRights;
    }

    /**
     * @param bool $leaseRights
     */
    public function setLeaseRights($leaseRights)
    {
        $this->leaseRights = $leaseRights;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
    }

    /**
     * @return string
     */
    public function getBathToilet()
    {
        return $this->bathToilet;
    }

    /**
     * @param string $bathToilet
     */
    public function setBathToilet($bathToilet)
    {
        $this->bathToilet = $bathToilet;
    }

    /**
     * @return bool
     */
    public function isHasAc()
    {
        return $this->hasAc;
    }

    /**
     * @param bool $hasAc
     */
    public function setHasAc($hasAc)
    {
        $this->hasAc = $hasAc;
    }

    /**
     * @return string
     */
    public function getOfficeCategory()
    {
        return $this->officeCategory;
    }

    /**
     * @param string $officeCategory
     */
    public function setOfficeCategory($officeCategory)
    {
        $this->officeCategory = $officeCategory;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Contract
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param Contract $contract
     */
    public function setContract($contract)
    {
        $this->contract = $contract;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param City $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return Borough
     */
    public function getBorough()
    {
        return $this->borough;
    }

    /**
     * @param Borough $borough
     */
    public function setBorough($borough)
    {
        $this->borough = $borough;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @param Administrator $administrator
     */
    public function setAdministrator($administrator)
    {
        $this->administrator = $administrator;
    }

    /**
     * @return mixed
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * @param mixed $contracts
     */
    public function setContracts($contracts)
    {
        $this->contracts = $contracts;
    }

    /**
     * @return mixed
     */
    public function getMedias()
    {
        return $this->medias;
    }

    /**
     * @param mixed $medias
     */
    public function setMedias($medias)
    {
        $this->medias = $medias;
    }


    public function __toString()
    {
        return '[#' . $this->getId() . '] ' . $this->getTypeFormatted() . '/' . $this->getSubTypeFormatted() . ' - ' . $this->getCity()->getName() .
            ($this->getStreet() ? ', ' . $this->getStreet() : '');
    }


}

