<?php

namespace AppBundle\Dto;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changelog
 *
 * @ORM\Table(name="changelog", indexes={
 *     @ORM\Index(name="pc_field_idx", columns={"field"}),
 *     @ORM\Index(name="pc_admin_idx", columns={"administrator_id"})
 * })
 * @ORM\Entity
 */
class Changelog extends AbstractDto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Dto\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Administrator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $administrator;


    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $field;

    /**
     * @var string
     *
     * @ORM\Column(name="old_value", type="text", length=65535, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_id", type="integer", nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $entityId;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_name", type="text", length=255, nullable=false)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $entityName;

    /**
     * @var string
     *
     * @ORM\Column(name="new_value", type="text", length=65535, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $newValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="changed_at", type="datetime", nullable=false)
     */
    protected $changedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @param Administrator $administrator
     */
    public function setAdministrator($administrator)
    {
        $this->administrator = $administrator;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @param string $oldValue
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * @return string
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @param string $newValue
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;
    }

    /**
     * @return \DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * @param \DateTime $changedAt
     */
    public function setChangedAt($changedAt)
    {
        $this->changedAt = $changedAt;
    }


}

