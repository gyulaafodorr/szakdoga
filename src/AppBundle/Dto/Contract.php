<?php

namespace AppBundle\Dto;

use AppBundle\Dto\AbstractDto;
use AppBundle\Enum\AgreementTypeEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contract
 *
 * @ORM\Table(name="contract", indexes={@ORM\Index(name="fk_pro_client_idx", columns={"client_id"}), @ORM\Index(name="fk_pro_administrator_idx", columns={"administrator_id"})})
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\ContractDtoRepository"
 * )
 */
class Contract extends AbstractDto
{
    const STATUS_ACTIVE = 1;
    const STATUS_ENDED = 2;
    const STATUS_ARCHIVED = 3;

    const RESULT_SUCCESS = 1;
    const RESULT_FAILURE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \AppBundle\Dto\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Client", inversedBy="contracts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    protected $client;

    /**
     * @var \AppBundle\Dto\Property
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Property", inversedBy="contracts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     * })
     */
    protected $property;

    /**
     * @var \AppBundle\Dto\Administrator
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Dto\Administrator", inversedBy="contracts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administrator_id", referencedColumnName="id")
     * })
     */
    protected $administrator;

    /**
     * @var string
     *
     * @ORM\Column(name="contract_number", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $contractNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="commission", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $commission;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    protected $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="result", type="integer", nullable=true)
     */
    protected $result;

    /**
     * @var integer
     *
     * @ORM\Column(name="agreement_type", type="integer", nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $agreementType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_exclusive", type="boolean", nullable=true)
     */
    protected $isExclusive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", nullable=true)
     */
    protected $deadline;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="bigint", nullable=true)
     * @Assert\NotBlank(message = "Kötelező mező")
     */
    protected $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_eur", type="bigint", nullable=true)
     */
    protected $priceEur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @return array
     */
    static public function getStatusMap()
    {
        return array(
            self::STATUS_ACTIVE => "Aktív",
            self::STATUS_ENDED => "Befejezett",
            self::STATUS_ARCHIVED => "Archív",
        );
    }

    /**
     * @return array
     */
    static public function getResultMap()
    {
        return array(
            self::RESULT_SUCCESS => "Sikeres",
            self::RESULT_FAILURE => "Sikertelen",
        );
    }

    /**
     * @return string
     */
    public function getStatusFormatted()
    {
        $map = self::getStatusMap();
        return isset($map[$this->getStatus()]) ? $map[$this->getStatus()] : $this->getStatus();
    }

    /**
     * @return string
     */
    public function getResultFormatted()
    {
        $map = self::getResultMap();
        return isset($map[$this->getResult()]) ? $map[$this->getResult()] : $this->getResult();
    }

    public function getAgreementTypeFormatted()
    {
        return AgreementTypeEnum::getLabelByConst($this->getAgreementType());
    }

    public function isExclusiveFormatted()
    {
        return $this->isIsExclusive() ? 'kizárólagos' : 'nem kizárólagos';
    }

    public function isActive()
    {
        return $this->getStatus() == self::STATUS_ACTIVE;
    }

    public function isEnded()
    {
        return $this->getStatus() == self::STATUS_ENDED;
    }

    public function isArchive()
    {
        return $this->getStatus() == self::STATUS_ARCHIVED;
    }

    public function __toString()
    {
        return '[' . $this->getId() . '] - ' . $this->getContractNumber();
    }

    /**
     * @return string
     */
    public function getContractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * @param string $contractNumber
     */
    public function setContractNumber($contractNumber)
    {
        $this->contractNumber = $contractNumber;
    }

    /**
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param string $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function getAgreementType()
    {
        return $this->agreementType;
    }

    /**
     * @param bool $agreementType
     */
    public function setAgreementType($agreementType)
    {
        $this->agreementType = $agreementType;
    }

    /**
     * @return bool
     */
    public function isIsExclusive()
    {
        return $this->isExclusive;
    }

    /**
     * @param bool $isExclusive
     */
    public function setIsExclusive($isExclusive)
    {
        $this->isExclusive = $isExclusive;
    }

    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime $deadline
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPriceEur()
    {
        return $this->priceEur;
    }

    /**
     * @param int $priceEur
     */
    public function setPriceEur($priceEur)
    {
        $this->priceEur = $priceEur;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return Administrator
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * @param Administrator $administrator
     */
    public function setAdministrator($administrator)
    {
        $this->administrator = $administrator;
    }

    /**
     * @return bool
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param bool $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param Property $property
     */
    public function setProperty($property)
    {
        $this->property = $property;
    }


}

