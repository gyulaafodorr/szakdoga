<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

class AgreementTypeEnum extends AbstractEnum
{
    const SALE = '1';
    const RENT = '2';


    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::SALE => 'eladó',
            self::RENT => 'kiadó',
        );
    }


    public static function getLabelConstMapWithEmpty()
    {
        return array('' => 'Összes') + static::getLabelConstMapConainer();
    }

}

