<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class EmailTypeEnum extends AbstractEnum
{
    const PERSONAL = 'PERSONAL';
    const WORK = 'WORK';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::PERSONAL => 'magán',
            self::WORK => 'munkahelyi',
        );
    }

}

