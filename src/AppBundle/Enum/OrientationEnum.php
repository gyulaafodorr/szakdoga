<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class OrientationEnum extends AbstractEnum
{
    const ESZAK = "ESZAK";
    const ESZAKKELET = "ESZAKKELET";
    const KELET = "KELET";
    const DELKELET = "DELKELET";
    const DEL = "DEL";
    const DELNYUGAT = "DELNYUGAT";
    const NYUGAT = "NYUGAT";
    const ESZAKNYUGAT = "ESZAKNYUGAT";

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::ESZAK => "észak",
            self::ESZAKKELET => "északkelet",
            self::KELET => "kelet",
            self::DELKELET => "délkelet",
            self::DEL => "dél",
            self::DELNYUGAT => "délnyugat",
            self::NYUGAT => "nyugat",
            self::ESZAKNYUGAT => "északnyugat",
        );
    }

}

