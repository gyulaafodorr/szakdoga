<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class CountyEnum extends AbstractEnum
{
    const Budapest = "BUDAPEST";
    const Baranya = "BARANYA";
    const BacsKiskun = "BACSKISKUN";
    const Bekes = "BEKES";
    const BorsodAbaujZemplen = "BORSOD";
    const Csongrad = "CSONGRAD";
    const Fejer = "FEJER";
    const GyorMosonSopron = "GYOR";
    const HajduBihar = "HAJDUBIHAR";
    const Heves = "HEVES";
    const KomaromEsztergom = "KOMAROM";
    const Nograd = "NOGRAD";
    const Pest = "PEST";
    const Somogy = "SOMOGY";
    const SzabolcsSzatmarBereg = "SZABOLCS";
    const JaszNagykunSzolnok = "JASZNAGYKUN";
    const Tolna = "TOLNA";
    const Vas = "VAS";
    const Veszprem = "VESZPREM";
    const Zala = "ZALA";


    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::Budapest => "Budapest",
            self::Baranya => "Baranya",
            self::BacsKiskun => "Bács-Kiskun",
            self::Bekes => "Békés",
            self::BorsodAbaujZemplen => "Borsod-Abaúj-Zemplén",
            self::Csongrad => "Csongrád",
            self::Fejer => "Fejér",
            self::GyorMosonSopron => "Győr-Moson-Sopron",
            self::HajduBihar => "Hajdú-Bihar",
            self::Heves => "Heves",
            self::KomaromEsztergom => "Komárom-Esztergom",
            self::Nograd => "Nógrád",
            self::Pest => "Pest",
            self::Somogy => "Somogy",
            self::SzabolcsSzatmarBereg => "Szabolcs-Szatmár-Bereg",
            self::JaszNagykunSzolnok => "Jász-Nagykun-Szolnok",
            self::Tolna => "Tolna",
            self::Vas => "Vas",
            self::Veszprem => "Veszprém",
            self::Zala => "Zala",
        );
    }

}

//update city set county="BACSKISKUN" where county="Bács-Kiskun";
//update city set county="BARANYA" where county="Baranya";
//update city set county="BEKES" where county="Békés";
//update city set county="BORSOD" where county="Borsod-Abaúj-Zemplén";
//update city set county="CSONGRAD" where county="Csongrád";
//update city set county="FEJER" where county="Fejér";
//update city set county="GYOR" where county="Győr-Moson-Sopron";
//update city set county="HAJDUBIHAR" where county="Hajdú-Bihar";
//update city set county="HEVES" where county="Heves";
//update city set county="JASZNAGYKUN" where county="Jász-Nagykun-Szolnok";
//update city set county="KOMAROM" where county="Komárom-Esztergom";
//update city set county="NOGRAD" where county="Nógrád";
//update city set county="PEST" where county="Pest";
//update city set county="SOMOGY" where county="Somogy";
//update city set county="SZABOLCS" where county="Szabolcs-Szatmár-Bereg";
//update city set county="TOLNA" where county="Tolna";
//update city set county="VAS" where county="Vas";
//update city set county="VESZPREM" where county="Veszprém";
//update city set county="ZALA" where county="Zala";
//update city set county="BUDAPEST" where county="főváros";