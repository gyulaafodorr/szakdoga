<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class HasFurnituresEnum extends AbstractEnum
{
    const NA = 0;
    const YES = 1;
    const NO = 2;
    const BOTH = 3;

    /**
     * {@inheritdoc }
     */
    public static function getLabelConstMap()
    {
        return array(
            self::NA => 'Nincs megadva',
            self::YES => 'Igen',
            self::NO => 'Nem',
            self::BOTH => 'Is-is',
        );
    }

}

