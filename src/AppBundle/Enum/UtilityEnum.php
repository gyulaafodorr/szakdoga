<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class UtilityEnum extends AbstractEnum
{
    const TELKEN_BELUL = 'TELKEN_BELUL';
    const UTCABAN = 'UTCABAN';
    const NINCS = 'NINCS';
    const VAN = 'VAN';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::TELKEN_BELUL => 'telken belül',
            self::UTCABAN => 'utcában',
            self::NINCS => 'nincs',
            self::VAN => 'van',
        );
    }

}

