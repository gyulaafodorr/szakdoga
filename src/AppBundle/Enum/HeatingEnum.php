<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 06. 30.
 * Time: 15:38
 */

namespace AppBundle\Enum;


use AppBundle\Enum\AbstractEnum;

class HeatingEnum extends AbstractEnum
{
    const NINCS_ADAT = 'NINCS_ADAT';
    const GAZ_CIRKO = 'GAZ_CIRKO';
    const GAZ_KONVEKTOR = 'GAZ_KONVEKTOR';
    const GAZ_HERA = 'GAZ_HERA';
    const GAZKAZAN = 'GAZKAZAN';
    const TAVFUTES = 'TAVFUTES';
    const TAVFUTES_EGYEDI = 'TAVFUTES_EGYEDI';
    const ELEKTROMOS = 'ELEKTROMOS';
    const HAZKOZPONTI = 'HAZKOZPONTI';
    const HAZKOZPONTI_EGYEDI = 'HAZKOZPONTI_EGYEDI';
    const FAN_COIL = 'FAN_COIL';
    const GEOTERMIKUS = 'GEOTERMIKUS';
    const CSEREPKALYHA = 'CSEREPKALYHA';
    const EGYEB_KAZAN = 'EGYEB_KAZAN';
    const GAZ_NAPKOLLEKTOR = 'GAZ_NAPKOLLEKTOR';
    const EGYEB = 'EGYEB';


    /**
     * {@inheritdoc }
     */
    public static function getLabelConstMapConainer()
    {
        return array(
            self::NINCS_ADAT => 'nincs adat',
            self::GAZ_CIRKO => 'gáz (cirko)',
            self::GAZ_KONVEKTOR => 'gáz (konvektor)',
            self::GAZ_HERA => 'gáz (héra)',
            self::GAZKAZAN => 'gázkazán',
            self::TAVFUTES => 'távfűtés',
            self::TAVFUTES_EGYEDI => 'távfűtés egyedi méréssel',
            self::ELEKTROMOS => 'elektromos',
            self::HAZKOZPONTI => 'házközponti',
            self::HAZKOZPONTI_EGYEDI => 'házközponti egyedi méréssel',
            self::FAN_COIL => 'fan-coil',
            self::GEOTERMIKUS => 'geotermikus',
            self::CSEREPKALYHA => 'cserépkályha',
            self::EGYEB_KAZAN => 'egyéb kazán',
            self::GAZ_NAPKOLLEKTOR => 'gáz + napkollektor',
            self::EGYEB => 'egyéb',
        );
    }

} 