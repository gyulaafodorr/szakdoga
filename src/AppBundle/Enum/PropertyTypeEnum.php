<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 06. 30.
 * Time: 15:38
 */

namespace AppBundle\Enum;


use AppBundle\Enum\AbstractEnum;

class PropertyTypeEnum extends AbstractEnum
{
    const TYPE_LAKAS = "LAKAS";
    const TYPE_HAZ = "HAZ";
    const TYPE_TELEK = "TELEK";
    const TYPE_GARAZS = "GARAZS";
    const TYPE_NYARALO = "NYARALO";
    const TYPE_IRODA = "IRODA";
    const TYPE_UZLETHELYISEG = "UZLETHELYISEG";
    const TYPE_VENDEGLATO = "VENDEGLATO";
    const TYPE_IPARI = "IPARI";
    const TYPE_MEZOGAZDASAGI = "MEZOGAZDASAGI";
    const TYPE_FEJLESZTESI = "FEJLESZTESI";
    const TYPE_INTEZMENY = "INTEZMENY";
    const TYPE_RAKTAR = "RAKTAR";


    const SUBTYPE_LAKAS_TEGLA = "TEGLA";
    const SUBTYPE_LAKAS_PANEL = "PANEL";
    const SUBTYPE_LAKAS_UJ = "UJ";
    const SUBTYPE_LAKAS_SZOBA = "SZOBA";
    const SUBTYPE_LAKAS_CSUSZTATOTT_ZSALUS = "CSUSZTATOTT_ZSALUS";

    const SUBTYPE_HAZ_CSALADI = "CSALADI";
    const SUBTYPE_HAZ_IKERHAZ = "IKERHAZ";
    const SUBTYPE_HAZ_SORHAZ = "SORHAZ";
    const SUBTYPE_HAZ_HAZRESZ = "HAZRESZ";
    const SUBTYPE_HAZ_KASTELY = "KASTELY";
    const SUBTYPE_HAZ_TANYA = "TANYA";
    const SUBTYPE_HAZ_KONNYUSZERKEZETES = "KONNYUSZERKEZETES";
    const SUBTYPE_HAZ_VALYOG = "VALYOG";

    const SUBTYPE_TELEK_LAKOOVEZETI = "LAKOOVEZETI";
    const SUBTYPE_TELEK_UDULOOVEZETI = "UDULOOVEZETI";
    const SUBTYPE_TELEK_KULTERULETI = "KULTERULETI";
    const SUBTYPE_TELEK_EGYEB = "EGYEB";

    const SUBTYPE_GARAZS_EGYEDI = "EGYEDI";
    const SUBTYPE_GARAZS_TEREM = "TEREM";

    const SUBTYPE_NYARALO_TELEK = "TELEK";
    const SUBTYPE_NYARALO_HETVEGI_HAZAS = "HETVEGI_HAZAS";
    const SUBTYPE_NYARALO_UDULOHAZAS = "UDULOHAZAS";

    const SUBTYPE_IRODA_IRODAHAZ = "IRODAHAZ";
    const SUBTYPE_IRODA_IRODAHAZBAN = "IRODAHAZBAN";
    const SUBTYPE_IRODA_CSALADI_HAZBAN = "CSALADI_HAZBAN";
    const SUBTYPE_IRODA_LAKASBAN = "LAKASBAN";
    const SUBTYPE_IRODA_EGYEB = "EGYEB";

    const SUBTYPE_UZLETHELYISEG_UZLETHAZBAN = "UZLETHAZBAN";
    const SUBTYPE_UZLETHELYISEG_UTCAI_BEJARATOS = "UTCAI_BEJARATOS";
    const SUBTYPE_UZLETHELYISEG_UDVARBAN = "UDVARBAN";
    const SUBTYPE_UZLETHELYISEG_EGYEB = "EGYEB";

    const SUBTYPE_VENDEGLATO_EGYEB = "EGYEB";
    const SUBTYPE_VENDEGLATO_SZALLODA = "SZALLODA";
    const SUBTYPE_VENDEGLATO_ETTEREM = "ETTEREM";

    const SUBTYPE_IPARI_RAKTAR = "RAKTAR";
    const SUBTYPE_IPARI_MUHELY = "MUHELY";
    const SUBTYPE_IPARI_TELEPHELY = "TELEPHELY";
    const SUBTYPE_IPARI_EGYEB = "EGYEB";
    const SUBTYPE_IPARI_TELEK = "TELEK";
    const SUBTYPE_IPARI_PARK = "PARK";


    const SUBTYPE_MEZOGAZDASAGI_TANYA = "TANYA";    //??
    const SUBTYPE_MEZOGAZDASAGI_ALTALANOS = "ALTALANOS";
    const SUBTYPE_MEZOGAZDASAGI_TERMOFOLD = "TERMOFOLD";
    const SUBTYPE_MEZOGAZDASAGI_ERDO = "ERDO";
    const SUBTYPE_MEZOGAZDASAGI_PINCE = "PINCE";


    const SUBTYPE_FEJLESZTESI_LAKO = "LAKO";
    const SUBTYPE_FEJLESZTESI_KERESKEDELMI = "KERESKEDELMI";
    const SUBTYPE_FEJLESZTESI_VEGYES = "VEGYES";
    const SUBTYPE_FEJLESZTESI_IPARI = "IPARI";
    const SUBTYPE_FEJLESZTESI_UDULO = "UDULO";
    const SUBTYPE_FEJLESZTESI_KULONLEGES = "KULONLEGES";

    const SUBTYPE_INTEZMENY_EGESZSEGUGYI = "EGESZSEGUGYI";
    const SUBTYPE_INTEZMENY_ISKOLA = "ISKOLA";
    const SUBTYPE_INTEZMENY_MUZEUM = "MUZEUM";
    const SUBTYPE_INTEZMENY_OVODA = "OVODA";

    const SUBTYPE_RAKTAR_RAKTAR = "RAKTAR";
    const SUBTYPE_RAKTAR_RAKTARBAZIS = "RAKTARBAZIS";
    const SUBTYPE_RAKTAR_LOGISZTIKAI_PARL = "LOGISZTIKAI_PARK";

    /**
     * {@inheritdoc }
     */
    public static function getLabelConstMapConainer()
    {
        return array(
            self::TYPE_LAKAS => "lakás",
            self::TYPE_HAZ => "ház",
            self::TYPE_TELEK => "telek",
            self::TYPE_GARAZS => "garázs",
            self::TYPE_NYARALO => "nyaraló",
            self::TYPE_IRODA => "iroda",
            self::TYPE_UZLETHELYISEG => "üzlethelyiség",
            self::TYPE_VENDEGLATO => "vendéglátó egység",
            self::TYPE_RAKTAR => "raktár",
            self::TYPE_IPARI => "ipari",
            self::TYPE_MEZOGAZDASAGI => "mezőgazdasági",
            self::TYPE_FEJLESZTESI => "fejlesztési terület",
        );
    }

    public static function getSubTypeLabelByConst($type, $constantName)
    {
        $translatedLabelsConstMap = static::getLabelConstSubtypeMap($type);
        return $translatedLabelsConstMap[$constantName];
    }

    /**
     * {@inheritdoc }
     */
    public static function getLabelConstSubtypeMap($type)
    {
        $map = self::getLabelConstSubtypeMapContainer();
        if (isset($map[$type])) {
            return $map[$type];
        }
        return array();
    }

    /**
     * {@inheritdoc }
     */
    public static function getLabelConstSubtypeMapContainer()
    {
        return array(
            self::TYPE_LAKAS => array(
                self::SUBTYPE_LAKAS_TEGLA => "tégla",
                self::SUBTYPE_LAKAS_PANEL => "panel",
//                self::SUBTYPE_LAKAS_UJ => "új építésű",
                self::SUBTYPE_LAKAS_SZOBA => "szoba",
                self::SUBTYPE_LAKAS_CSUSZTATOTT_ZSALUS => "csúsztatott zsalus",
            ),
            self::TYPE_HAZ => array(
                self::SUBTYPE_HAZ_CSALADI => "családi ház",
                self::SUBTYPE_HAZ_IKERHAZ => "ikerház",
                self::SUBTYPE_HAZ_SORHAZ => "sorház",
                self::SUBTYPE_HAZ_HAZRESZ => "házrész",
                self::SUBTYPE_HAZ_KASTELY => "kastély",
                self::SUBTYPE_HAZ_TANYA => "tanya",
                self::SUBTYPE_HAZ_KONNYUSZERKEZETES => "könnyűszerkezetes",
                self::SUBTYPE_HAZ_VALYOG => "vályog",
            ),
            self::TYPE_TELEK => array(
                self::SUBTYPE_TELEK_LAKOOVEZETI => "lakóövezeti",
                self::SUBTYPE_TELEK_UDULOOVEZETI => "üdülőövezeti",
                self::SUBTYPE_TELEK_KULTERULETI => "külterületi",
                self::SUBTYPE_TELEK_EGYEB => "egyéb",
            ),
            self::TYPE_GARAZS => array(
                self::SUBTYPE_GARAZS_EGYEDI => "egyedi garázs",
                self::SUBTYPE_GARAZS_TEREM => "teremgarázs",
            ),
            self::TYPE_NYARALO => array(
                self::SUBTYPE_NYARALO_TELEK => "telek",
                self::SUBTYPE_NYARALO_HETVEGI_HAZAS => "hétvégi házas",
                self::SUBTYPE_NYARALO_UDULOHAZAS => "üdülőházas",
            ),
            self::TYPE_IRODA => array(
                self::SUBTYPE_IRODA_IRODAHAZ => "irodaház",
                self::SUBTYPE_IRODA_IRODAHAZBAN => "irodaházban",
                self::SUBTYPE_IRODA_CSALADI_HAZBAN => "családi házban",
                self::SUBTYPE_IRODA_LAKASBAN => "lakásban",
                self::SUBTYPE_IRODA_EGYEB => "egyéb",
            ),
            self::TYPE_UZLETHELYISEG => array(
                self::SUBTYPE_UZLETHELYISEG_UZLETHAZBAN => "üzletházban",
                self::SUBTYPE_UZLETHELYISEG_UTCAI_BEJARATOS => "utcai bejáratos",
                self::SUBTYPE_UZLETHELYISEG_UDVARBAN => "udvarban",
                self::SUBTYPE_UZLETHELYISEG_EGYEB => "egyéb",
            ),
            self::TYPE_VENDEGLATO => array(
                self::SUBTYPE_VENDEGLATO_SZALLODA => "szálloda, hotel, panzió",
                self::SUBTYPE_VENDEGLATO_ETTEREM => "étterem, vendéglő",
                self::SUBTYPE_VENDEGLATO_EGYEB => "egyéb",
            ),
            self::TYPE_IPARI => array(
                self::SUBTYPE_IPARI_MUHELY => "műhely",
                self::SUBTYPE_IPARI_TELEPHELY => "telephely",
//                self::SUBTYPE_IPARI_TELEK => "telek",
                self::SUBTYPE_IPARI_EGYEB => "egyéb",
                self::SUBTYPE_IPARI_PARK => "ipari park",
            ),
            self::TYPE_MEZOGAZDASAGI => array(
                self::SUBTYPE_MEZOGAZDASAGI_TANYA => "tanya",
                self::SUBTYPE_MEZOGAZDASAGI_ALTALANOS => "általános mezőgazdasági terület",
                self::SUBTYPE_MEZOGAZDASAGI_TERMOFOLD => "termőföl, szántó",
                self::SUBTYPE_MEZOGAZDASAGI_ERDO => "erdő",
                self::SUBTYPE_MEZOGAZDASAGI_PINCE => "pince, présház",
            ),
            self::TYPE_FEJLESZTESI => array(
                self::SUBTYPE_FEJLESZTESI_LAKO => "lakóterület",
                self::SUBTYPE_FEJLESZTESI_KERESKEDELMI => "kereskedelmi, szolgáltató terület",
                self::SUBTYPE_FEJLESZTESI_VEGYES => "vegyes (lakó- és kereskedelmi) terület",
                self::SUBTYPE_FEJLESZTESI_IPARI => "ipari terület",
                self::SUBTYPE_FEJLESZTESI_UDULO => "üdülőterület",
                self::SUBTYPE_FEJLESZTESI_KULONLEGES => "különleges terület",
            ),
            self::TYPE_INTEZMENY => array(
                self::SUBTYPE_INTEZMENY_EGESZSEGUGYI => "egészségügyi intézmény",
                self::SUBTYPE_INTEZMENY_ISKOLA => "iskola",
                self::SUBTYPE_INTEZMENY_MUZEUM => "múzeum",
                self::SUBTYPE_INTEZMENY_OVODA => "óvoda",
            ),
            self::TYPE_RAKTAR => array(
                self::SUBTYPE_RAKTAR_RAKTAR => "raktárhelyiség",
                self::SUBTYPE_RAKTAR_RAKTARBAZIS => "raktárbázis",
                self::SUBTYPE_RAKTAR_LOGISZTIKAI_PARL => "logisztikai park",
            ),
        );
    }


} 