<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class CeilingHeightEnum extends AbstractEnum
{
    const KISEBB_3 = 1;
    const NAGYOBB_3 = 2;

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::KISEBB_3 => '3 m-nél alacsonyabb',
            self::NAGYOBB_3 => '3 m-nél magasabb',
        );
    }

}

