<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class FloorEnum extends AbstractEnum
{
    const SZUTEREN = "-2";
    const FOLDSZINT = "-1";
    const FELEMELET = "0";
    const EMELET_1 = "1";
    const EMELET_2 = "2";
    const EMELET_3 = "3";
    const EMELET_4 = "4";
    const EMELET_5 = "5";
    const EMELET_6 = "6";
    const EMELET_7 = "7";
    const EMELET_8 = "8";
    const EMELET_9 = "9";
    const EMELET_10 = "10";
    const TOBB_MINT_10 = "99";

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::SZUTEREN => "szuterén",
            self::FOLDSZINT => "földszint",
            self::FELEMELET => "félemelet",
            self::EMELET_1 => "1. emelet",
            self::EMELET_2 => "2. emelet",
            self::EMELET_3 => "3. emelet",
            self::EMELET_4 => "4. emelet",
            self::EMELET_5 => "5. emelet",
            self::EMELET_6 => "6. emelet",
            self::EMELET_7 => "7. emelet",
            self::EMELET_8 => "8. emelet",
            self::EMELET_9 => "9. emelet",
            self::EMELET_10 => "10. emelet",
            self::TOBB_MINT_10 => "több mint 10",
        );
    }

}

