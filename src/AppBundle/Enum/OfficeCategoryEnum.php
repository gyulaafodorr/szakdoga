<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class OfficeCategoryEnum extends AbstractEnum
{
    const CATEGORY_A = "A";
    const CATEGORY_B = "B";
    const CATEGORY_C = "C";

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::CATEGORY_A => "A/A+",
            self::CATEGORY_B => "B/B+",
            self::CATEGORY_C => "C",
        );
    }

}

