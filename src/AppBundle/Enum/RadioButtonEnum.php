<?php

namespace AppBundle\Enum;

class RadioButtonEnum extends AbstractEnum
{
    const NA = 0;
    const YES = 1;
    const NO = 2;

    /**
     * {@inheritdoc }
     */
    public static function getLabelConstMap()
    {
        return array(
            self::NA => 'Nincs megadva',
            self::YES => 'Igen',
            self::NO => 'Nem',
        );
    }

}

