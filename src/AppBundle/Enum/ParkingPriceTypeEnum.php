<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class ParkingPriceTypeEnum extends AbstractEnum
{
    const FT = 1;
    const EU = 2;

    /**
     * {@inheritdoc }
     */
    public static function getLabelConstMapConainer()
    {
        return array(
            self::FT => 'e Ft/hó/gk',
            self::EU => '€/hó/gk',
        );
    }

}

