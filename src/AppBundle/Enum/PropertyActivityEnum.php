<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class PropertyActivityEnum extends AbstractEnum
{
    const MODOSITAS = 'MODOSITAS';
    const MUTATAS = 'MUTATAS';
    const VETELI_AJANLAT = 'VETELI_AJANLAT';
    const TRANZAKCIO = 'TRANZAKCIO';
    const ELSZAMOLAS = 'ELSZAMOLAS';
    const EGYEB = 'EGYEB';


    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::MODOSITAS => 'módosítás',
            self::MUTATAS => 'mutatás',
            self::VETELI_AJANLAT => 'vételi ajánlat',
            self::TRANZAKCIO => 'tranzakció',
            self::ELSZAMOLAS => 'elszámolás, jutalék összege',
            self::EGYEB => 'egyéb',
        );
    }

}

