<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class ConditionEnum extends AbstractEnum
{
    const UJEPITESU = 'UJEPITESU';
    const UJSZERU = 'UJSZERU';
    const FELUJITOTT = 'FELUJITOTT';
    const JO = 'JO';
    const KOZEPES = 'KOZEPES';
    const FELUJITANDO = 'FELUJITANDO';
    const UJPARCELLAZASU = 'UJPARCELLAZASU';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::UJEPITESU => 'új építésű',
            self::UJSZERU => 'újszerű',
            self::FELUJITOTT => 'felújított',
            self::JO => 'jó állapotú',
            self::KOZEPES => 'közepes állapotú',
            self::FELUJITANDO => 'felújítandó',
            self::UJPARCELLAZASU => 'új parcellázású',
        );
    }

}

