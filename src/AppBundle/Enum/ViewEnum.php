<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

class ViewEnum extends AbstractEnum
{
    const UDVARI = 'UDVARI';
    const UTCAI = 'UTCAI';
    const PANORAMAS = 'PANORAMAS';
    const KERTRENEZO = 'KERTRENEZO';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::UDVARI => 'udvari',
            self::UTCAI => 'utcai',
            self::PANORAMAS => 'panorámás',
            self::KERTRENEZO => 'kertre néző',
        );
    }

}

