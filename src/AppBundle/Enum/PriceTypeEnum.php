<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class PriceTypeEnum extends AbstractEnum
{
    const HUF = 1;
    const EUR = 2;
    const BOTH = 3;

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::HUF => 'fix forint ár',
            self::EUR => 'fix euró ár',
            self::BOTH => 'fix forint és euró ár',
        );
    }

}

