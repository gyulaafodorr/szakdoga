<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class AtticEnum extends AbstractEnum
{
    const NINCS_ADAT = 'NINCS_ADAT';
    const TETOTERI = 'TETOTERI';
    const NEM_TETOTERI = 'NEM_TETOTERI';
    const LEGFELSO_EMELET = 'LEGFELSO_EMELET';
    const ZAROSZINT = 'ZAROSZINT';
    const PENTHOUSE = 'PENTHOUSE';
    const BEEPITETT = 'BEEPITETT';
    const BEEPITHETO = 'BEEPITHETO';
    const NEM_BEEPITHETO = 'NEM_BEEPITHETO';


    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::NINCS_ADAT => 'nincs adat',
            self::TETOTERI => 'tetőtéri',
            self::NEM_TETOTERI => 'nem tetőtéri',
            self::LEGFELSO_EMELET => 'legfelső emelet, nem tetőtéri',
            self::ZAROSZINT => 'zárószint',
            self::PENTHOUSE => 'penthouse',
            self::BEEPITETT => 'beépített',
            self::BEEPITHETO => 'beépíthető',
            self::NEM_BEEPITHETO => 'nem beépíthető',
        );
    }

}

