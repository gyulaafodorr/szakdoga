<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class PhoneTypeEnum extends AbstractEnum
{
    const MOBILE = 'MOBILE';
    const HOME = 'HOME';
    const WORK = 'WORK';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::MOBILE => 'mobil',
            self::HOME => 'otthoni',
            self::WORK => 'munkahelyi',
        );
    }

}

