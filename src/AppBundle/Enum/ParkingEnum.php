<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class ParkingEnum extends AbstractEnum
{
    const NINCS_ADAT = 'NINCS_ADAT';
    const GARAZS_ARBAN = 'GARAZS_ARBAN';
    const GARAZS_MEGVASAROLHATO = 'GARAZS_MEGVASAROLHATO';
    const KULTERI_PARKOLO_ARBAN = 'KULTERI_PARKOLO_ARBAN';
    const KULTERI_PARKOLO_MEGVASAROLHATO = 'KULTERI_PARKOLO_MEGVASAROLHATO';
    const UTCAN_INGYENES = 'UTCAN_INGYENES';
    const UTCAN_FIZETOS = 'UTCAN_FIZETOS';
    const TEREMGARAZS_ARBAN = 'TEREMGARAZS_ARBAN';
    const TEREMGARAZS_MEGVASAROLHATO = 'TEREMGARAZS_MEGVASAROLHATO';
    const TEREMGARAZS_BERELHETO = 'TEREMGARAZS_BERELHETO';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::NINCS_ADAT => 'nincs adat',
            self::GARAZS_ARBAN => 'garázs - az árban',
            self::GARAZS_MEGVASAROLHATO => 'garázs - megvehető',
            self::KULTERI_PARKOLO_ARBAN => 'kültéri - az árban',
            self::KULTERI_PARKOLO_MEGVASAROLHATO => 'kültéri parkoló - megvehető',
            self::UTCAN_INGYENES => 'utcán, közterületen - ingyenes',
            self::UTCAN_FIZETOS => 'utcán, közterületen - fizetős',
            self::TEREMGARAZS_ARBAN => 'teremgarázs - az árban',
            self::TEREMGARAZS_MEGVASAROLHATO => 'teremgarázs - megvehető',
            self::TEREMGARAZS_BERELHETO => 'teremgarázs - bérelhető',
        );
    }

}

