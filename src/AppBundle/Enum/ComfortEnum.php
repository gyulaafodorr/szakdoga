<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class ComfortEnum extends AbstractEnum
{
    const LUXUS = 'LUXUS';
    const DUPLA = 'DUPLA';
    const OSSZ = 'OSSZ';
    const KOMFORT = 'KOMFORT';
    const FEL = 'FEL';
    const KOMFORT_NELKULI = 'KOMFORT_NELKULI';

    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::LUXUS => 'luxus',
            self::DUPLA => 'duplakomfortos',
            self::OSSZ => 'összkomfortos',
            self::KOMFORT => 'komfortos',
            self::FEL => 'félkomfortos',
            self::KOMFORT_NELKULI => 'komfort nélküli',
        );
    }

}

