<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 07. 06.
 * Time: 21:56
 */

namespace AppBundle\Enum;

use AppBundle\Enum\AbstractEnum;

class BathroomToiletEnum extends AbstractEnum
{
    const KULON = 'KULON';
    const EGYBEN = 'EGYBEN';
    const KULONESEGYBEN = 'KULONESEGYBEN';


    /**
     * {@inheritdoc }
     */
    protected static function getLabelConstMapConainer()
    {
        return array(
            self::KULON => 'külön',
            self::EGYBEN => 'egyben',
            self::KULONESEGYBEN => 'külön és egyben is',
        );
    }

}

