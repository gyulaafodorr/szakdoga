<?php

namespace AppBundle;

use AppBundle\Event\EventDispatcher;
use AppBundle\Event\SymfonyEventDispatcherHandler;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function boot()
    {
        parent::boot();

        $handler = new SymfonyEventDispatcherHandler($this->container->get('event_dispatcher'));
        EventDispatcher::setEventHandler($handler);
    }
}
