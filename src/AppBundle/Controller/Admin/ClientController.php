<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 5/3/17
 * Time: 10:39 AM
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Dto\Activity;
use AppBundle\Dto\Client;
use AppBundle\Helper\PaginatorHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\ClientService;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class ClientController extends AbstractAdminController
{
    /**
     * @var ClientService
     */
    protected $service;

    /**
     * @var EntityManager
     */
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $this->get('doctrine.orm.dto_entity_manager');
        $this->service = $this->get('app.service.client');
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/create", name="client_create")
     */
    public function createAction(Request $request)
    {
        $clientDto = new Client();
        $form = $this->createForm('AppBundle\Form\ClientType', $clientDto);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $clientDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();
                $clientDto->setAdministrator($administrator);

                $clientId = $this->service->create($clientDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen létrejött.');
                return $this->redirectToRoute('client_show', array('id' => $clientId));
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/client/create.html.twig', array(
            'client' => $clientDto,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/client", name="client_index")
     */
    public function indexAction(Request $request, $page = 1)
    {
        /** @var Paginator $paginator */
        $paginator = $this->em->getRepository('AppBundle\Dto\Client')
            ->getAll($this->getFilter($request), $this->getOrder($request), $page);

        $maxPages = ceil($paginator->count() / PaginatorHelper::LIMIT);
        $list = $paginator->getIterator();

        return $this->render('admin/client/index.html.twig',
            compact('list', 'maxPages', 'page')
        );
    }

    /**
     * @Route("client/filter/show", name="client_show_filter")
     */
    public function showFilter(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ClientFilterType', $this->getFilter($request), array(
            'em' => $this->em,
        ));

        return $this->render('admin/client/filter.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("client/filter", name="client_filter")
     */
    public function setFilter(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ClientFilterType', null, array(
            'em' => $this->em,
        ));
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                if ($request->get('reset')) {
                    $request->getSession()->set($this->getFilterKey($request), array());
                    $request->getSession()->set($this->getOrderKey($request), array('order' => '', 'current' => ''));
                }
                else {
                    $filterData = $form->getData();

                    $request->getSession()->set($this->getFilterKey($request), $filterData);
                }
                $request->getSession()->set($this->getPageKey($request), 1);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Hibás form: ' . $form->getErrorsAsString());
            }
        }
        return $this->getRedirectResponse($request);
    }


    /**
     * @Security("has_role('ROLE_ADMIN') and is_granted('edit', clientDto)")
     * @Route("client/{id}/update", name="client_update")
     */
    public function updateAction(Request $request, Client $clientDto)
    {
        $this->denyAccessUnlessGranted('edit', $clientDto);

        $form = $this->createForm('AppBundle\Form\ClientType', $clientDto);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $clientDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();
                $clientDto->setAdministrator($administrator);

                $clientId = $this->service->update($clientDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem módosításai sikeresen mentve.');
                return $this->redirectToRoute('client_show', array('id' => $clientId));
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/client/update.html.twig', array(
            'client' => $clientDto,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("client/{id}/assign", name="client_assign")
     */
    public function assignAction(Request $request, Client $clientDto)
    {
        $form = $this->createForm('AppBundle\Form\ChooseAdministratorType');
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $data = $form->getData();

                $this->service->assign($clientDto->getId(), $data['administrator']->getId());

                $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
                return $this->redirectToRoute('client_show', ['id' => $clientDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/client/assign.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
        ));
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{id}/events", name="client_events")
     */
    public function eventsAction(Request $request, Client $clientDto)
    {
        $contract = null;
        if ($request->get('contractId')){
            $contract = $this->em
                ->getRepository('AppBundle\Dto\Contract')
                ->find($request->get('contractId'));
        }

        $list= $this->em->getRepository('AppBundle\Dto\Activity')
            ->getAllByClient($clientDto->getId(), $contract ? $contract->getId() : null);

        $activityDto = new Activity();
        $activityDto->setContract($contract);

        $form = $this->createForm('AppBundle\Form\ActivityType', $activityDto, array(
            'clientId' => $clientDto->getId(),
        ));
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $activityDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();
                $activityDto->setAdministrator($administrator);
                $activityDto->setClient($clientDto);

                $this->service->createEvent($activityDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az esemény sikeresen létrejött.');
                return $this->redirectToRoute('client_events', ['id' => $clientDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/client/events.html.twig', array(
            'client' => $clientDto,
            'form' => $form->createView(),
            'list' => $list,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{id}/show", name="client_show")
     */
    public function showAction(Request $request, Client $clientDto)
    {
        $form = $this->createForm('AppBundle\Form\ClientType', $clientDto);

        return $this->render('admin/client/show.html.twig', array(
            'client' => $clientDto,
            'form' => $form->createView(),
        ));
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOrder(Request $request)
    {
        return $this->render('admin/client/order.html.twig', array(
            'order' => $request->get('order'),
            'current' => $this->getOrder($request),
        ));
    }

    /**
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Route("client/{id}/archive", name="client_archive")
     */
    public function archiveAction(Request $request, $id)
    {
        $this->service->archive($id);

        $request->getSession()->getFlashBag()->add('notice', 'Az elem archiválva.');
        return $this->redirectToRoute('client_index');
    }


    protected function getRedirectResponse(Request $request)
    {
        return $this->redirectToRoute('client_index');
    }


}