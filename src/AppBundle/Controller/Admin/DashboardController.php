<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DashboardController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/", name="admin_dashboard")
     */
    public function indexAction(Request $request)
    {
        return $this->render('admin/dashboard.html.twig', []);
    }
}
