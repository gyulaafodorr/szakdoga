<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 5/3/17
 * Time: 10:39 AM
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Dto\Client;
use AppBundle\Dto\Contract;
use AppBundle\Dto\Property;
use AppBundle\Form\PropertyType;
use AppBundle\Helper\PaginatorHelper;
use AppBundle\Service\PropertyService;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\ContractService;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ContractController extends AbstractAdminController
{
    /**
     * @var ContractService
     */
    protected $service;

    /**
     * @var PropertyService
     */
    protected $propertyService;

    /**
     * @var EntityManager
     */
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $this->get('doctrine.orm.dto_entity_manager');
        $this->service = $this->get('app.service.contract');
        $this->propertyService = $this->get('app.service.property');
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("contract/filter/show", name="contract_show_filter")
     */
    public function showFilter(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ContractFilterType', $this->getFilter($request), array(
            'em' => $this->em,
        ));

        return $this->render('admin/contract/filter.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("contract/filter", name="contract_filter")
     */
    public function setFilter(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ContractFilterType', null, array(
            'em' => $this->em,
        ));
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                if ($request->get('reset')) {
                    $request->getSession()->set($this->getFilterKey($request), array());
                    $request->getSession()->set($this->getOrderKey($request), array('order' => '', 'current' => ''));
                }
                else {
                    $filterData = $form->getData();

                    $request->getSession()->set($this->getFilterKey($request), $filterData);
                }
                $request->getSession()->set($this->getPageKey($request), 1);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Hibás form: ' . $form->getErrorsAsString());
            }
        }
        return $this->getRedirectResponse($request);
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/contract/{page}", name="contract_index")
     */
    public function indexAction(Request $request, $page = 1)
    {
        /** @var Paginator $paginator */
        $paginator = $this->em->getRepository('AppBundle\Dto\Contract')
            ->getAll($this->getFilter($request), $this->getOrder($request), $page);

        $maxPages = ceil($paginator->count() / PaginatorHelper::LIMIT);
        $list = $paginator->getIterator();
        return $this->render('admin/contract/index.html.twig',
            compact('list', 'maxPages', 'page')
        );
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/contract/create", name="contract_create")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function createAction(Request $request, Client $clientDto)
    {
        $form = $this->createForm('AppBundle\Form\ContractType', new Contract());
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $contractDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();
                $contractDto->setAdministrator($administrator);

                $contractDto->setClient($clientDto);

                $id = $this->service->create($contractDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen létrejött.');
                return $this->redirectToRoute('contract_assign_property', array('clientId' => $clientDto->getId(), 'id' => $id));
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/contract/create.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
            'step' => 1,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN') and is_granted('edit', contractDto)")
     * @Route("client/{clientId}/contract/update/{id}", name="contract_update")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function updateAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        $form = $this->createForm('AppBundle\Form\ContractType', $contractDto);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $contractDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();
                $contractDto->setAdministrator($administrator);

                $id = $this->service->update($contractDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem módosításai sikeresen mentve.');
                if (!$contractDto->getProperty()) {
                    return $this->redirectToRoute('contract_assign_property', array('clientId' => $clientDto->getId(), 'id' => $contractDto->getId()));
                }
                else {
                    return $this->redirectToRoute('property_update_contract', array('clientId' => $clientDto->getId(), 'id' => $contractDto->getId()));
                }
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/contract/update.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
            'contract' => $contractDto,
            'property' => $contractDto->getProperty(),
            'step' => 1,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN') and is_granted('edit', contractDto)")
     * @Route("client/{clientId}/contract/{id}/assignProperty", name="contract_assign_property")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function assignPropertyAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        if ($request->isMethod('GET')) {
            if ($request->get('action') == "assign" && $request->get('propertyId')) {
                $this->service->assignProperty($contractDto->getId(), $request->get('propertyId'));

                $request->getSession()->getFlashBag()->add('notice', 'Az Ingatlant sikeresen a Megbízáshoz rendeltük.');
                return $this->redirectToRoute('property_update_contract', ['clientId' => $clientDto->getId(), 'id' => $contractDto->getId()]);
            }
        }

        /** @var Paginator $paginator */
        $paginator = $this->em->getRepository('AppBundle\Dto\Property')
            ->getAll(array(
                'client' => $clientDto,
            ), 1, 10000);

        $list = $paginator->getIterator();

        $form = $this->createForm(PropertyType::class, new Property());
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $propertyDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();

                $propertyDto->setClient($clientDto);
                $propertyDto->setAdministrator($administrator);

                $propertyId = $this->propertyService->create($propertyDto);
                $this->service->assignProperty($contractDto->getId(), $propertyId);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen létrejött.');
                return $this->redirectToRoute('property_media_contract', ['clientId' => $clientDto->getId(), 'id' => $contractDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/contract/assignProperty.html.twig', array(
            'form' => $form->createView(),
            'list' => $list,
            'client' => $clientDto,
            'contract' => $contractDto,
            'step' => 2,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/contract/show/{id}", name="contract_show")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function showAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        $form = $this->createForm('AppBundle\Form\ContractType', $contractDto);

        return $this->render('admin/contract/show.html.twig', array(
            'contract' => $contractDto,
            'client' => $contractDto->getClient(),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') and is_granted('edit', contractDto)")
     * @Route("client/{clientId}/contract/{id}/assign", name="contract_assign")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function assignAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        $form = $this->createForm('AppBundle\Form\ChooseAdministratorType');
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $data = $form->getData();

                $this->service->assignAdministrator($clientDto->getId(), $data['administrator']->getId());

                $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
                return $this->redirectToRoute('contract_show', ['clientId' => $clientDto->getId(), 'id' => $clientDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/contract/assignAdministrator.html.twig', array(
            'form' => $form->createView(),
            'contract' => $contractDto,
            'client' => $clientDto,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN') and is_granted('edit', contractDto)")
     * @Route("client/{clientId}/contract/{id}/close", name="contract_close")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function closeAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        $form = $this->createForm('AppBundle\Form\CloseContractType');
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $data = $form->getData();

                $this->service->close($clientDto->getId(), $data['result']);

                $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
                return $this->redirectToRoute('contract_show', ['clientId' => $clientDto->getId(), 'id' => $clientDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/contract/close.html.twig', array(
            'form' => $form->createView(),
            'contract' => $contractDto,
            'client' => $clientDto,
        ));
    }

    /**
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Route("client/{clientId}/contract/archive/{id}", name="contract_archive")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function archiveAction(Request $request, Client $clientDto, $id)
    {
        $this->service->archive($id);

        $request->getSession()->getFlashBag()->add('notice', 'Az elem archiválva.');
        return $this->redirectToRoute('client_show', ['id' => $clientDto->getId()]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/contract/activate/{id}", name="contract_activate")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function activateAction(Request $request, Client $clientDto, $id)
    {
        $this->service->activate($id);

        $request->getSession()->getFlashBag()->add('notice', 'Az elem aktiválva.');
        return $this->redirectToRoute('contract_show', ['clientId' => $clientDto->getId(), 'id' => $id]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOrder(Request $request)
    {
        return $this->render('admin/contract/order.html.twig', array(
            'order' => $request->get('order'),
            'current' => $this->getOrder($request),
        ));
    }

    protected function getRedirectResponse(Request $request)
    {
        return $this->redirectToRoute('contract_index');
    }

}