<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 03.
 * Time: 13:48
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Dto\Client;
use AppBundle\Dto\Contract;
use AppBundle\Dto\Property;
use AppBundle\Enum\PropertyTypeEnum;
use AppBundle\Form\PropertyMediaType;
use AppBundle\Form\PropertyType;
use AppBundle\Service\PropertyService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Tools\Pagination\Paginator;
use AppBundle\Helper\PaginatorHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class PropertyController
 */
class PropertyController extends AbstractAdminController
{
    /**
     * @var PropertyService
     */
    protected $service;

    /**
     * @var EntityManager
     */
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $this->get('doctrine.orm.dto_entity_manager');
        $this->service = $this->get('app.service.property');
    }


    /**
     * @Route("property/filter/show", name="property_show_filter")
     */
    public function showFilter(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\PropertyFilterType', $this->getFilter($request), array(
            'em' => $this->em,
        ));

        return $this->render('admin/property/filter.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("property/filter", name="property_filter")
     */
    public function setFilter(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\PropertyFilterType', null, array(
            'em' => $this->em,
        ));
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                if ($request->get('reset')) {
                    $request->getSession()->set($this->getFilterKey($request), array());
                    $request->getSession()->set($this->getOrderKey($request), array('order' => '', 'current' => ''));
                }
                else {
                    $filterData = $form->getData();

                    $request->getSession()->set($this->getFilterKey($request), $filterData);
                }
                $request->getSession()->set($this->getPageKey($request), 1);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Hibás form: ' . $form->getErrorsAsString());
            }
        }
        return $this->getRedirectResponse($request);
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/create", name="property_create")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function createAction(Request $request, Client $clientDto)
    {
        $form = $this->createForm(PropertyType::class, new Property());
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $propertyDto = $form->getData();

                $administrator = $this->get('security.token_storage')->getToken()->getUser();

                $propertyDto->setClient($clientDto);
                $propertyDto->setAdministrator($administrator);

                $id = $this->service->create($propertyDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen létrejött.');
                return $this->redirectToRoute('property_media', ['clientId' => $clientDto->getId(), 'id' => $id]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/property/create.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
            'step' => 2,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/update/{id}", name="property_update")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function updateAction(Request $request, Client $clientDto, Property $propertyDto)
    {
        $form = $this->createForm(PropertyType::class, $propertyDto);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $propertyDto = $form->getData();

                $this->service->update($propertyDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen módosult.');
                return $this->redirectToRoute('property_media', ['clientId' => $clientDto->getId(), 'id' => $propertyDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/property/update.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
            'property' => $propertyDto,
            'step' => 2,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/updateContract/{id}", name="property_update_contract")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function updateContractAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        if (!$contractDto->getProperty()) {
            $request->getSession()->getFlashBag()->add('error', 'Ehhez a megbízáshoz még nem tartozik Ingatlan!');
            return $this->redirectToRoute('contract_update', ['clientId' => $clientDto->getId(), 'id' => $contractDto->getId()]);
        }

        /** @var Property $property */
        $property = $this->em->getRepository('AppBundle\Dto\Property')->find($contractDto->getProperty()->getId());

        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $propertyDto = $form->getData();

                $this->service->update($propertyDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen módosult.');
                return $this->redirectToRoute('property_media_contract', ['clientId' => $clientDto->getId(), 'id' => $contractDto->getId()]);
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/property/update.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
            'property' => $property,
            'contract' => $contractDto,
            'step' => 2,
        ));
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/media/{id}", name="property_media")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function mediaAction(Request $request, Client $clientDto, Property $propertyDto)
    {

        return $this->render('admin/property/media.html.twig', array(
            'client' => $clientDto,
            'property' => $propertyDto,
            'step' => 3,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/media/{id}/items", name="property_media_items")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function mediaItemsAction(Request $request, Client $clientDto, Property $propertyDto)
    {
        return $this->render('admin/property/mediaItems.html.twig', array(
            'client' => $clientDto,
            'property' => $propertyDto,
        ));
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/media/{id}/remove/{mediaId}", name="property_media_remove")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function mediaRemoveAction(Request $request, Client $clientDto, Property $propertyDto, $mediaId)
    {
        if ($mediaId) {
            $this->service->removeMedia($propertyDto->getId(), $mediaId);
        }
        return new Response("ok");
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("client/{clientId}/property/mediaContract/{id}", name="property_media_contract")
     * @ParamConverter("clientDto", class="AppBundle\Dto\Client", options={"id" = "clientId"})
     */
    public function mediaContractAction(Request $request, Client $clientDto, Contract $contractDto)
    {
        if (!$contractDto->getProperty()) {
            $request->getSession()->getFlashBag()->add('error', 'Ehhez a megbízáshoz még nem tartozik Ingatlan!');
            return $this->redirectToRoute('contract_update', ['clientId' => $clientDto->getId(), 'id' => $contractDto->getId()]);
        }

        $propertyDto = $contractDto->getProperty();

        $form = $this->createForm(PropertyType::class, $propertyDto);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
//                $propertyDto = $form->getData();
//
//                $client = $this->em->getRepository('AppBundle\Dto\Client')->find($property->getClient()->getId());
//                $administrator = $this->get('security.token_storage')->getToken()->getUser();
//
//                $propertyDto->setClient($client);
//                $propertyDto->setAdministrator($administrator);
//
//
//                $this->service->update($propertyDto);
//
//                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen módosult.');
                return $this->redirectToRoute('property_index');
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/property/media.html.twig', array(
            'form' => $form->createView(),
            'client' => $clientDto,
            'property' => $propertyDto,
            'contract' => $contractDto,
            'step' => 3,
        ));
    }


    /**
     * @Route("/subTypes", name="property_subtypes")
     */
    public function getSubTypes(Request $request)
    {
        $params = $request->get('depdrop_all_params');
        $ret = array();

        // ha van kerület, csak Budapesten vesszük figyelembe
        $list = PropertyTypeEnum::getLabelConstSubtypeMap($params['property-type']);
        foreach ($list as $k => $b) {
            $ret[] = array(
                'id' => $k,
                'name' => PropertyTypeEnum::getSubTypeLabelByConst($params['property-type'], $k),
            );
        }

        return new JsonResponse(array('output' => $ret, 'selected' => ''));
    }


    /**
     * @Route("/property/cities", name="property_cities")
     */
    public function getCities(Request $request)
    {
        $params = $request->get('depdrop_all_params');
        $ret = array();

        if ($params['property-county']) {
            $list = $this->em
                ->getRepository('AppBundle\Dto\City')
                ->createQueryBuilder('c')
                ->select('c')
                ->where('c.county = :countyId')
                ->setParameter('countyId', $params['property-county'])
                ->addOrderBy('c.name')
                ->getQuery()
                ->getArrayResult();

            foreach ($list as $k) {
                $ret[] = array(
                    'id' => $k['id'],
                    'name' => $k['name'],
                );
            }
        }

        return new JsonResponse(array('output' => $ret, 'selected' => ''));
    }

    /**
     * @Route("/property/boroughs", name="property_boroughs")
     */
    public function getBoroughs(Request $request)
    {
        $params = $request->get('depdrop_all_params');
        $ret = array();

        if ($params['property-city']) {
            $q = $this->em
                ->getRepository('AppBundle\Dto\Borough')
                ->createQueryBuilder('b')
                ->select('b')
                ->innerJoin('b.city', 'c')
                ->where('c.id = :city')
                ->setParameter('city', $params['property-city'])
                ->addOrderBy('b.borough');

            if (isset($params['property-district']) && is_numeric($params['property-district'])) {
                $q->andWhere('b.district = :district')
                    ->setParameter('district', $params['property-district']);
            }

            $list = $q->getQuery()
                ->getArrayResult();

            foreach ($list as $k) {
                $ret[] = array(
                    'id' => $k['id'],
                    'name' => $k['borough'],
                );
            }
        }

        return new JsonResponse(array('output' => $ret, 'selected' => ''));
    }


    /**
     * @Route("/property/districts", name="property_districts")
     */
    public function getDistricts(Request $request)
    {
        $params = $request->get('depdrop_all_params');
        $ret = array();

        if ($params['property-city']) {
            $city = $this->em
                ->getRepository('AppBundle\Dto\City')
                ->find($params['property-city']);

            if ($city->getName() == 'Budapest') {
                for ($i = 1; $i <= 23; $i++) {
                    $ret[] = array(
                        'id' => $i,
                        'name' => $i,
                    );
                }
            }
        }

        return new JsonResponse(array('output' => $ret, 'selected' => ''));
    }

    /**
     * @Route("/property/search/city/json", name="property_search_city_json")
     */
    public function searchCityJson(Request $request)
    {
        $ret = array();

        $q = $this->em
            ->getRepository('AppBundle\Dto\City')
            ->createQueryBuilder('c')
            ->select('c')
            ->where('c.name LIKE :city')
            ->setParameter('city', $request->get('term') . '%')
            ->addGroupBy('c.name')
            ->addOrderBy('c.name');

        $list = $q->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        foreach ($list as $c) {
            $ret[] = array(
                'id' => $c['id'],
                'label' => $c['name'],
                'value' => $c['name'],
            );
        }
        return new JsonResponse($ret);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/property/{page}", name="property_index")
     */
    public function indexAction(Request $request, $page = 1)
    {

        /** @var Paginator $paginator */
        $paginator = $this->em->getRepository('AppBundle\Dto\Property')
            ->getAll($this->getFilter($request), $this->getOrder($request), $page);

        $maxPages = ceil($paginator->count() / PaginatorHelper::LIMIT);
        $list = $paginator->getIterator();

        return $this->render('admin/property/index.html.twig',
            compact('list', 'maxPages', 'page')
        );
    }


    protected function getRedirectResponse(Request $request)
    {
        return $this->redirectToRoute('property_index');
    }
}