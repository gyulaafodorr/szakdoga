<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Dto\Administrator;
use AppBundle\Form\AdministratorType;
use AppBundle\Helper\PaginatorHelper;
use AppBundle\Service\AdministratorService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdministratorController extends Controller
{
    /**
     * @var AdministratorService
     */
    protected $service;

    /**
     * @var EntityManager
     */
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $this->get('doctrine.orm.dto_entity_manager');
        $this->service = $this->get('app.service.administrator');
    }


    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/administrator/list", name="admin_administrator_list")
     */
    public function indexAction($page = 1)
    {
        /** @var Paginator $paginator */
        $paginator = $this->em->getRepository('AppBundle\Dto\Administrator')
            ->getAll(array(), $page);

        $maxPages = ceil($paginator->count() / PaginatorHelper::LIMIT);
        $list = $paginator->getIterator();

        return $this->render('admin/administrator/index.html.twig',
            compact('list', 'maxPages', 'page')
        );
    }

    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/administrator/new", name="admin_administrator_new")
     */
    public function showNewFormAction(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\AdministratorType', new Administrator());
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $administratorDto = $form->getData();

                $this->service->create($administratorDto);

                $request->getSession()->getFlashBag()->add('notice', 'Az elem sikeresen létrejött.');
                return $this->redirectToRoute('admin_administrator_list');
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/administrator/new.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/administrator/edit/{id}", name="admin_administrator_edit")
     */
    public function showEditFormAction(Request $request, $id)
    {
        $administratorDto = $this->em
            ->getRepository('AppBundle\Dto\Administrator')
            ->find($id);

        $form = $this->createForm('AppBundle\Form\AdministratorType', $administratorDto);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $administratorDto = $form->getData();

                $this->service->update($id, $administratorDto);

                $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
                return $this->redirectToRoute('admin_administrator_list');
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/administrator/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/administrator/inactivate/{id}", name="administrator_inactivate")
     */
    public function inactivateAction(Request $request, $id)
    {
        try {
            $administratorDto = $this->em
                ->getRepository('AppBundle\Dto\Administrator')
                ->find($id);

            if ($administratorDto->getContracts()->count() > 0 or $administratorDto->getClients()->count() > 0){
                $request->getSession()->getFlashBag()->add('error', 'A kért művelete nem hajtható végre, mert a felhasználóhoz még Ügyfél vagy Megbízás tartozik.');
                return $this->redirectToRoute('admin_administrator_list');
            }

            $this->service->inactivate($id);

            $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
        }
        return $this->redirectToRoute('admin_administrator_list');
    }


    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/administrator/activate/{id}", name="administrator_activate")
     */
    public function activateAction(Request $request, $id)
    {
        try {
            $this->service->activate($id);

            $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
        }
        return $this->redirectToRoute('admin_administrator_list');
    }

    /**
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_SYSTEM_ADMIN')")
     * @Route("/administrator/archive/{id}", name="administrator_archive")
     */
    public function archiveAction(Request $request, $id)
    {
        try {
            $this->service->archive($id);

            $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
        }
        return $this->redirectToRoute('admin_administrator_list');
    }


    /**
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Route("/administrator/assignAllClients/{id}", name="administrator_assign_all_clients")
     */
    public function assignAllClientAction(Request $request, $id)
    {
        $administratorDto = $this->em
            ->getRepository('AppBundle\Dto\Administrator')
            ->find($id);

        if ($administratorDto->getClients()->count() == 0){
            $request->getSession()->getFlashBag()->add('error', 'A Munkatárshoz nem tartozik Ügyfél.');
            return $this->redirectToRoute('admin_administrator_list');
        }

        $form = $this->createForm('AppBundle\Form\ChooseAdministratorType');
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $data = $form->getData();

                $this->service->assignAllClients($id, $data['administrator']->getId());

                $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
                return $this->redirectToRoute('admin_administrator_list');
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/administrator/assignAllClients.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Route("/administrator/assignAllContracts/{id}", name="administrator_assign_all_contracts")
     */
    public function assignAllContractsAction(Request $request, $id)
    {
        $administratorDto = $this->em
            ->getRepository('AppBundle\Dto\Administrator')
            ->find($id);

        if ($administratorDto->getContracts()->count() == 0){
            $request->getSession()->getFlashBag()->add('error', 'A Munkatárshoz nem tartozik Megbízás.');
            return $this->redirectToRoute('admin_administrator_list');
        }

        $form = $this->createForm('AppBundle\Form\ChooseAdministratorType');
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $data = $form->getData();

                $this->service->assignAllContracts($id, $data['administrator']->getId());

                $request->getSession()->getFlashBag()->add('notice', 'A módosításokat sikeresen mentetted.');
                return $this->redirectToRoute('admin_administrator_list');
            }
            else {
                $request->getSession()->getFlashBag()->add('error', 'Az elem mentése nem sikerült. Ellenőrizd a hibákat!');
            }
        }

        return $this->render('admin/administrator/assignAllContracts.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
