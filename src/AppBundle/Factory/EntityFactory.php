<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 12.
 * Time: 18:21
 */

namespace AppBundle\Factory;

use AppBundle\Entity\Activity;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;

class EntityFactory
{
    /**
     * @param \AppBundle\Dto\Client $dto
     * @param $administrator
     * @return Client
     */
    public function createClient(\AppBundle\Dto\Client $dto, $administrator)
    {
        return new Client(
            $dto->getFirstName(),
            $dto->getLastName(),
            $dto->getZipcode(),
            $dto->getCity(),
            $dto->getAddress(),
            $dto->getIdNumber(),
            $dto->getEmail(),
            $dto->getEmailType(),
            $dto->getContact1Phone(),
            $dto->getContact1Description(),
            $dto->getContact1Type(),
            $dto->getContact2Phone(),
            $dto->getContact2Description(),
            $dto->getContact2Type(),
            $dto->getContact3Phone(),
            $dto->getContact3Description(),
            $dto->getContact3Type(),
            $dto->getComment(),
            $dto->isIsActive(),
            $administrator
        );
    }

    /**
     * @param \AppBundle\Dto\Activity $dto
     * @param $administrator
     * @param $client
     * @param null $contract
     * @return Activity
     */
    public function createEvent(\AppBundle\Dto\Activity $dto, $administrator, $client, $contract = null)
    {
        return new Activity(
            $administrator,
            $client,
            $dto->getDescription(),
            $contract
        );
    }


    /**
     * @param \AppBundle\Dto\Contract $dto
     * @param $administrator
     * @param $client
     * @return Contract
     */
    public function createContract(\AppBundle\Dto\Contract $dto, $administrator, $client)
    {
        return new Contract(
            $dto->getContractNumber(),
            $dto->getCommission(),
            $dto->getComment(),
            $dto->getAgreementType(),
            $dto->isIsExclusive(),
            $dto->getDeadline(),
            $dto->getPrice(),
            $dto->getPriceEur(),
            $client,
            $administrator
        );
    }


}