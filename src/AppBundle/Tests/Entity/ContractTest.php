<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 05.
 * Time: 13:49
 */

namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Administrator;
use AppBundle\Entity\Borough;
use AppBundle\Entity\City;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;
use AppBundle\Entity\Property;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\AtticEnum;
use AppBundle\Enum\BathroomToiletEnum;
use AppBundle\Enum\CeilingHeightEnum;
use AppBundle\Enum\ComfortEnum;
use AppBundle\Enum\ConditionEnum;
use AppBundle\Enum\CountyEnum;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\EnergyCertificateEnum;
use AppBundle\Enum\HeatingEnum;
use AppBundle\Enum\OrientationEnum;
use AppBundle\Enum\ParkingEnum;
use AppBundle\Enum\PhoneTypeEnum;
use AppBundle\Enum\PropertyTypeEnum;
use AppBundle\Enum\RadioButtonEnum;
use AppBundle\Enum\ViewEnum;
use AppBundle\Service\PropertyService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class ContractTest extends TestCase
{
    private $client;
    private $property;
    private $admin;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->admin = new Administrator(
            'Test Referens',
            'test_referens',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234567',
            '+36301234567',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );

        $this->client = new Client(
            'Gipsz',
            'Jakab',
            '1113',
            'Budapest',
            'Avar utca 10',
            '323445LA',
            'teszt_ugyefel@tetetetetetet.hu',
            EmailTypeEnum::PERSONAL,
            '+36301234567',
            'Desc1',
            PhoneTypeEnum::MOBILE,
            '+36301234568',
            'Desc2',
            PhoneTypeEnum::HOME,
            '+36301234569',
            'Desc3',
            PhoneTypeEnum::WORK,
            'Megjegyzés',
            true,
            $this->admin
        );

        $this->property = new Property(
            $this->admin,
            $this->client,
            Property::STATUS_ACTIVE,
            PropertyTypeEnum::TYPE_LAKAS,
            PropertyTypeEnum::SUBTYPE_LAKAS_TEGLA,
            '100',
            null,
            CountyEnum::Budapest,
            new City(),
            new Borough(),
            'Avar utca',
            false,
            '1',
            '10',
            false,
            '5',
            '1',
            '0',
            '3',
            HeatingEnum::GAZ_CIRKO,
            ComfortEnum::OSSZ,
            'Valami leírás',
            RadioButtonEnum::NO,
            ParkingEnum::GARAZS_ARBAN,
            null,
            null,
            null,
            RadioButtonEnum::YES,
            null,
            ViewEnum::UTCAI,
            null,
            null,
            ConditionEnum::FELUJITOTT,
            null,
            null,
            AtticEnum::NEM_TETOTERI,
            null,
            false,
            EnergyCertificateEnum::A,
            CeilingHeightEnum::NAGYOBB_3,
            null,
            RadioButtonEnum::NO,
            null,
            null,
            null,
            '60.000 / hó',
            RadioButtonEnum::NO,
            null,
            OrientationEnum::DELNYUGAT,
            BathroomToiletEnum::KULONESEGYBEN,
            RadioButtonEnum::YES,
            null
        );
    }

    public function testCreateFail()
    {
        $contract = new Contract(
            '',
            '3%',
            'Sürgős szerződés',
            AgreementTypeEnum::SALE,
            true,
            new \DateTime('2020-01-01'),
            '50000000',
            '160000',
            $this->client,
            $this->admin
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($contract);

        $this->assertTrue(count($errors) > 0);
    }

    public function testCreateSuccess()
    {
        $contract = new Contract(
            '2017/1',
            '3%',
            'Sürgős szerződés',
            AgreementTypeEnum::SALE,
            true,
            new \DateTime('2020-01-01'),
            '50000000',
            '160000',
            $this->client,
            $this->admin
        );

        $this->assertTrue('2017/1' == $contract->getContractNumber());

        return $contract;
    }

    /**
     * @depends testCreateSuccess
     */
    public function testClientAssignToAdministrator(Contract $contract)
    {
        $contract->assignToAdministrator($this->admin);
        $this->assertTrue($this->admin === $contract->getAdministrator());
    }

    /**
     * @depends testCreateSuccess
     */
    public function testClientAssignProperty(Contract $contract)
    {
        $contract->assignProperty($this->property);
        $this->assertTrue($this->property === $contract->getProperty());
    }

    /**
     * @depends testCreateSuccess
     */
    public function testArchive(Contract $contract)
    {
        $contract->archive();
        $this->assertTrue($contract->getStatus() === Contract::STATUS_ARCHIVED);
    }

    /**
     * @depends testCreateSuccess
     */
    public function testClose(Contract $contract)
    {
        $contract->close(Contract::RESULT_SUCCESS);
        $this->assertTrue($contract->getStatus() === Contract::STATUS_ENDED);
        $this->assertTrue($contract->getResult() === Contract::RESULT_SUCCESS);

        $contract->close(Contract::RESULT_FAILURE);
        $this->assertTrue($contract->getStatus() === Contract::STATUS_ENDED);
        $this->assertTrue($contract->getResult() === Contract::RESULT_FAILURE);
    }

    /**
     * @depends testCreateSuccess
     */
    public function testActivate(Contract $contract)
    {
        $contract->activate();
        $this->assertTrue($contract->getStatus() === Contract::STATUS_ACTIVE);
    }

}