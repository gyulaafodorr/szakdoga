<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 05.
 * Time: 13:49
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Activity;
use AppBundle\Entity\Administrator;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\PhoneTypeEnum;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class ActivityTest extends TestCase
{
    private $admin;
    private $client;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->admin = new Administrator(
            'Test Referens',
            'test_referens',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234567',
            '+36301234567',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );

        $this->client = new Client(
            'Gipsz',
            'Jakab',
            '1113',
            'Budapest',
            'Avar utca 10',
            '323445LA',
            'teszt_ugyefel@tetetetetetet.hu',
            EmailTypeEnum::PERSONAL,
            '+36301234567',
            'Desc1',
            PhoneTypeEnum::MOBILE,
            '+36301234568',
            'Desc2',
            PhoneTypeEnum::HOME,
            '+36301234569',
            'Desc3',
            PhoneTypeEnum::WORK,
            'Megjegyzés',
            true,
            $this->admin
        );
    }

    public function testCreateFail()
    {
        $activity = new Activity(
            $this->admin,
            $this->client,
            null
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($activity);

        $this->assertTrue(count($errors) > 0);
    }

    public function testCreateSuccess()
    {
        $activity = new Activity(
            $this->admin,
            $this->client,
            'Teszt esemény.'
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($activity);

        $this->assertEquals(0, count($errors));

        $this->assertTrue('Teszt esemény.' == $activity->getDescription());
        $this->assertTrue($this->client === $activity->getClient());
        $this->assertTrue($this->admin === $activity->getAdministrator());

        return $activity;
    }

    public function testCreateWithContract()
    {
        $contract = new Contract(
            '2017/1',
            '3%',
            'Sürgős szerződés',
            AgreementTypeEnum::SALE,
            true,
            new \DateTime('2020-01-01'),
            '50000000',
            '160000',
            $this->client,
            $this->admin
        );


        $activity = new Activity(
            $this->admin,
            $this->client,
            'Teszt esemény.',
            $contract
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($activity);

        $this->assertEquals(0, count($errors));

        $this->assertTrue('Teszt esemény.' == $activity->getDescription());
        $this->assertTrue($this->client === $activity->getClient());
        $this->assertTrue($this->admin === $activity->getAdministrator());
        $this->assertTrue($contract === $activity->getContract());

        return $activity;
    }

}