<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 05.
 * Time: 13:49
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Administrator;
use AppBundle\Entity\Client;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\PhoneTypeEnum;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class ClientTest extends TestCase
{
    private $admin;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->admin = new Administrator(
            'Test Referens',
            'test_referens',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234567',
            '+36301234567',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );
    }

    public function testCreateFail()
    {
        $client = new Client(
            '',
            '',
            '1113',
            'Budapest',
            'Avar utca 10',
            '323445LA',
            'teszt_ugyefel@tetetetetetet.hu',
            EmailTypeEnum::PERSONAL,
            '+36301234567',
            'Desc1',
            PhoneTypeEnum::MOBILE,
            '+36301234568',
            'Desc2',
            PhoneTypeEnum::HOME,
            '+36301234569',
            'Desc3',
            PhoneTypeEnum::WORK,
            'Megjegyzés',
            true,
            $this->admin
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($client);

        $this->assertTrue(count($errors) > 0);
    }

    public function testCreateSuccess()
    {
        $client = new Client(
            'Gipsz',
            'Jakab',
            '1113',
            'Budapest',
            'Avar utca 10',
            '323445LA',
            'teszt_ugyefel@tetetetetetet.hu',
            EmailTypeEnum::PERSONAL,
            '+36301234567',
            'Desc1',
            PhoneTypeEnum::MOBILE,
            '+36301234568',
            'Desc2',
            PhoneTypeEnum::HOME,
            '+36301234569',
            'Desc3',
            PhoneTypeEnum::WORK,
            'Megjegyzés',
            true,
            $this->admin
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($client);

        $this->assertEquals(0, count($errors));

        $this->assertTrue('Gipsz' == $client->getFirstname());
        $this->assertTrue('Jakab' == $client->getLastname());
        $this->assertTrue('1113' == $client->getZipcode());
        $this->assertTrue('Budapest' == $client->getCity());
        $this->assertTrue('Avar utca 10' == $client->getAddress());
        $this->assertTrue('323445LA' == $client->getIdNumber());
        $this->assertTrue('teszt_ugyefel@tetetetetetet.hu' == $client->getEmail());
        $this->assertTrue(EmailTypeEnum::PERSONAL == $client->getEmailType());
        $this->assertTrue('+36301234567' == $client->getContact1Phone());
        $this->assertTrue('Desc1' == $client->getContact1Description());
        $this->assertTrue(PhoneTypeEnum::MOBILE == $client->getContact1Type());
        $this->assertTrue('+36301234568' == $client->getContact2Phone());
        $this->assertTrue('Desc2' == $client->getContact2Description());
        $this->assertTrue(PhoneTypeEnum::HOME == $client->getContact2Type());
        $this->assertTrue('+36301234569' == $client->getContact3Phone());
        $this->assertTrue('Desc3' == $client->getContact3Description());
        $this->assertTrue(PhoneTypeEnum::WORK == $client->getContact3Type());
        $this->assertTrue('Megjegyzés' == $client->getComment());
        $this->assertTrue(true == $client->isActive());
        $this->assertTrue($this->admin === $client->getAdministrator());

        return $client;
    }

    /**
     * @depends testCreateSuccess
     */
    public function testClientArchivate(Client $client)
    {

        $clone = clone $client;

        $client->archive();

        $this->assertTrue($client->getFirstname() == substr(md5($clone->getFirstname()), 0, 10));
        $this->assertTrue($client->getLastname() == substr(md5($clone->getLastname()), 0, 10));
        $this->assertTrue($client->getIdNumber() == substr(md5($clone->getIdNumber()), 0, 10));
        $this->assertTrue($client->getEmail() == substr(md5($clone->getEmail()), 0, 10));
        $this->assertTrue($client->getContact1Phone() == substr(md5($clone->getContact1Phone()), 0, 10));
        $this->assertTrue($client->getContact1Description() == substr(md5($clone->getContact1Description()), 0, 10));
        $this->assertTrue($client->getContact2Phone() == substr(md5($clone->getContact2Phone()), 0, 10));
        $this->assertTrue($client->getContact2Description() == substr(md5($clone->getContact2Description()), 0, 10));
        $this->assertTrue($client->getContact3Phone() == substr(md5($clone->getContact3Phone()), 0, 10));
        $this->assertTrue($client->getContact3Description() == substr(md5($clone->getContact3Description()), 0, 10));
        $this->assertFalse($client->isActive());

    }

    /**
     * @depends testCreateSuccess
     */
    public function testClientAssign(Client $client)
    {
        $adminNew = new Administrator(
            'Test Referens11',
            'test_referens11',
            'test_referens11@testtestte.hu',
            '+36301234522',
            '+36301234533',
            '+36301234544',
            'valami11',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );
        $client->assignToAdministrator($adminNew);

        $this->assertTrue($client->getAdministrator() === $adminNew);
    }
}