<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 05.
 * Time: 14:00
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Administrator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class AdministratorTest extends TestCase
{
    public function testCreateFail()
    {
        $admin = new Administrator(
            'Test Referens',
            '',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234567',
            '+36301234567',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($admin);

        $this->assertTrue(count($errors) > 0);
    }

    public function testCanCreateWithValidEmail()
    {
        $admin = new Administrator(
            'Test Referens',
            'test_referens',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234568',
            '+36301234569',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );

        $this->assertTrue('Test Referens' == $admin->getName());
        $this->assertTrue('test_referens' == $admin->getUsername());
        $this->assertTrue('test_referens@testtestte.hu' == $admin->getEmail());
        $this->assertTrue('+36301234567' == $admin->getPhone());
        $this->assertTrue('+36301234568' == $admin->getPhone2());
        $this->assertTrue('+36301234569' == $admin->getPhone3());
        $this->assertTrue(Administrator::ROLE_ADMIN == $admin->getRole());
        $this->assertTrue(Administrator::STATUS_ACTIVE == $admin->getStatus());

        return $admin;
    }

    /**
     * @depends testCanCreateWithValidEmail
     */
    public function testInactivate(Administrator $admin)
    {
        $admin->inactivate();
        $this->assertTrue(Administrator::STATUS_INACTIVE == $admin->getStatus());
    }

    /**
     * @depends testCanCreateWithValidEmail
     */
    public function testActivate(Administrator $admin)
    {
        $admin->activate();
        $this->assertTrue(Administrator::STATUS_ACTIVE == $admin->getStatus());
    }


    /**
     * @depends testCanCreateWithValidEmail
     */
    public function testArchive(Administrator $admin)
    {
        $admin->archive();
        $this->assertTrue(Administrator::STATUS_ARCHIVED == $admin->getStatus());
    }

}