<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 05.
 * Time: 13:49
 */

namespace AppBundle\Tests\Entity;


use AppBundle\Entity\Activity;
use AppBundle\Entity\Administrator;
use AppBundle\Entity\Changelog;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\PhoneTypeEnum;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class ChangelogTest extends TestCase
{
    private $admin;
    private $client;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->admin = new Administrator(
            'Test Referens',
            'test_referens',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234567',
            '+36301234567',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );

    }

    public function testCreateFail()
    {
        $changelog = new Changelog(
            $this->admin,
            null,
            null,
            '',
            '',
            ''
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($changelog);

        $this->assertTrue(count($errors) > 0);
    }

    public function testCreateSuccess()
    {
        $changelog = new Changelog(
            $this->admin,
            '10',
            'Client',
            'firstname',
            'Gipsz',
            'Gipszusz'
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($changelog);

        $this->assertEquals(0, count($errors));

        $this->assertTrue('firstname' == $changelog->getField());
        $this->assertTrue('Gipsz' == $changelog->getOldValue());
        $this->assertTrue('Gipszusz' == $changelog->getNewValue());
        $this->assertTrue($this->admin === $changelog->getAdministrator());
        $this->assertTrue('Client' == $changelog->getEntityName());
        $this->assertTrue('10' == $changelog->getEntityId());

        return $changelog;
    }

}