<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 05.
 * Time: 13:49
 */

namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Administrator;
use AppBundle\Entity\Borough;
use AppBundle\Entity\City;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;
use AppBundle\Entity\Property;
use AppBundle\Entity\PropertyMedia;
use AppBundle\Enum\AgreementTypeEnum;
use AppBundle\Enum\AtticEnum;
use AppBundle\Enum\BathroomToiletEnum;
use AppBundle\Enum\CeilingHeightEnum;
use AppBundle\Enum\ComfortEnum;
use AppBundle\Enum\ConditionEnum;
use AppBundle\Enum\CountyEnum;
use AppBundle\Enum\EmailTypeEnum;
use AppBundle\Enum\EnergyCertificateEnum;
use AppBundle\Enum\HeatingEnum;
use AppBundle\Enum\OrientationEnum;
use AppBundle\Enum\ParkingEnum;
use AppBundle\Enum\ParkingPriceTypeEnum;
use AppBundle\Enum\PhoneTypeEnum;
use AppBundle\Enum\PropertyTypeEnum;
use AppBundle\Enum\RadioButtonEnum;
use AppBundle\Enum\ViewEnum;
use AppBundle\Service\PropertyService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class PropertyTest extends TestCase
{
    private $client;
    private $property;
    private $admin;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->admin = new Administrator(
            'Test Referens',
            'test_referens',
            'test_referens@testtestte.hu',
            '+36301234567',
            '+36301234567',
            '+36301234567',
            'valami',
            Administrator::ROLE_ADMIN,
            Administrator::STATUS_ACTIVE
        );

        $this->client = new Client(
            'Gipsz',
            'Jakab',
            '1113',
            'Budapest',
            'Avar utca 10',
            '323445LA',
            'teszt_ugyefel@tetetetetetet.hu',
            EmailTypeEnum::PERSONAL,
            '+36301234567',
            'Desc1',
            PhoneTypeEnum::MOBILE,
            '+36301234568',
            'Desc2',
            PhoneTypeEnum::HOME,
            '+36301234569',
            'Desc3',
            PhoneTypeEnum::WORK,
            'Megjegyzés',
            true,
            $this->admin
        );


    }

    public function testCreateFail()
    {
        $property = new Property(
            $this->admin,
            $this->client,
            Property::STATUS_ACTIVE,
            null,
            PropertyTypeEnum::SUBTYPE_LAKAS_TEGLA,
            '100',
            null,
            CountyEnum::Budapest,
            new City(),
            new Borough(),
            'Avar utca',
            false,
            '1',
            '10',
            false,
            '5',
            '1',
            '0',
            '3',
            HeatingEnum::GAZ_CIRKO,
            ComfortEnum::OSSZ,
            'Valami leírás',
            RadioButtonEnum::NO,
            ParkingEnum::GARAZS_ARBAN,
            null,
            null,
            null,
            RadioButtonEnum::YES,
            null,
            ViewEnum::UTCAI,
            null,
            null,
            ConditionEnum::FELUJITOTT,
            null,
            null,
            AtticEnum::NEM_TETOTERI,
            null,
            false,
            EnergyCertificateEnum::A,
            CeilingHeightEnum::NAGYOBB_3,
            null,
            RadioButtonEnum::NO,
            null,
            null,
            null,
            '60.000 / hó',
            RadioButtonEnum::NO,
            null,
            OrientationEnum::DELNYUGAT,
            BathroomToiletEnum::KULONESEGYBEN,
            RadioButtonEnum::YES,
            null
        );

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($property);

        $this->assertTrue(count($errors) > 0);
    }

    public function testCreateSuccess()
    {
        $city = new City();
        $borough = new Borough();

        $property = new Property(
            $this->admin,
            $this->client,
            Property::STATUS_ACTIVE,
            PropertyTypeEnum::TYPE_LAKAS,
            PropertyTypeEnum::SUBTYPE_LAKAS_TEGLA,
            '100',
            '1000',
            CountyEnum::Budapest,
            $city,
            $borough,
            'Avar utca',
            false,
            '1',
            '10',
            false,
            '5',
            '1',
            '0',
            '3',
            HeatingEnum::GAZ_CIRKO,
            ComfortEnum::OSSZ,
            'Valami leírás',
            RadioButtonEnum::NO,
            ParkingEnum::GARAZS_ARBAN,
            RadioButtonEnum::YES,
            RadioButtonEnum::YES,
            RadioButtonEnum::YES,
            RadioButtonEnum::YES,
            RadioButtonEnum::YES,
            ViewEnum::UTCAI,
            '450/óra',
            ParkingPriceTypeEnum::FT,
            ConditionEnum::FELUJITOTT,
            '5.6',
            '150',
            AtticEnum::NEM_TETOTERI,
            RadioButtonEnum::NO,
            false,
            EnergyCertificateEnum::A,
            CeilingHeightEnum::NAGYOBB_3,
            RadioButtonEnum::NO,
            RadioButtonEnum::NO,
            RadioButtonEnum::NO,
            RadioButtonEnum::NO,
            RadioButtonEnum::NO,
            '60.000 / hó',
            RadioButtonEnum::NO,
            false,
            OrientationEnum::DELNYUGAT,
            BathroomToiletEnum::KULONESEGYBEN,
            RadioButtonEnum::YES,
            '-'
        );

        $this->assertTrue($this->admin === $property->getAdministrator());
        $this->assertTrue($this->client === $property->getClient());
        $this->assertTrue(Property::STATUS_ACTIVE == $property->getStatus());
        $this->assertTrue(PropertyTypeEnum::TYPE_LAKAS == $property->getType());
        $this->assertTrue(PropertyTypeEnum::SUBTYPE_LAKAS_TEGLA == $property->getsubType());
        $this->assertTrue(100 == $property->getSize());
        $this->assertTrue(1000 == $property->getLotSize());
        $this->assertTrue(CountyEnum::Budapest == $property->getCounty());
        $this->assertTrue($city === $property->getCity());
        $this->assertTrue($borough === $property->getBorough());
        $this->assertTrue('Avar utca' == $property->getStreet());
        $this->assertTrue(false == $property->isStreetNp());
        $this->assertTrue('1' == $property->getDistrict());
        $this->assertTrue('10' == $property->getHouseNumber());
        $this->assertTrue(false == $property->isHouseNumberNp());
        $this->assertTrue(5 == $property->getRooms());
        $this->assertTrue(1 == $property->getHalfRooms());
        $this->assertTrue(0 == $property->getFloor());
        $this->assertTrue(3 == $property->getLevels());
        $this->assertTrue(HeatingEnum::GAZ_CIRKO == $property->getHeating());
        $this->assertTrue(ComfortEnum::OSSZ == $property->getComfort());
        $this->assertTrue('Valami leírás' == $property->getDescription());
        $this->assertTrue(RadioButtonEnum::NO == $property->isHasElevator());
        $this->assertTrue(ParkingEnum::GARAZS_ARBAN == $property->getParking());
        $this->assertTrue(RadioButtonEnum::YES == $property->getHasDrain());
        $this->assertTrue(RadioButtonEnum::YES == $property->getHasElectricity());
        $this->assertTrue(RadioButtonEnum::YES == $property->getHasGas());
        $this->assertTrue(RadioButtonEnum::YES == $property->getHasWater());
        $this->assertTrue(RadioButtonEnum::YES == $property->isHasCellar());
        $this->assertTrue(ViewEnum::UTCAI == $property->getView());
        $this->assertTrue('450/óra' == $property->getParkingFee());
        $this->assertTrue(ParkingPriceTypeEnum::FT == $property->getParkingFeeUom());
        $this->assertTrue(ConditionEnum::FELUJITOTT == $property->getCondition());
        $this->assertTrue(5.6 == $property->getBalconySize());
        $this->assertTrue(150 == $property->getGardenSize());
        $this->assertTrue(AtticEnum::NEM_TETOTERI == $property->getAttic());
        $this->assertTrue(RadioButtonEnum::NO == $property->isHasFurniture());
        $this->assertTrue(false == $property->isAgentsCanHelp());
        $this->assertTrue(EnergyCertificateEnum::A == $property->getEnergyCertificate());
        $this->assertTrue(CeilingHeightEnum::NAGYOBB_3 == $property->getCeilingHeight());
        $this->assertTrue(RadioButtonEnum::NO == $property->isSmoker());
        $this->assertTrue(RadioButtonEnum::NO == $property->isHasDisabledAccess());
        $this->assertTrue(RadioButtonEnum::NO == $property->isMechanized());
        $this->assertTrue(RadioButtonEnum::NO == $property->isPetAllowed());
        $this->assertTrue(RadioButtonEnum::NO == $property->isPanelProgram());
        $this->assertTrue('60.000 / hó' == $property->getUtilityBills());
        $this->assertTrue(RadioButtonEnum::NO == $property->isHasGardenAccess());
        $this->assertTrue(false == $property->isLeaseRights());
        $this->assertTrue(OrientationEnum::DELNYUGAT == $property->getOrientation());
        $this->assertTrue(BathroomToiletEnum::KULONESEGYBEN == $property->getBathToilet());
        $this->assertTrue(RadioButtonEnum::YES == $property->isHasAc());
        $this->assertTrue('-' == $property->getOfficeCategory());

        return $property;
    }

    /**
     * @depends testCreateSuccess
     */
    public function testArchive(Property $property)
    {
        $property->archive();
        $this->assertTrue(Property::STATUS_ARCHIVED === $property->getStatus());
    }

    /**
     * @depends testCreateSuccess
     */
    public function testMedia(Property $property)
    {
        $property->addMedia('path/to/media1.jpg', 1);
        $this->assertTrue(1 == count($property->getMedias()));

        $property->addMedia('path/to/media2.jpg', 2);
        $this->assertTrue(2 == count($property->getMedias()));

        /** @var PropertyMedia $m */
        foreach ($property->getMedias() as $m) {
            $this->assertTrue('path/to/media' . $m->getListingOrder() . '.jpg' == $m->getSrc());
            $this->assertTrue($property === $m->getProperty());
        }

    }


}