<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 11.
 * Time: 14:57
 */

namespace AppBundle\Tests\Service;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Client;
use AppBundle\Dto\Contract;
use AppBundle\Factory\EntityFactory;
use AppBundle\Service\ContractService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class ContractTest extends TestCase
{

    public function testCreate()
    {
        $admin = $this->createMock(\AppBundle\Entity\Administrator::class);
        $adminDto = $this->createMock(Administrator::class);
        $adminDto->expects($this->once())
            ->method('getId')
            ->willReturn(2);

        $client = $this->createMock(\AppBundle\Entity\Client::class);
        $clientDto = $this->createMock(Client::class);
        $clientDto->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $contractDto = $this->createMock(Contract::class);
        $contractDto->expects($this->once())
            ->method('getAdministrator')
            ->willReturn($adminDto);

        $contractDto->expects($this->once())
            ->method('getClient')
            ->willReturn($clientDto);

        $contract = $this->createMock(\AppBundle\Entity\Contract::class);
        $contract->expects($this->once())
            ->method('getId')
            ->willReturn(3);

        $factory = $this->createMock(EntityFactory::class);
        $factory->expects($this->once())
            ->method('createContract')
            ->willReturn($contract);


        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () use ($admin, $client, $contract) {
                $args = func_get_args();
                $repository = $this->createMock(EntityRepository::class);

                if ($args[0] == 'AppBundle:Client') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($client);
                }
                else if ($args[0] == 'AppBundle:Administrator') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);
                }
                return $repository;

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $this->assertTrue(3 == $service->create($contractDto));

    }


    public function testUpdate()
    {
        $admin = $this->createMock(\AppBundle\Entity\Administrator::class);
        $adminDto = $this->createMock(Administrator::class);
        $adminDto->expects($this->once())
            ->method('getId')
            ->willReturn(2);

        $client = $this->createMock(\AppBundle\Entity\Client::class);
        $clientDto = $this->createMock(Client::class);
        $clientDto->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $contractDto = $this->createMock(Contract::class);
        $contractDto->expects($this->once())
            ->method('getAdministrator')
            ->willReturn($adminDto);
        $contractDto->expects($this->once())
            ->method('getClient')
            ->willReturn($clientDto);
        $contractDto->expects($this->once())
            ->method('getId')
            ->willReturn(3);

        $contract = $this->createMock(\AppBundle\Entity\Contract::class);
        $contract->expects($this->once())
            ->method('getId')
            ->willReturn(3);

        $factory = $this->createMock(EntityFactory::class);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () use ($admin, $client, $contract) {
                $args = func_get_args();
                $repository = $this->createMock(EntityRepository::class);

                if ($args[0] == 'AppBundle:Client') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($client);
                }
                else if ($args[0] == 'AppBundle:Administrator') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);
                }
                else if ($args[0] == 'AppBundle:Contract') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(3))
                        ->willReturn($contract);
                }
                return $repository;

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $this->assertTrue(3 == $service->update($contractDto));
    }


    public function testArchive()
    {
        $contract = $this->createMock(\AppBundle\Entity\Contract::class);
        $contract->expects($this->once())
            ->method('archive');

        $factory = $this->createMock(EntityFactory::class);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($contract);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $service->archive(1);
    }

    public function testActivate()
    {
        $contract = $this->createMock(\AppBundle\Entity\Contract::class);
        $contract->expects($this->once())
            ->method('activate');

        $factory = $this->createMock(EntityFactory::class);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($contract);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $service->activate(1);
    }


    public function testClose()
    {
        $contract = $this->createMock(\AppBundle\Entity\Contract::class);
        $contract->expects($this->once())
            ->method('close')
            ->with($this->equalTo(\AppBundle\Entity\Contract::RESULT_SUCCESS));

        $factory = $this->createMock(EntityFactory::class);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($contract);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $service->close(1, \AppBundle\Entity\Contract::RESULT_SUCCESS);
    }


    public function testAssignProperty()
    {
        $property = $this->createMock(\AppBundle\Entity\Property::class);

        $contract = $this->createMock(\AppBundle\Entity\Contract::class);

        $factory = $this->createMock(EntityFactory::class);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () use ($property, $contract) {
                $args = func_get_args();
                $repository = $this->createMock(EntityRepository::class);

                if ($args[0] == 'AppBundle:Property') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($property);
                }
                else if ($args[0] == 'AppBundle:Contract') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($contract);
                }
                return $repository;

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $service->assignProperty(2, 1);
    }


    public function testAssignAdministrator()
    {
        $admin = $this->createMock(\AppBundle\Entity\Administrator::class);

        $contract = $this->createMock(\AppBundle\Entity\Contract::class);

        $factory = $this->createMock(EntityFactory::class);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () use ($admin, $contract) {
                $args = func_get_args();
                $repository = $this->createMock(EntityRepository::class);

                if ($args[0] == 'AppBundle:Contract') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($contract);
                }
                else if ($args[0] == 'AppBundle:Administrator') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);
                }
                return $repository;

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $service->assignAdministrator(1, 2);
    }

}


