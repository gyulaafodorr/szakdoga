<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 11.
 * Time: 14:57
 */

namespace AppBundle\Tests\Service;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Client;
use AppBundle\Dto\Activity;
use AppBundle\Entity\Contract;
use AppBundle\Entity\Property;
use AppBundle\Factory\EntityFactory;
use AppBundle\Repository\ClientRepository;
use AppBundle\Repository\ContractRepository;
use AppBundle\Service\AdministratorService;
use AppBundle\Service\ClientService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function testCreate()
    {
        $administrator = $this->createMock(\AppBundle\Entity\Administrator::class);

        $dtoAdmin = $this->createMock(Administrator::class);
        $dtoAdmin->expects($this->once())
            ->method("getId")
            ->willReturn(1);

        $dto = $this->createMock(Client::class);
        $dto->expects($this->once())
            ->method("getAdministrator")
            ->willReturn($dtoAdmin);

        $client = $this->createMock(\AppBundle\Entity\Client::class);
        $client->expects($this->once())
            ->method('getId')
            ->willReturn(1);


        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($administrator);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);


        $em->expects($this->once())
            ->method('persist');
        $em->expects($this->once())
            ->method('flush');


        $factory = $this->createMock(EntityFactory::class);
        $factory->expects($this->once())
            ->method('createClient')
            ->with(
                $this->identicalTo($dto),
                $this->identicalTo($administrator)
            )
            ->willReturn($client);

        $service = new ClientService($em, $factory);

        $this->assertTrue(1 == $service->create($dto));

    }


    public function testUpdate()
    {
        $dto = $this->createMock(Client::class);
        $dto->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $factory = $this->createMock(EntityFactory::class);

        $client = $this->createMock(\AppBundle\Entity\Client::class);
        $client->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($client);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new ClientService($em, $factory);

        $this->assertTrue(1 == $service->update($dto));
    }

    public function testArchive()
    {
        $contract = $this->createMock(Contract::class);
        $contract->expects($this->once())
            ->method('archive');

        $property = $this->createMock(Property::class);
        $property->expects($this->once())
            ->method('archive');

        $factory = $this->createMock(EntityFactory::class);

        $client = $this->createMock(\AppBundle\Entity\Client::class);
        $client->expects($this->once())
            ->method('getContracts')
            ->willReturn(array($contract));
        $client->expects($this->once())
            ->method('getProperties')
            ->willReturn(array($property));

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($client);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new ClientService($em, $factory);

        $service->archive(1);
    }

    public function testAssign()
    {
        $factory = $this->createMock(EntityFactory::class);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () {
                $args = func_get_args();
                $admin = $this->createMock(\AppBundle\Entity\Administrator::class);

                if ($args[0] == 'AppBundle:Client') {
                    $client = $this->createMock(\AppBundle\Entity\Client::class);
                    $client->expects($this->once())
                        ->method('assign');

                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($client);

                    return $repository;
                }
                else if ($args[0] == 'AppBundle:Administrator') {

                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);

                    return $repository;
                }

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new ClientService($em, $factory);

        $service->assign(1, 2);
    }


    public function testCreateEventWithoutContract()
    {
        $clientDto = $this->createMock(Client::class);
        $clientDto->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $adminDto = $this->createMock(Administrator::class);
        $adminDto->expects($this->once())
            ->method('getId')
            ->willReturn(2);

        $activity = $this->createMock(\AppBundle\Entity\Activity::class);
        $activity->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $activityDto = $this->createMock(Activity::class);
        $activityDto->expects($this->once())
            ->method('getClient')
            ->willReturn($clientDto);
        $activityDto->expects($this->once())
            ->method('getAdministrator')
            ->willReturn($adminDto);
        $activityDto->expects($this->once())
            ->method('getContract')
            ->willReturn(false);

        $factory = $this->createMock(EntityFactory::class);
        $factory->expects($this->once())
            ->method('createEvent')
            ->willReturn($activity);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () {
                $args = func_get_args();

                if ($args[0] == 'AppBundle:Client') {
                    $client = $this->createMock(\AppBundle\Entity\Client::class);

                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($client);

                    return $repository;
                }
                else if ($args[0] == 'AppBundle:Administrator') {
                    $admin = $this->createMock(\AppBundle\Entity\Administrator::class);

                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);

                    return $repository;
                }
                else if ($args[0] == 'AppBundle:Contract') {
                    $contract = $this->createMock(\AppBundle\Entity\Contract::class);

                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(3))
                        ->willReturn($contract);

                    return $repository;
                }

            }));

        $em->expects($this->once())
            ->method('persist');
        $em->expects($this->once())
            ->method('flush');

        $service = new ClientService($em, $factory);

        $service->createEvent($activityDto);
    }


}


