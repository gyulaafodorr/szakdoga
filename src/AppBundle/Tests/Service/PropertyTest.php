<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 11.
 * Time: 14:57
 */

namespace AppBundle\Tests\Service;

use AppBundle\Dto\Administrator;
use AppBundle\Dto\Client;
use AppBundle\Dto\Contract;
use AppBundle\Factory\EntityFactory;
use AppBundle\Service\ContractService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class PropertyTest extends TestCase
{

    public function testCreate()
    {
        $admin = $this->createMock(\AppBundle\Entity\Administrator::class);
        $adminDto = $this->createMock(Administrator::class);
        $adminDto->expects($this->once())
            ->method('getId')
            ->willReturn(2);

        $client = $this->createMock(\AppBundle\Entity\Client::class);
        $clientDto = $this->createMock(Client::class);
        $clientDto->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $contractDto = $this->createMock(Contract::class);
        $contractDto->expects($this->once())
            ->method('getAdministrator')
            ->willReturn($adminDto);

        $contractDto->expects($this->once())
            ->method('getClient')
            ->willReturn($clientDto);

        $contract = $this->createMock(\AppBundle\Entity\Contract::class);
        $contract->expects($this->once())
            ->method('getId')
            ->willReturn(3);

        $factory = $this->createMock(EntityFactory::class);
        $factory->expects($this->once())
            ->method('createContract')
            ->willReturn($contract);


        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () use ($admin, $client, $contract) {
                $args = func_get_args();
                $repository = $this->createMock(EntityRepository::class);

                if ($args[0] == 'AppBundle:Client') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(1))
                        ->willReturn($client);
                }
                else if ($args[0] == 'AppBundle:Administrator') {
                    $repository->expects($this->atLeastOnce())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);
                }
                return $repository;

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new ContractService($em, $factory);

        $this->assertTrue(3 == $service->create($contractDto));

    }



}


