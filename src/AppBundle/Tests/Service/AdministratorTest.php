<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 11.
 * Time: 14:57
 */

namespace AppBundle\Tests\Service;

use AppBundle\Dto\Administrator;
use AppBundle\Repository\ClientRepository;
use AppBundle\Repository\ContractRepository;
use AppBundle\Service\AdministratorService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;

class AdministratorTest extends TestCase
{
    public function testCreate()
    {
        $em = $this->createMock(EntityManager::class);
        $dto = $this->createMock(Administrator::class);

        $em->expects($this->once())
            ->method('persist');
        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $this->assertInstanceOf('\AppBundle\Entity\Administrator', $service->create($dto));
    }


    public function testUpdate()
    {
        $entity = $this->createMock(\AppBundle\Entity\Administrator::class);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($entity);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);
        $dto = $this->createMock(Administrator::class);


        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $this->assertInstanceOf('\AppBundle\Entity\Administrator', $service->update(1, $dto));
    }


    public function testActivate()
    {
        $entity = $this->createMock(\AppBundle\Entity\Administrator::class);
        $entity->expects($this->once())
            ->method('activate');
        $entity->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($entity);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $this->assertTrue(1 == $service->activate(1));
    }


    public function testInactivate()
    {
        $entity = $this->createMock(\AppBundle\Entity\Administrator::class);
        $entity->expects($this->once())
            ->method('inactivate');
        $entity->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($entity);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $this->assertTrue(1 == $service->inactivate(1));
    }

    public function testArchive()
    {
        $entity = $this->createMock(\AppBundle\Entity\Administrator::class);
        $entity->expects($this->once())
            ->method('archive');
        $entity->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->once())
            ->method('find')
            ->with($this->equalTo(1))
            ->willReturn($entity);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repository);

        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $this->assertTrue(1 == $service->archive(1));
    }


    public function testAssignAllClients()
    {
        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () {
                $args = func_get_args();
                $admin = $this->createMock(\AppBundle\Entity\Administrator::class);

                if ($args[0] == 'AppBundle:Administrator') {
                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);

                    return $repository;
                }
                else if ($args[0] == 'AppBundle:Client') {
                    $repository = $this->createMock(ClientRepository::class);
                    $entity = $this->createMock(\AppBundle\Entity\Client::class);
                    $entity->expects($this->atLeastOnce())
                        ->method('assignToAdministrator')
                        ->with($this->equalTo($admin));
                    $repository->expects($this->once())
                        ->method('getAllByAdministrator')
                        ->with($this->equalTo(1))
                        ->willReturn(array($entity));
                    return $repository;
                }

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $service->assignAllClients(1, 2);
    }


    public function testAssignAllContracts()
    {
        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->will($this->returnCallback(function () {
                $args = func_get_args();
                $admin = $this->createMock(\AppBundle\Entity\Administrator::class);

                if ($args[0] == 'AppBundle:Administrator') {
                    $repository = $this->createMock(EntityRepository::class);
                    $repository->expects($this->once())
                        ->method('find')
                        ->with($this->equalTo(2))
                        ->willReturn($admin);

                    return $repository;
                }
                else if ($args[0] == 'AppBundle:Contract') {
                    $repository = $this->createMock(ContractRepository::class);
                    $entity = $this->createMock(\AppBundle\Entity\Contract::class);
                    $entity->expects($this->atLeastOnce())
                        ->method('assignToAdministrator')
                        ->with($this->equalTo($admin));
                    $repository->expects($this->once())
                        ->method('getAllByAdministrator')
                        ->with($this->equalTo(1))
                        ->willReturn(array($entity));
                    return $repository;
                }

            }));

        $em->expects($this->once())
            ->method('flush');

        $service = new AdministratorService($em);

        $service->assignAllContracts(1, 2);
    }

}


