<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 11:39
 */

namespace AppBundle\Event;

class EventDispatcher implements EventDispatcherInterface
{
    /**
     * @var EventHandlerInterface
     */
    private static $eventHandler;

    public static function setEventHandler(EventHandlerInterface $handler)
    {
        self::$eventHandler = $handler;
    }

    public static function raise(EventInterface $event)
    {
        return self::$eventHandler->handleEvent($event);
    }

}
