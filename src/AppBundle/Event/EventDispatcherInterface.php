<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 11:40
 */

namespace AppBundle\Event;


interface EventDispatcherInterface
{
    public static function raise(EventInterface $event);
}

