<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 11:43
 */

namespace AppBundle\Event;


class SymfonyEventDispatcherHandler implements EventHandlerInterface
{
    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    private $handler;

    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher)
    {
        $this->handler = $dispatcher;
    }

    public function handleEvent(EventInterface $event)
    {
        return $this->handler->dispatch($event->getEventName(), $event);
    }

}
