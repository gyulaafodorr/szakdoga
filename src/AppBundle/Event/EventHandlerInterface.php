<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 03. 19.
 * Time: 11:43
 */

namespace AppBundle\Event;

interface EventHandlerInterface
{
    public function handleEvent(EventInterface $event);
}
