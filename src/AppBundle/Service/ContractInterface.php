<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 18.
 * Time: 15:25
 */

namespace AppBundle\Service;


use AppBundle\Dto\Contract;

interface ContractInterface
{
    public function create(Contract $dto);

    public function update(Contract $dto);

    public function archive($id);

    public function assignProperty($id, $propertyId);
}