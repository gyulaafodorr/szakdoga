<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 18.
 * Time: 15:21
 */

namespace AppBundle\Service;

use AppBundle\Entity\Activity;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;
use AppBundle\Entity\Property;
use AppBundle\Factory\EntityFactory;
use Doctrine\ORM\EntityManager;

class ClientService extends AbstractService implements ClientInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityFactory
     */
    private $factory;

    /**
     * ClientService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, EntityFactory $factory)
    {
        $this->em = $em;
        $this->factory = $factory;
    }

    /**
     * @param \AppBundle\Dto\Client $dto
     * @return int
     */
    public function create(\AppBundle\Dto\Client $dto)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($dto->getAdministrator()->getId());
        $client = $this->factory->createClient($dto, $administrator);

        $this->em->persist($client);
        $this->em->flush();

        return $client->getId();
    }

    /**
     * @param \AppBundle\Dto\Client $dto
     * @return int
     */
    public function update(\AppBundle\Dto\Client $dto)
    {
        $client = $this->em->getRepository('AppBundle:Client')->find($dto->getId());
        $client->update(
            $dto->getFirstName(),
            $dto->getLastName(),
            $dto->getZipcode(),
            $dto->getCity(),
            $dto->getAddress(),
            $dto->getIdNumber(),
            $dto->getEmail(),
            $dto->getEmailType(),
            $dto->getContact1Phone(),
            $dto->getContact1Description(),
            $dto->getContact1Type(),
            $dto->getContact2Phone(),
            $dto->getContact2Description(),
            $dto->getContact2Type(),
            $dto->getContact3Phone(),
            $dto->getContact3Description(),
            $dto->getContact3Type(),
            $dto->getComment(),
            $dto->isIsActive()
        );

        $this->em->flush();

        return $client->getId();
    }

    /**
     * Ügyfél archiválása
     *
     * @param $id
     */
    public function archive($id)
    {
        $client = $this->em->getRepository('AppBundle:Client')->find($id);

        /** @var Contract $c */
        foreach ($client->getContracts() as $c) {
            $c->archive();
        }

        /** @var Property $p */
        foreach ($client->getProperties() as $p) {
            $p->archive();
        }

        $client->archive();

        $this->em->flush();
    }

    /**
     * Ügyfél Munkatárshoz rendelése
     *
     * @param $id
     * @param $administratorId
     */
    public function assign($id, $administratorId)
    {
        $client = $this->em->getRepository('AppBundle:Client')->find($id);
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($administratorId);

        $client->assign($administrator);

        $this->em->flush();
    }

    /**
     * @param \AppBundle\Dto\Activity $dto
     * @return int
     */
    public function createEvent(\AppBundle\Dto\Activity $dto)
    {
        $client = $this->em->getRepository('AppBundle:Client')->find($dto->getClient()->getId());
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($dto->getAdministrator()->getId());
        $contract = null;
        if ($dto->getContract() && $dto->getContract()->getId()) {
            $contract = $this->em->getRepository('AppBundle:Contract')->find($dto->getContract()->getId());
        }

        $activity = $this->factory->createEvent($dto, $administrator, $client, $contract);

        $this->em->persist($activity);
        $this->em->flush();

        return $activity->getId();
    }


}