<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 18.
 * Time: 15:25
 */

namespace AppBundle\Service;

use AppBundle\Dto\Activity;
use AppBundle\Dto\Client;

interface ClientInterface
{
    public function create(Client $dto);

    public function update(Client $dto);

    public function archive($id);

    public function assign($id, $administratorId);

    public function createEvent(Activity $dto);
}