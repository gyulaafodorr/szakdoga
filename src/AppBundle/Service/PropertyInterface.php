<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 04.
 * Time: 10:43
 */

namespace AppBundle\Service;

use AppBundle\Dto\Property;

interface PropertyInterface
{
    public function create(Property $dto);

    public function update(Property $dto);

    public function addMedia($propertyId, $path);

    public function removeMedia($id, $mediaId);


}