<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 18.
 * Time: 15:21
 */

namespace AppBundle\Service;


use AppBundle\Entity\Contract;
use AppBundle\Factory\EntityFactory;
use Doctrine\ORM\EntityManager;

class ContractService extends AbstractService implements ContractInterface
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var EntityFactory
     */
    private $factory;

    /**
     * ContractService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, EntityFactory $factory)
    {
        $this->em = $em;
        $this->factory = $factory;
    }

    /**
     * @param \AppBundle\Dto\Contract $dto
     * @return int
     */
    public function create(\AppBundle\Dto\Contract $dto)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($dto->getAdministrator()->getId());
        $client = $this->em->getRepository('AppBundle:Client')->find($dto->getClient()->getId());

        $contract = $this->factory->createContract($dto, $administrator, $client);

        $this->em->persist($contract);
        $this->em->flush();

        return $contract->getId();
    }

    /**
     * @param \AppBundle\Dto\Contract $dto
     * @return int
     */
    public function update(\AppBundle\Dto\Contract $dto)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($dto->getAdministrator()->getId());
        $client = $this->em->getRepository('AppBundle:Client')->find($dto->getClient()->getId());
        $contract = $this->em->getRepository('AppBundle:Contract')->find($dto->getId());
        $contract->update(
            $dto->getContractNumber(),
            $dto->getCommission(),
            $dto->getComment(),
            $dto->getAgreementType(),
            $dto->isIsExclusive(),
            $dto->getDeadline(),
            $dto->getPrice(),
            $dto->getPriceEur(),
            $client,
            $administrator
        );

        $this->em->flush();

        return $contract->getId();
    }

    /**
     * Archiválás
     *
     * @param $id
     */
    public function archive($id)
    {
        $contract = $this->em->getRepository('AppBundle:Contract')->find($id);
        $contract->archive();

        $this->em->flush();
    }

    /**
     * Archiválás
     *
     * @param $id
     */
    public function activate($id)
    {
        $contract = $this->em->getRepository('AppBundle:Contract')->find($id);
        $contract->activate();

        $this->em->flush();
    }

    /**
     * Ingatlan hozzárendelése
     *
     * @param $id
     * @param $propertyId
     */
    public function assignProperty($id, $propertyId)
    {
        $property = $this->em->getRepository('AppBundle:Property')->find($propertyId);
        $contract = $this->em->getRepository('AppBundle:Contract')->find($id);
        $contract->assignProperty($property);

        $this->em->flush();
    }


    /**
     * Munkatárs hozzárendelése
     *
     * @param $id
     * @param $administratorId
     */
    public function assignAdministrator($id, $administratorId)
    {
        $admin = $this->em->getRepository('AppBundle:Administrator')->find($administratorId);
        $contract = $this->em->getRepository('AppBundle:Contract')->find($id);
        $contract->assignToAdministrator($admin);

        $this->em->flush();
    }

    /**
     * Megbízás zárása.
     *
     * @param $id
     * @param $result
     */
    public function close($id, $result)
    {
        $contract = $this->em->getRepository('AppBundle:Contract')->find($id);
        $contract->close($result);

        $this->em->flush();
    }
}