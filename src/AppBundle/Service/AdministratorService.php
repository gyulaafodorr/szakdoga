<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 18.
 * Time: 15:21
 */

namespace AppBundle\Service;

use AppBundle\Entity\Administrator;
use AppBundle\Entity\Client;
use AppBundle\Entity\Contract;
use Doctrine\ORM\EntityManager;

class AdministratorService extends AbstractService implements AdministratorInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * AdministratorService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param \AppBundle\Dto\Administrator $dto
     * @return Administrator
     */
    public function create(\AppBundle\Dto\Administrator $dto)
    {
        $administrator = new Administrator(
            $dto->getName(),
            $dto->getUsername(),
            $dto->getEmail(),
            $dto->getPhone(),
            $dto->getPhone2(),
            $dto->getPhone3(),
            $dto->getPassword(),
            $dto->getRole(),
            $dto->getStatus()
        );

        $this->em->persist($administrator);
        $this->em->flush();

        return $administrator;
    }

    /**
     * @param \AppBundle\Dto\Administrator $id
     * @param $dto
     * @return Administrator|null|object
     */
    public function update($id, \AppBundle\Dto\Administrator $dto)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($id);
        $administrator->update(
            $dto->getName(),
            $dto->getUsername(),
            $dto->getEmail(),
            $dto->getPhone(),
            $dto->getPhone2(),
            $dto->getPhone3(),
            $dto->getPassword(),
            $dto->getRole(),
            $dto->getStatus()
        );

        $this->em->flush();

        return $administrator;
    }

    /**
     * Munkatárs aktiválása
     *
     * @param $id
     * @return int
     */
    public function activate($id)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($id);
        $administrator->activate();

        $this->em->flush();

        return $administrator->getId();
    }

    /**
     * Munkatárs inaktiválása
     *
     * @param $id
     * @return int
     */
    public function inactivate($id)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($id);
        $administrator->inactivate();

        $this->em->flush();

        return $administrator->getId();
    }

    /**
     * Munkatárs archiválása
     *
     * @param $id
     * @return int
     */
    public function archive($id)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($id);
        $administrator->archive();

        $this->em->flush();

        return $administrator->getId();
    }

    /**
     * Ügyfelek adminisztrotorhoz rendelés
     * @param $id
     * @param $toId
     */
    public function assignAllClients($id, $toId)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($toId);

        $list = $this->em->getRepository('AppBundle:Client')
            ->getAllByAdministrator($id);

        /** @var Client $client */
        foreach ($list as $client) {
            $client->assignToAdministrator($administrator);
        }

        $this->em->flush();
    }

    /**
     * Megbízások adminisztrátorhoz rendelése
     *
     * @param $id
     * @param $toId
     */
    public function assignAllContracts($id, $toId)
    {
        $administrator = $this->em->getRepository('AppBundle:Administrator')->find($toId);

        $list = $this->em->getRepository('AppBundle:Contract')
            ->getAllByAdministrator($id);

        /** @var Contract $contract */
        foreach ($list as $contract) {
            $contract->assignToAdministrator($administrator);
        }
        $this->em->flush();
    }


}