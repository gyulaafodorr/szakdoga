<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 04. 18.
 * Time: 15:25
 */

namespace AppBundle\Service;


use AppBundle\Dto\AbstractDto;
use AppBundle\Dto\Administrator;

interface AdministratorInterface
{
    public function create(Administrator $dto);

    public function update($id, Administrator $dto);

    public function activate($id);

    public function inactivate($id);

    public function archive($id);

    public function assignAllClients($id, $toId);

    public function assignAllContracts($id, $toId);

}