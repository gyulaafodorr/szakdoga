<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2017. 05. 04.
 * Time: 10:43
 */

namespace AppBundle\Service;


use AppBundle\Entity\Property;
use AppBundle\Entity\PropertyMedia;
use AppBundle\Factory\EntityFactory;
use Doctrine\ORM\EntityManager;

class PropertyService implements PropertyInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $uploadUrl;

    /**
     * @var EntityFactory
     */
    private $factory;

    /**
     * AdministratorService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $uploadUrl, EntityFactory $factory)
    {
        $this->em = $em;
        $this->uploadUrl = $uploadUrl;
        $this->factory = $factory;
    }


    /**
     * Ingatlan létrehozása
     *
     * @param \AppBundle\Dto\Property $dto
     * @return int
     */
    public function create(\AppBundle\Dto\Property $dto)
    {
        $administrator = $this->em->getRepository('AppBundle\Entity\Administrator')->find($dto->getAdministrator()->getId());
        $client = $this->em->getRepository('AppBundle\Entity\Client')->find($dto->getClient()->getId());

        $city = $dto->getCity() ? $this->em->getRepository('AppBundle\Entity\City')->find($dto->getCity()->getId()) : null;
        $borough = $dto->getBorough() ? $this->em->getRepository('AppBundle\Entity\Borough')->find($dto->getBorough()->getId()) : null;


        $property = new Property(
            $administrator,
            $client,
            $dto->getStatus(),
            $dto->getType(),
            $dto->getSubType(),
            $dto->getSize(),
            $dto->getLotSize(),
            $dto->getCounty(),
            $city,
            $borough,
            $dto->getStreet(),
            $dto->isStreetNp(),
            $dto->getDistrict(),
            $dto->getHouseNumber(),
            $dto->isHouseNumberNp(),
            $dto->getRooms(),
            $dto->getHalfRooms(),
            $dto->getFloor(),
            $dto->getLevels(),
            $dto->getHeating(),
            $dto->getComfort(),
            $dto->getDescription(),
            $dto->isHasElevator(),
            $dto->getParking(),
            $dto->getHasElectricity(),
            $dto->getHasGas(),
            $dto->getHasWater(),
            $dto->isHasCellar(),
            $dto->getHasDrain(),
            $dto->getView(),
            $dto->getParkingFee(),
            $dto->getParkingFeeUom(),
            $dto->getCondition(),
            $dto->getBalconySize(),
            $dto->getGardenSize(),
            $dto->getAttic(),
            $dto->isHasFurniture(),
            $dto->isAgentsCanHelp(),
            $dto->getEnergyCertificate(),
            $dto->isCeilingHeight(),
            $dto->isSmoker(),
            $dto->isHasDisabledAccess(),
            $dto->isMechanized(),
            $dto->isPetAllowed(),
            $dto->isPanelProgram(),
            $dto->getUtilityBills(),
            $dto->isHasGardenAccess(),
            $dto->isLeaseRights(),
            $dto->getOrientation(),
            $dto->getBathToilet(),
            $dto->isHasAc(),
            $dto->getOfficeCategory()
        );

        $this->em->persist($property);
        $this->em->flush();

        return $property->getId();
    }

    public function update(\AppBundle\Dto\Property $dto)
    {
        $property = $this->em->getRepository('AppBundle\Entity\Property')->find($dto->getId());

        $administrator = $this->em->getRepository('AppBundle\Entity\Administrator')->find($dto->getAdministrator()->getId());
        $client = $this->em->getRepository('AppBundle\Entity\Client')->find($dto->getClient()->getId());

        $city = $dto->getCity() ? $this->em->getRepository('AppBundle\Entity\City')->find($dto->getCity()->getId()) : null;
        $borough = $dto->getBorough() ? $this->em->getRepository('AppBundle\Entity\Borough')->find($dto->getBorough()->getId()) : null;

        $property->update(
            $administrator,
            $client,
            $dto->getStatus(),
            $dto->getType(),
            $dto->getSubType(),
            $dto->getSize(),
            $dto->getLotSize(),
            $dto->getCounty(),
            $city,
            $borough,
            $dto->getStreet(),
            $dto->isStreetNp(),
            $dto->getDistrict(),
            $dto->getHouseNumber(),
            $dto->isHouseNumberNp(),
            $dto->getRooms(),
            $dto->getHalfRooms(),
            $dto->getFloor(),
            $dto->getLevels(),
            $dto->getHeating(),
            $dto->getComfort(),
            $dto->getDescription(),
            $dto->isHasElevator(),
            $dto->getParking(),
            $dto->getHasElectricity(),
            $dto->getHasGas(),
            $dto->getHasWater(),
            $dto->isHasCellar(),
            $dto->getHasDrain(),
            $dto->getView(),
            $dto->getParkingFee(),
            $dto->getParkingFeeUom(),
            $dto->getCondition(),
            $dto->getBalconySize(),
            $dto->getGardenSize(),
            $dto->getAttic(),
            $dto->isHasFurniture(),
            $dto->isAgentsCanHelp(),
            $dto->getEnergyCertificate(),
            $dto->isCeilingHeight(),
            $dto->isSmoker(),
            $dto->isHasDisabledAccess(),
            $dto->isMechanized(),
            $dto->isPetAllowed(),
            $dto->isPanelProgram(),
            $dto->getUtilityBills(),
            $dto->isHasGardenAccess(),
            $dto->isLeaseRights(),
            $dto->getOrientation(),
            $dto->getBathToilet(),
            $dto->isHasAc(),
            $dto->getOfficeCategory()
        );

        $this->em->flush();

        return $property->getId();
    }

    /**
     * Kép hozzáadása
     *
     * @param $propertyId
     * @param $path
     * @return PropertyMedia
     */
    public function addMedia($propertyId, $path)
    {
        $property = $this->em->getRepository('AppBundle\Entity\Property')->find($propertyId);
        $max = 0;

        /** @var PropertyMedia $ph */
        foreach ($property->getMedias() as $ph) {
            if ($max < $ph->getListingOrder()) {
                $max = $ph->getListingOrder();
            }
        }
        $photo = $property->addMedia($path, $max + 1);

        $this->em->flush();

        return $photo;
    }

    /**
     * Kép törlése
     *
     * @param $id
     * @param $mediaId
     * @return bool
     */
    public function removeMedia($id, $mediaId)
    {
        $property = $this->em->getRepository('AppBundle\Entity\Property')->find($id);
        $media = $this->em->getRepository('AppBundle\Entity\PropertyMedia')->find($mediaId);

        @unlink($this->uploadUrl . '/' . $media->getSrc());

        $this->em->remove($media);

        $this->em->flush();

        return true;
    }


}